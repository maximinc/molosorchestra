\version "2.18.2"
\language "english"

\header {
  title = "Hava nagila"
  composer = "Folklorique Hébreu"
}

#(set-default-paper-size "a4")
%\set Score.skipTypesetting = ##t
%showLastLength = R1*8

melody = \relative c' {
  \clef "treble"
  \key c \major
  \time 4/4
  \tempo 2=120

  \repeat volta 2 {

    \repeat volta 2 {
      e2 e2~        |  e4 gs4( f4) e4  |  gs2 gs~   |
      gs4 b( a) gs  |  a2 a            |  a4 c b a  |
    }
    \alternative {
      {  \set Score.repeatCommands = #'((volta "1, 3"))
         gs2 f8( e f4)    |  gs1  |  }
      {  \set Score.repeatCommands = #'((volta "2, 4") end-repeat)
         gs2( f8 e f gs)  |  e1   |  }
    }

    gs4 gs2( f4)      |  e e e2           |  f4 f2( e4)       |
    d4 d d2           |  d2 f4.( e8       |  d4) d a'2        |
    gs f8( e f4)      |  << e2. gs >> r4  |  gs gs2( f4)      |
    e e e2            |  f4 f2( e4)       |  d d d2           |
    d2 f4.( e8        |  d4) d a'2        |  gs f8( e f4)     |  e2. r4      |
    a1                |  a                |  a2 e'            |  c a         |
    a8 a a4 c4.( b8   |  a4) c b a        |  a8 a a4 c4.( b8  |  a4) c b a   |
    b8 b b4 d4. ( c8  |  b4) d c b        |  b8 b b4 d4.( c8  |  b4) d c b   |
    b8 b b4 e2        |  b8 b b4 e4. e8   |
  }
  \alternative {
    {  e4.( d8 c4) b     |  a e e e  |  }
    {  e'( f8 e d c b4)  |  a g' r2  |  }
  }
}

harmony = \chordmode {
  \repeat volta 2 {
    \repeat volta 2 {
      e1  |  e  |  e  |  e  |  a:m  |  a:m  |
    }
    \alternative {
      {  e2 d:m  |  e1  |  }
      {  e2 d:m  |  e1  |  }
    }
    e:7  |  e:7  |  d:m  |  d:m  |  d:m  |  d:m  |  e:7     |  e:7  |
    e:7  |  e:7  |  d:m  |  d:m  |  d:m  |  d:m  |  e2 d:m  |  e1:7 |
    a:m  |  a:m  |  a:m  |  a:m  |  a:m  |  a:m  |  a:m     |  a:m  |
    e:7  |  e:7  |  e:7  |  e:7  |  e:7  |  e:7  |
  }
  \alternative {
    {  e:7  |  a:m  |  }
    {  e:7  |  a:m  |  }
  }
}



\book {
  \bookOutputName  "hava_nagila_Bb"
  \score {
    <<
      \new ChordNames {
        \transpose c d { \harmony }
      }
      \new Staff \with { instrumentName = #"Bb" } {
        \transpose c d  { \melody }
      }
    >>
    \layout {}
  }
}


\book {
  \bookOutputName  "hava_nagila_C"
  \score {
    <<
      \new ChordNames {
        \transpose c c { \harmony }
      }
      \new Staff \with { instrumentName = #"C" } {
        \transpose c c  { \melody }      }
    >>
    \layout {}
  }
}


\book {
  \bookOutputName  "hava_nagila_Eb"
  \score {
    <<
      \new ChordNames {
        \transpose c a { \harmony }
      }
      \new Staff \with { instrumentName = #"Eb" } {
        \transpose c a  { \melody }      }
    >>
    \layout {}
  }
}

