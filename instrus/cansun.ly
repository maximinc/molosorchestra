\version "2.18.2"
\language "english"

\paper {
  system-system-spacing #'basic-distance = #1
}

\header {
  title = "Cansun'd Rusbela - Ballo Del Curioso Accidente"
  composer = "The Folk Messengers"
}

global = {
  \set Score.skipBars = ##t
  \time 12/8
}

gridA={
  \chordmode{
    d2.:m s4. d4.:m
    g2.:m s4. d4.:m
    g2.:m s4. d4.:m
    bf2. c4.:7sus4 c
  }
}

gridB={
  \chordmode{
    d1.:m  g2.:m a
    c2. s4. f4. g:m c a:m d:m
    d1.:m  g2.:m a
    c2. s4. f4. g:m c a:m d:m

    f2. g:m c4. a d2.
    bf2. c4. f g:m c a:m d:m
    f2. g:m c4. a d2.
    bf2. c4. f g:m c a:m d:m
  
  }
}

chordNames={
  \gridA
  \gridA
  \gridA
  \gridA
  \gridB
}

theme={
  \key d \minor

  \repeat volta 2{
    \relative c'{
      f4. a4 f8 d4. d4 c8 d4. f c4 f8 e4 g8 f4 a8 c4 a8
      f2. r g8 g g~ g f e \break
  
      f4. a4 f8 d4. d4 c8 d4. f c4 f8 e4 g8 f4 a8 c4 a8
      f2. r r \break
      
      d8 d d e4 f8 g4. r4 a8\mordent  
      g4 a8~ a8 a\mordent g f4. d4.
      f4 \appoggiatura {g16} a8 g4 f8 c4 d8 f4 g8 f4 a8~\mordent a4 f8 e4. d4 c8
      \break
  
      d8 d d e4 f8 g4. r4 a8\mordent  
      g4 a8~ a8 a\mordent g f4. d4.
      f4 \appoggiatura {g16} a8 g4 f8 c4 d8 f4 g8 f4 a8~\mordent a4 f8 e4. d4 c8
  
    }
  }
  \break

  \repeat volta 2{
    \relative c''{
      d8 cs d a4\mordent g8 f e f d f a d e f f e d cs d e a,4.\mordent
      \break
      c8 d c c4 a8 c d e f c bf a g f c4 e8 e d c d4.
      \break
    }
    \relative c''{
      d8 cs d a4\mordent g8 f e f d f a d e f f e d cs d e a,4.\mordent
      \break
      c8 d c c4 a8 c d e f c bf a g f c4 e8 e d c d4.
      \break
    }
    \relative c''{
      a8 bf c c d c bf a bf g4 g8 e f g g f e a4 a8 a4.
      \break
      d8 e f f e d c d bf a bf g f e d e4 f8 e d c d4.
      \break
    }
    \relative c''{
      a8 bf c c d c bf a bf g4 g8 e f g g f e a4 a8 a4.
      \break
      d8 e f f e d c d bf a bf g f e d e4 f8 e d c d4.
      \break
    }
  }
}


\book {
  \bookOutputName "cansun_Bb"
  \score{
  <<
    \global 
    \new ChordNames { 
      \transpose c d { \chordNames }
    }

    \new Staff \with { instrumentName = #"Bb" } {
      \transpose c d { \theme }
    }
  >>
  \layout { }
  }
}

\book {
  \bookOutputName "cansun_C"
  \score{
  <<
    \global 
    \new ChordNames { 
      \transpose c c { \chordNames }
    }

    \new Staff \with { instrumentName = #"C" } {
      \transpose c c { \theme }
    }
  >>
  \layout { }
  }
}


\book {
  \bookOutputName "cansun_Eb"
  \score{
  <<
    \global 
    \new ChordNames { 
      \transpose c a { \chordNames }
    }

    \new Staff \with { instrumentName = #"Eb" } {
      \transpose c a { \theme }
    }
  >>
  \layout { }
  }
}

