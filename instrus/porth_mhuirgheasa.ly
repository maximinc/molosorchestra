\version "2.18.2"

\header {
  title = "Porth Mhuirgeasa"
  composer = "Gigue Irlandaise"
}

#(set-default-paper-size "a4")
%\set Score.skipTypesetting = ##t
%showLastLength = R1*8

melody = \relative c' {
  \clef "treble"
  \key g \major
  \time 6/8
  \tempo 4.=130

  \repeat volta 2 {
    a8 a8 a8  e'8 a,8 e'8  |  a,8 e'8 a,8  d8 b8 g8  |
    a8 a8 a8  e'8 a,8 e'8  |  g8 fis8 e8  d8 b8 g8   |   \break
    a8 a8 a8  e'8 a,8 e'8  |  a,8 e'8 a,8  d8 b8 g8  |
    c8 c8 c8  b8 c8 d8     |  e8 d8 c8  b8 a8 g8     |   \break
  }

  \repeat volta 2 {
    e'8 a8 a8  b8 a8 a8    |  d8 a8 a8  b8 a8 g8     |
    e8 a8 a8  b8 a8 a8     |  d4 c8  b8 a8 g8        |   \break
  }
  \alternative {
    {  e8 a8 a8  b8 a8 a8  |  d8 a8 a8  b8 a8 g8     |
       c8 b8 a8  g4 d8     |  e8 d8 c8  b8 a8 g8     |   \break }
    {  c'4.  c8 b8 a8      |  g8 a8 b8  c4 g8        |
       a8 g8 fis8  g4 d8   |  e8 d8 c8  b8 a8 g8     |  a2.  |  }
  }
}


harmony = \chordmode {
  \repeat volta 2 {
    a2.:m          |  a4.:m g4.  |  a2.:m        |  g4. e4.:m  |
    a2.:m          |  a4.:m g4.  |  c4. g4.      |  c4. e4.:m  |
  }
  \repeat volta 2 {
    a4.:m e4.:m    |  f4. g4.    |  a4.:m e4.:m  |  f4. g4.    |
  }
  \alternative {
    { a4.:m e4.:m  |  f4. g4.    |  a4.:m g4.    |  c4. e4.:m  |   }
    { c2.          |  c2.        |  d4. g4.      |  c4. e4.:m  |   }
  }
  a2.:m  |
}



\book {
  \bookOutputName "porth_mhuirgheasa_Bb"
  \score {
    <<
      \new ChordNames {
        \transpose c d  { \harmony }
      }
      \new Staff \with { instrumentName = #"Bb" } {
        \transpose c d  { \melody }
      }
    >>
    \layout {}
  }
}

\book {
  \bookOutputName "porth_mhuirgheasa_C"
  \score {
    <<
      \new ChordNames {
        \transpose c c  { \harmony }
      }
      \new Staff \with { instrumentName = #"C" } {
        \transpose c c  { \melody }
      }
    >>
    \layout {}
  }
}


\book {
  \bookOutputName "porth_mhuirgheasa_Eb"
  \score {
    <<
      \new ChordNames {
        \transpose c a  { \harmony }
      }
      \new Staff \with { instrumentName = #"Eb" } {
        \transpose c a  { \melody }
      }
    >>
    \layout {}
  }
}
