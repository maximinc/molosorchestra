\version "2.18.2"

\header {
  title = "Tarentelle"
  composer = "Traditionnel Italien"
}

#(set-default-paper-size "a4")


melody = \relative c' {
  \clef "treble"
  \key c \major
  \time 6/8
  \tempo 4.=130

  \repeat volta 2 {
    e8 a8 b8  c8 b8 c8  |  d8 c8 b8  c8 r8 c8  |  a8 b8 c8  b8 g8 b8  |
  }
  \alternative {
    { d8 c8 b8  c4 a8   |  }
    { d8 c8 b8  a4.     |  }
  }
  \break
  \repeat volta 2 {
    a'4 a8  e4 e8       |  a4 a8  e4 e8        |
    e8 d8 e8  f4 f8     |  f8 a8 f8  e4 e8     |   \break
    e8 f8 e8  d4 d8     |  d8 e8 d8  c4 c8     |
    a8 b8 c8  b8 g8 b8  |  d8 c8 b8  a4.       |
  }
}


harmony = \chordmode {
  \repeat volta 2 {
    a2.:m  |  r4. f2. g4.  |
  }
  \alternative {
    { e4.:m a4.:m  |  }
    { e4.:m a4.:m  |  }
  }
  \repeat volta 2 {
    a2.:m      |  a2.:m    |  a4.:m d4.:m  |  d4.:m a4.:m  |
    a4.:m g4.  |  g4. f4.  |  f4. g4.      |  e4.:m a4.:m  |
  }
}



\book {
  \bookOutputName "tarentelle_all"
  \score {
    <<
      \new ChordNames {
        \transpose c d  { \harmony }
      }
      \new Staff \with { instrumentName = #"Bb" } {
        \transpose c d  { \melody }
      }
    >>
    \layout {}
  }
              
  \score {
    <<
      \new ChordNames {
        \transpose c c  { \harmony }
      }
      \new Staff \with { instrumentName = #"C" } {
        \transpose c c  { \melody }
      }
    >>
    \layout {}
  }
  
  \score {
    <<
      \new ChordNames {
        \transpose c a  { \harmony }
      }
      \new Staff \with { instrumentName = #"Eb" } {
        \transpose c a  { \melody }
      }
    >>
    \layout {}
  }
}



\book {
  \bookOutputName "tarentelle_Bb"
  \score {
    <<
      \new ChordNames {
        \transpose c d  { \harmony }
      }
      \new Staff \with { instrumentName = #"Bb" } {
        \transpose c d  { \melody }
      }
    >>
    \layout {}
  }
}


\book {
  \bookOutputName "tarentelle_C"
  \score {
    <<
      \new ChordNames {
        \transpose c c  { \harmony }
      }
      \new Staff \with { instrumentName = #"C" } {
        \transpose c c  { \melody }
      }
    >>
    \layout {}
  }
}


\book {
  \bookOutputName "tarentelle_Eb"
  \score {
    <<
      \new ChordNames {
        \transpose c a  { \harmony }
      }
      \new Staff \with { instrumentName = #"Eb" } {
        \transpose c a  { \melody }
      }
    >>
    \layout {}
  }
}

