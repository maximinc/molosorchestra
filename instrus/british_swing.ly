\version "2.18.2"
\language "english"

\header {
  title = "British Swing"
  composer = "Scottiche"
}

%#(set-default-paper-size "a4")
%\set Score.skipTypesetting = ##t
%showLastLength = R1*8

melody = \relative c' {
  \clef "treble"
  \key c \major
  \time 2/4
  \tempo 4 = 100

  \repeat volta 2 {
    a8 e'16 d~  d e~ e c~  |  c8. a16~  a16 b c e~  |
    e8 f16 e  d c~ c b~    |  b8. a16  g a b c      |  \break
    a8 e'16 f~  f e~ e c~  |  c8. a16~  a b c e     |
    d c b a~  a b c d      |
  }
  \alternative {
    {  b4~  b8. a16    |  }
    {  b4~  b8. g'16~  |  }
  }
  \break

  \repeat volta 4 {
    g8 e16 c'~  c g~ g e |  b'8. g16~  g e g b  |
  }
    \alternative {
    {  r8 e,16 a  b c d e~  |  e d c b~  b a~ a g  |  \break  }
    {  r8 a16 e  d e~ e d~  |  d c a b  c d b8     |          }
    {  r8 e16 a  b c d e~   |  e d c b~  b a~ a g  |  \break  }
    {  a e d e~  e d~ d c   |  a b c d  e8. a,16~  |          }
  }
  a1
}

harmony = \chordmode {
  \repeat volta 2 {
    a2:m  |  f  |  d:m  |  e:m  |
    a:m   |  f  |  d:m  |
  }
  \alternative {
    {  e:m  |  }
    {  e:m  |  }
  }
  \repeat volta 4 {
    c  |  e:m  |
  }
  \alternative {
    {  f   |  g         |  }
    {  f   |  d4:m g    |  }
    {  f2  |  g         |  }
    {  f   |  d4:m e:m  |  }
  }
  a2:m
}



\book {
  \bookOutputName "british_swing_Bb"
  \score {
    <<
      \new ChordNames {
        \transpose c d  { \harmony }
      }
      \new Staff \with { instrumentName = #"Bb" } {
        \transpose c d  { \melody }
      }
    >>
    \layout {}
  }
}


\book {
  \bookOutputName "british_swing_C"
  \score {
    <<
      \new ChordNames {
        \transpose c c  { \harmony }
      }
      \new Staff \with { instrumentName = #"C" } {
        \transpose c c  { \melody }
      }
    >>
    \layout {}
  }
}


\book {
  \bookOutputName "british_swing_Eb"
  \score {
    <<
      \new ChordNames {
        \transpose c a { \harmony }
      }
      \new Staff \with { instrumentName = #"Eb" } {
        \transpose c a { \melody }
      }
    >>
    \layout {}
  }
}