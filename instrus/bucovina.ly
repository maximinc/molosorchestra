\version "2.18.2"
\language "english"

\header {
  title = "Bucovina"
  composer = "Shantel"
}

global = {
  \set Score.skipBars = ##t
  \time 2/4
}

theme={
  \tempo 4 = 120
  \key g \minor
  
  
  \relative c''{
    s4
    \new Voice
    <<
    {bf16 bf bf bf d c8 bf16 fs8 fs \bar "||" 
    d4 
    bf'8 bf c16 g8 a16 bf8 g fs4
    fs8 a c16 g8 a16 bf8 g \break
    fs4
    c16 c c c d d8 a16 d8 d d4
    }
    {d16 d d d f ef8 d16 a8 a g4
    d'8 d ef16 d8 c16 d8 bf a4
    a8 c ef16 d8 c16 d8 bf a4
    d,16 d d d fs fs8 d16 fs8 fs g4}
    >>
  }
  
  \relative c''{
    \new Voice
    <<
    {bf16 bf bf bf d c8 bf16 fs8 fs d4 
    bf'8 bf c16 g8 a16 bf8 g \break fs4
    fs8 a c16 g8 a16 bf8 g fs4
    c16 c c c d d8 a16 d8 d  d4
    }
    {d16 d d d f ef8 d16 a8 a g4
    d'8 d ef16 d8 c16 d8 bf a4
    a8 c ef16 d8 c16 d8 bf a4
    d,16 d d d fs fs8 d16 fs8 fs g4}
    >>

  }

  
  
  \relative c''{
    \new Voice
    <<
    
    {d16 d d d f ef8 d16 a8 a \bar "||"
    \mark \markup { \musicglyph #"scripts.segno" }
    \break
    g4
    d'8 d ef16 d8 c16 d8 bf a4
    a8 c ef16 d8 c16 d8 bf a4
    d,16 d d d fs fs8 d16 fs8 fs \break g4}
    
    {bf16 bf bf bf d c8 bf16 fs8 fs d4 
    bf'8 bf c16 g8 a16 bf8 g fs4
    fs8 a c16 g8 a16 bf8 g fs4
    c16 c c c d d8 a16 d8 d d4
    }
    
    { d16 d d d f ef8 d16 a8 a g4
    d'8 d ef16 d8 c16 d8 bf a4
    a8 c ef16 d8 c16 d8 bf a4
    d,16 d d d fs fs8 d16 fs8 fs g4}
    >>
  }
  

  \relative c''{
    \new Voice
    <<
    {d16 d d d f ef8 d16 a8 a g4
    d'8 d ef16 d8 c16 d8 bf \break a4
    a8 c ef16 d8 c16 d8 bf a4
    d,16 d d d fs^"To solo 1-2-3 and coda" fs8 d16 fs8 
    \mark \markup { \musicglyph #"scripts.coda" } 
    fs 
    
    \break 
    \mark \markup { \musicglyph #"scripts.coda" 1 } 
    }
    
    {bf16 bf bf bf d c8 bf16 fs8 fs d4 
    bf'8 bf c16 g8 a16 bf8 g fs4
    fs8 a c16 g8 a16 bf8 g fs4
    c16 c c c d d8 a16 d8 d \bar "||" 
    }
    
    { d16 d d d f ef8 d16 a8 a g4
    d'8 d ef16 d8 c16 d8 bf a4
    a8 c ef16 d8 c16 d8 bf a4
    d,16 d d d fs fs8 d16 fs8 fs }
    >>
  }
  
  
  \relative c'{
    <<
      {<g d' g>4}
      \\
      {r16^"Solo 1" d'8 d16 }
    >>
    d8 ef16 d16~ d4~ d16 bf c d16~ d d c bf c d d d~ d d8. d16 ef d c \break  d4 
    r8. d16 g af g gf f8 ef16 d~d d8. c16 d c bf a8 a a16 bf8 c16~ c8 d8
    r16 d g d' 
    \break
    df8 c16 bf~ bf g bf a~ a g8 d16~ d8 cs16 d f gf f ef d c bf 
    <<
      {r16 
       \break
       r^"Kenza" fs' a c}
      \\
      {d,16~ d4}
    >>
    ef'16 fs d ef fs g a g  fs g fs ef  d ef d c d16 c32 bf a16 bf32 a g4
  }
  \relative c''{
    \new Voice
    <<
    {d16 d d d f ef8 d16 a8^"D.S. to solo 2" a }
    {bf16 bf bf bf d c8 bf16 fs8 fs }
    { d16 d d d f ef8 d16 a8 a }
    >>
  }
  \bar "||"
  
  \break
  \relative c'{
    \mark \markup { \musicglyph #"scripts.coda" 2 } 
    <g d' g>8.^"Solo 2" bf16~ bf8 d8
    g,8. bf16~ bf8 d
    g,8. bf16~ bf8 d
    fs,8. a16~ a8 c
    fs,8. a16~ a8 c
    fs,8. a16~ a8 c
    fs,8. a16~ a8 c
    g8. bf16~ bf8 d
    g,8. bf16~ bf8 d
    g,8. bf16~ bf8 d
    g,8. bf16~ bf8 d
    fs,8. a16~ a8 c
  }
  \break
  \relative c{
    r16^"Kenza" fs' a c
    ef16 fs d ef fs g a g fs g fs ef d ef d c d16 c32 bf a16 bf32 a 
    g4
  }
  \relative c''{
    \new Voice
    <<
    {d16 d d d f ef8 d16 a8^"D.S. to solo 3" a }
    {bf16 bf bf bf d c8 bf16 fs8 fs }
    { d16 d d d f ef8 d16 a8 a }
    >>
  }
  
  \bar "||"
  \break
  \relative c'{
    \mark \markup { \musicglyph #"scripts.coda" 3 } 
    <g d' g>4
  }
  \relative c''{
    <<
    {r4^"Solo 3" r16 g a bf c d ef c d2 r16 g, a bf c d ef d c2 r16 g a bf c d ef c d2
    r16 g, a bf c d ef c d2 r16 g, a bf c d ef c}
    \\
    {r4 r2 r16 g a bf c d ef c d2 r16 g, a bf c d ef d c2 r16 g a bf c d ef c d2
    r16 g, a bf c d ef c d2}
    >>
    <<
      { d2 r16 g, a bf c d ef d c2 }
      \\
      {\stemUp r16 g a bf c d ef c d2 r16 g, a bf c d ef d 
       <<
         {c2}
         \\
         {r16 \stemUp fs,^"Kenza" a c ef fs d ef}
       >>}
      \\
      {\stemDown
       g,,16 a bf g a bf g a bf g a bf g a bf8
       fs16 g a fs g a fs g a fs g a fs g a8  }
    >>
  }
  \relative c''{
    fs16 g a g fs g fs ef d ef d c d16 c32 bf a16 bf32 a g4
    r4 r2 r r r r r
  }
  \relative c''{
   r4   
  \new Voice
  <<
  {d16 d d d f^"D.C. (double bar) al coda"  ef8 d16 a8 a }
  {bf16 bf bf bf d c8 bf16 fs8 fs }
  { d16 d d d f ef8 d16 a8 a }
  >>
  \bar "||"
  }
  
  \relative c'{
    \mark \markup { \musicglyph #"scripts.coda" } 
    <g d' g>4 r 
    \bar "|."
  }
}

bass={
  \key g \minor
  \clef F
  \relative c'{
    s4 r4 r2 r r r r r r

    r2 g4 d g
    bf g bf a
    d, a' d a
    d, a' d, g

    
    r4 g4 d \break
    \mark \markup { \musicglyph #"scripts.segno" }
    g
    bf g bf a
    d, a' d a
    d, a' d, g

    r4 g4 d g
    bf g bf a
    d, a' d a
    d, a'^"To solo 1-2-3 and coda" 
    
    \mark \markup { \musicglyph #"scripts.coda" }
    d,
    
    \bar "||" 
    
  }

  \break
  \relative c'{
    \mark \markup { \musicglyph #"scripts.coda"1 }  
    g4^"Solo 1" d g bf g bf 
    a d, a' d, a' d, a' d, a' d,
    g d g d
    g bf a d a d a d a d g, r g d^"D.S. to solo 2"
  }
  \break
  
  \relative c'{
    \mark \markup { \musicglyph #"scripts.coda"2 }  
    g^"Solo 2" d g bf g bf
    a d a d a d a d
    g, d g d g bf 
    g bf a d a d a d a d g, r g d^"D.S. to solo 3"
  }
  \break
  \relative c'{
    \mark \markup { \musicglyph #"scripts.coda"3 }  
    g^"Solo 3" d g d g bf g bf
    a d a d a d a d
    g, d g d g bf g bf 
    a d a d a d a d
    g, d g bf g bf
    a d a d a d a d
    g, d g d  
    
    \mark \markup { \musicglyph #"scripts.coda"}  
    g r \bar "|."
  }
}

cgm={r8 <g bf d>8}

cd={r8 <fs a d>8}

comp={
  \key g \minor
  
  s4 r4 r2 r r r r r r r

  \relative c'{
    \cgm \cd \cgm
    \cgm \cgm \cgm \cd
    \cd  \cd \cgm \cd 
    \cd
    \relative c'{
      r8 \tuplet 3/2 {ef16 d c } d8  \tuplet 3/2 {c16 bf a} g4
    }
  }

  \relative c'{
    r4 \cgm \cd
    \mark \markup { \musicglyph #"scripts.segno" }
    \cgm
    \cgm \cgm \cgm \cd
    \cd  \cd \cgm \cd 
    \cd
    \relative c'{
      r8 \tuplet 3/2 {ef16 d c } d8  \tuplet 3/2 {c16 bf a} g4
    }
    r4 \cgm \cd \cgm
    \cgm \cgm \cgm \cd
    \cd  \cd \cgm \cd 
    \cd
    \relative c'{
      r8^"To solo 1-2-3 and coda" \tuplet 3/2 {ef16 d c } d8  
      
      \tuplet 3/2 {c16 bf  \mark \markup { \musicglyph #"scripts.coda" } a}  
      
      \break
      \mark \markup { \musicglyph #"scripts.coda" 1 } 
    }
    
    g8^"Solo 1" <g bf d> \cgm \cgm \cgm \cgm \cgm
    \cd \cd \cd \cd \cd \cd \cd \cd \cd \cd 
    \cgm \cgm \cgm \cgm \cgm \cgm
    \cd \cd \cd \cd \cd \cd \cd \cd  <g bf d>8 r r4
    \cgm r8^"D.S. to solo 2"  <fs a d>8
    \bar "||"
    
    \break
    \mark \markup { \musicglyph #"scripts.coda" 2 } 
    r8^"Solo 2" <g bf d>8 \cgm \cgm \cgm \cgm \cgm 
    \cd \cd \cd \cd \cd \cd \cd \cd
    \cgm \cgm \cgm \cgm \cgm \cgm \cgm \cgm
    \cd \cd \cd \cd \cd \cd \cd \cd  <g bf d>8 r r4
    \cgm r8^"D.S. to solo 3"  <fs a d>8
    \break
  }
  \relative c'{
    \mark \markup { \musicglyph #"scripts.coda" 3 } 
    r8^"Solo 3" <g bf d>8 \cgm \cgm \cgm \cgm \cgm \cgm \cgm
    \cd \cd \cd \cd \cd \cd \cd \cd
    \cgm \cgm \cgm \cgm \cgm \cgm \cgm \cgm
    \cd  \cd  \cd  \cd \cd \cd \cd \cd
    <g bf d>8 r r4
  
    r2 r2 r2 r2 r2 r2 r2 \cgm \cd 
    
    
    <g bf d>4 r
    
  }
}

grid=\chordmode{
  s4
  g:m d4:7 g1:m
  d2.:7 g4:m
  d1:7 g4:m 
}


chordNames={
  s4 
  \grid
  \grid
  \grid
  \grid
  \chordmode{
    s4 g2:m g:m
    d:7 d:7 d:7 d:7 d:7
    g:m g:m g:m g:m 
    d:7 d:7 d:7 g:m g4:m d:7
    
    g2:m g:m g:m d:7 d:7 d:7 d:7 g:m g:m g:m g:m
    d:7 d:7 d:7 d:7 g:m g4:m d:7
    
    g2:m g:m  g:m  g:m
    d:7 d:7 d:7 d:7
    g:m g:m g:m g:m d:7 d:7 d:7 d:7
    g:m g:m g:m
    d:7  d:7  d:7  d:7 
    g:m g4:m d:7
    g4:m s
  }
}


\book{
  \bookOutputName "bucovina_all"
  \score {
    <<
    \global 
      \new ChordNames { 
        \transpose c c { \chordNames }
      }
    
      \new Staff {
        \transpose c c { \theme }
      }
    
      \new Staff {
        \transpose c c { \comp }
      }
    
      \new Staff {
        \transpose c c {  \bass }
      }
    >>
    \layout { }
  }
}

\book{
  \bookOutputName "bucovina_Bb"
  \score {
    <<
    \global 
      \new ChordNames { 
        \transpose c d { \chordNames }
      }
    
      \new Staff {
        \transpose c d { \theme }
      }
    
      \new Staff {
        \transpose c d { \comp }
      }    
    >>
    \layout { }
  }
}

\book{
  \bookOutputName "bucovina_C"
  \score {
    <<
    \global 
      \new ChordNames { 
        \transpose c c { \chordNames }
      }
    
      \new Staff {
        \transpose c c { \theme }
      }
    
      \new Staff {
        \transpose c c { \comp }
      }    
    >>
    \layout {  #(layout-set-staff-size 18) }
  }
}

\book{
  \bookOutputName "bucovina_Eb"
  \score {
    <<
    \global 
      \new ChordNames { 
        \transpose c a { \chordNames }
      }
    
      \new Staff {
        \transpose c a { \theme }
      }
    
      \new Staff {
        \transpose c a { \comp }
      }    
    >>
    \layout { }
  }
}


\book{
  \bookOutputName "bucovina_C_clef_ut"
  \score {
    <<
    \global 
      \new ChordNames { 
        \transpose c c { \chordNames }
      }
    
      \new Staff {
        \clef C
        \transpose c c { \theme }
      }
    
      \new Staff {
        \clef C
        \transpose c c { \comp }
      }    
    >>
    \layout { }
  }
}

\book{
  \bookOutputName "bucovina_Bb_bass"
  \score {
    <<
    \global 
      \new ChordNames { 
        \transpose c d { \chordNames }
      }
    
      \new Staff {
        \transpose c d { \theme }
      }
    
      \new Staff {
        \transpose c d { \bass }
      }    
    >>
    \layout { }
  }
}

\book{
  \bookOutputName "bucovina_C_bass"
  \score {
    <<
    \global 
      \new ChordNames { 
        \transpose c c { \chordNames }
      }
    
      \new Staff {
        \transpose c c { \theme }
      }
    
      \new Staff {
        \transpose c c { \bass }
      }    
    >>
    \layout { }
  }
}
