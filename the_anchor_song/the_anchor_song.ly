
\version "2.18.2"
\language "english"

\paper {
  system-system-spacing #'basic-distance = #1
}



\header {
  title = "The Anchor Song"
  composer = "Björk"
}


words= \lyricmode {
  a
}

theme={
  r1 r r r r r r r
  
  \repeat volta 2{
    \relative c'{
      r4 c e f8 g g4 g2.
    }
    r1 r1
    \time 5/4 r1 r4
    \time 4/4
    \relative c'{
      r4 c e f8 g g4 g2.
    }
    r1 r r
  }

  \relative c'{
    r4 c e f8 g g4 g2.~ g1~ g2 g4 c~ c4. g8~g8 g8~ g8 c,~ c d4. r2
    
    c4 d~ d8 c d4
    c f e d r2 c4. d8 c4 g'~g8 e4.
  }
  \bar "||"

}

alto={
  \relative c''{
    r4 fs b, cs d1
    r4 fs b, cs d4. b gs4
    r4 fs' b, cs d1
    e1 \appoggiatura fs8
    gs1 \fermata    
  }
  %\bar "||"
  \break
  
  \repeat volta 2{
    r1 r1
    \relative c'''{
      d4 cs b a r fs gs a
      \time 5/4
      e4. fs8~ fs2.
      \time 4/4
      r1
      r
    }
    \relative c'''{
      d4 cs b a
    }
    \relative c''{
      \tuplet 3/2 {r8 r d e fs r b, cs d~} d4~ d fs2.
    }
  }
  
  \relative c'''{
    r1 r4 a2.~ a4 gs2.~ gs4 e2.~ e4 r4 r2
    r1 r r r r
  }

  \relative c''{
    r4 fs b, cs d1
    r4 fs b, cs d4. b gs4
    r4 fs' b, cs d1
    e1
    \appoggiatura fs8 
    gs1
  }

  \break
  \repeat volta 2{
    r1 r1
    \relative c'''{
      d4 cs b a r fs gs a
      \time 5/4
      e4. fs8~ fs2.
      \time 4/4
      r1
      r
    }
    \relative c'''{
      d4 cs b a
    }
    \relative c''{
      \tuplet 3/2 {r8 r d e fs r b, cs d~} d4~ 
      \time 7/4
      d fs2.~ fs2.
    }
  }


}

tenor={
  \relative c''{
    r1
    d1 r
    b4. a g4 
    r
    a'4 cs, d e1
    fs2. e4
    fs1 \fermata
  }
  
  \repeat volta 2{
    \relative c''{
      r1 r1 
      d1
      r4 a2.~ 
      \time 5/4
      a4. e'8~ e2.
      \time 4/4
      r1 r1
      d1 b2 a2~ a4 d2.
    }
  }

  \relative c''{
    r1 r4 fs2.~ fs4 a2.~ a4 e2.~ e4 r4 r2
    r1 r r r r
  }

  \relative c''{
    r1
    d1 r
    b4. a g4 
    r a'4 cs, d 
    %r1
    e'1
    fs2. e4
    fs1
  }


  \repeat volta 2{
    \relative c''{
      r1 r1 
      d1
      r4 a2.~ 
      \time 5/4
      a4. e'8~ e2.
      \time 4/4
      r1 r1
      d1 b2 a2~ 
      \time 7/4
      a4 cs2. d2.
    }
  }

}

baryton={
  
  \relative c''{
    r2 r4 
    fs4~ fs e2.
    r1
    e4. d b4
    r1
    a'1 gs a,\fermata
  }
  
  \repeat volta 2{
    \relative c'{
      r1 r1 b'
      r4 fs2.~ 
      \time 5/4
      fs4. cs'8~ cs2.
      \time 4/4
      r1 r1 b1~ b2 b2~ b4 gs'2.
    }
  }

  \relative c'''{
    r1 r4 a2.~ a4 gs2.~ gs4 e2.~ e4 r4 r2
    r1 r r r r
  }

  \relative c''{
    r2 r4 
    fs4~ fs e2.
    r1
    e4. d b4
    r1
    a'1 gs a,,
  }

  \repeat volta 2{
    \relative c'{
      r1 r1 b'
      r4 fs2.~ 
      \time 5/4
      fs4. cs'8~ cs2.
      \time 4/4
      r1 r1 b1~ b2 b2~ 
      \time 7/4
      b4 gs'2.~ gs2.
    }
  }

}



\book {
  \bookOutputName "the_anchor_song_C"
  \score{
  <<
    \time 4/4
    

    \new Staff \with { instrumentName = #"Voice" } {
      \tempo 4=180
      \transpose c c { \theme }
      \addlyrics { \words }
    }

    \new Staff \with { instrumentName = #"Alto" } {
      \transposition ef
      \set Staff.midiInstrument = "alto sax"
      \transpose c c {
        \key fs \minor
        \alto
      }
    }

    \new Staff \with { instrumentName = #"Tenor" } {
      \transposition bf,
      \set Staff.midiInstrument = "tenor sax"
      \transpose c c { 
        \key b \minor
        \tenor
      }
    }

    \new Staff \with { instrumentName = #"Baritone" } {
      \transposition ef,
      \set Staff.midiInstrument = "baritone sax"
      \transpose c c { 
        \key fs \minor
        \baryton
      }
    }

  >>
  \layout { }
  \midi { } 
  }
}

