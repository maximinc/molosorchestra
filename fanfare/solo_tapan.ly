\version "2.18.2"
\language "english"

\header {
  title = "Solo Tapan"
  composer = "Kocani"
}


theme={
  \key f \minor
  \time 4/4
  \tempo 4 = 180

  \set Score.markFormatter = #format-mark-circle-letters
  \mark \default

  \repeat volta 2{
    \relative c'{
      f2 ef4 gf f r r r
      f4 f ef gf
    }
  }
  \alternative {
    \relative c'{f r r r}
    \relative c'{f r r r8 f16 f }
  }
  \mark \default
  \bar ".|:"
  \relative c'{
    \parenthesize f4^"1st time only"
    
    <<{
        ef8 f f4 g af8 bf~ bf4 \tuplet 3/2 {af8 bf af} g4
        af8 bf~ bf4 \tuplet 3/2 {af8 bf af} g4
        
        \tuplet 3/2 {af8 bf af} g f f ef f4
      }
     
      {
        g8 af af4 bf4 c8 df~ df4 \tuplet 3/2 {c8 df c} bf4
        c8 df~ df4 \tuplet 3/2 {c8 df c} bf4
        \tuplet 3/2 {c8 df c} bf8 af af  g \mark "x4" af4 
      }
    >>
  }
  
  \bar ":|.|:" 
  \mark \default
  %\bar ".|:"
  \break
  \relative c''{
    <<
      {
        af8 bf af bf \tuplet 3/2 {af bf af } g4
      }
      {
        c8 df c df \tuplet 3/2 {c df c } bf4
      }
    >>
  }

  \relative c'{
    <<
      {
        af8 bf af bf \tuplet 3/2 {af bf af } g4
      }
      {
        c8 df c df \tuplet 3/2 {c df c } bf4
      }
    >>
  }

  \relative c''{
    <<
      {
        af8 bf af bf \tuplet 3/2 {af bf af } g4
        \tuplet 3/2 {af8 bf af} g f f ef f4
      }
      {
        c'8 df c df \tuplet 3/2 {c df c } bf4
        \tuplet 3/2 {c8 df c} bf8 af af  g af4      
      }
    >>
  }
  \bar ":|.|:" 
  \break

  \mark \default
  \repeat volta 2{
    \relative c'{
      <<
        {
          r4 r4 ef f8 g af4 af af af af8 a~ a a a4 a \tuplet 3/2 {af8 bff af a bf a} af g af4
          \break
          r  r c4 df ef df c bf af8 bf~ bf4 \tuplet 3/2 { af8 bf af } g4
        }
        {
          r4 r4 g4 af8 bf c4 c c c c8 cs8 ~ cs cs cs4 cs4 \tuplet 3/2 {c8 df c cs d cs} c8 b c4
          r4 r4 ef f g f ef df c8 df~ df4 \tuplet 3/2 {c8 df c } bf4 
        }
      >>
    }
  }
  \alternative{
    %\set Score.repeatCommands = #'((volta "1,3"))
    <<
    \relative c''{ \tuplet 3/2 { af8 bf af } g8 f f ef f4 }
    \relative c''{ \tuplet 3/2 {c8 df c} bf af af g af4}
    >>
    %\bar ":|."
    %\set Score.repeatCommands = #'((volta "2,4"))
    <<
    \relative c''{ \tuplet 3/2 { af8 bf af } g8 f f4 r }
    \relative c''{ \tuplet 3/2 {c8 df c } bf af af4}
    >>
  }
  \break
  \mark \default
  \repeat volta 1{
    \relative c''{
      <<
        {
          af8 bf~ bf4 \tuplet 3/2 {af8 bf af} g4
          \tuplet 3/2 {af8 bf af} g f f g af bf
          af8 bf~ bf4 \tuplet 3/2 {af8 bf af} g4
          
        }
        {
          c8 df~ df4 \tuplet 3/2 {c8 df c} bf4
          \tuplet 3/2 {c8 df c} bf af af bf c df
          c8 df~ df4 \tuplet 3/2 {c8 df c} bf4
        }
      >>
    }
  }
  %\alternative{
    \set Score.repeatCommands = #'((volta "1,3"))
    <<
      \mark \markup { \musicglyph #"scripts.coda" }
      \relative c''{ \tuplet 3/2 {af8 bf af} g f f ef f4 }
      \relative c''{ \tuplet 3/2 {c8 df c} bf af af g af4}

    >>
    
    \set Score.repeatCommands = #'((volta "2,4") end-repeat)
    %\set Score.repeatCommands = #'((volta "2,4"))
    <<
      %\coda
      \relative c''{ \tuplet 3/2 {af8 bf af} g f f2 }
      \relative c''{ \tuplet 3/2 {c8 df c} bf af af2 }
    >>
    \set Score.repeatCommands = #'((volta #f))
  %}
  
  \bar ":|.|:"
  
  \break
  \clef F
  
  \relative c,{ f4.^"solo dorian mode" af8 c4 ef8 (f)  f,4. af8 c4 ef8 (f) }
  \relative c,{ f4. af8 c4 ef8 (f)  f4. f8 f8^"Ad lib" f16 ef c8 \mark "D.C. al coda" af }
  
  
  \bar ":|."
  \break
  \mark \markup { \musicglyph #"scripts.coda" }
  
  \clef G
  <<
    \relative c''{ \tuplet 3/2 {af8 bf af} g f f ef f4 ~ f1 ~ f f4->}
    \relative c''{ \tuplet 3/2 {c8 df c} bf af af g af4 ~ af1 ~ af af4->}
  >>
  r4 r r 
  \bar "|."

}



comp={
   \key f \minor
   
   \repeat volta 2{
    \relative c'{
      f2 ef4 gf f r r r
      f4 f ef gf
    }
  }
  \alternative {
    \relative c'{f r r r}
    \relative c {f r r r8 <af c f>16 <af c f>16 }
  }
  
  \relative c'{<af c f>4 <af c f>8 <af c f>8 r8 <af c f>16 <af c f>16 <af c f>8 <af c f>8}  
  \relative c'{<af c f>4 <af c f>8 <af c f>8 r8 <af c f>16 <af c f>16 <af c f>8 <af c f>8}  
  \relative c'{<af c f>4 <af c f>8 <af c f>8 r8 <af c f>16 <af c f>16 <af c f>8 <af c f>8}  
  \relative c{<f af df>2 <g bf ef>2}  
  
  \relative c'{<af c f>4 r r2 }

  \relative c'{
    <<
      {
        af8 bf af bf \tuplet 3/2 {af bf af } g4
      }
      {
        c8 df c df \tuplet 3/2 {c df c } bf4
      }
    >>
  }

  \relative c{<f af df>2 <g bf ef>2}  
  \relative c'{<af c f>4 <af c f>8 <af c f>8 r8 <af c f>16 <af c f>16 <af c f>8 <af c f>8}  
  
  \relative c'{<af c f>4 r r2}
  \relative c'{<af c ef>4 <af c ef>8 <af c ef>8 r8 <af c ef>16 <af c ef>16 <af c ef>8 <af c ef>8}    
  \relative c'{<a cs e>4 <a cs e>8 <a cs e>8 r8 <a cs e>16 <a cs e>16 <a cs e>8 <a cs e>8}    
  \relative c'{<af c ef>4 <a cs e>4 <a cs e>4 <af c ef>4 <af c ef>4 r r2}
  
  \relative c{<f bf df>4 <f bf df>8 <f bf df>8 r <f bf df>16 <f bf df> <f bf df>8 <f bf df>8}

  \relative c{r8 <f af df>16 <f af df> <f af df>8 <f af df>8
              r8 <g bf ef>16 <g bf ef> <g bf ef>8 <g bf ef>8}  

  \relative c'{<af c f>4 <af c f>8 <af c f>8 r8 <af c f>16 <af c f>16 <af c f>8 <af c f>8}  
  
  \relative c'{<af c f>4 <af c f>8 <af c f>8 r8 <af c f>16 <af c f>16 <af c f>8 <af c f>8}  
  
  \relative c'{<af c f>4 <af c f>8 <af c f>8 r8 <af c f>16 <af c f>16 <af c f>8 <af c f>8}  
  \relative c'{<af c f>4 <af c f>8 <af c f>8 r8 <af c f>16 <af c f>16 <af c f>8 <af c f>8}  
  \relative c'{<af c f>4 <af c f>8 <af c f>8 r8 <af c f>16 <af c f>16 <af c f>8 <af c f>8}  
  \relative c'{<af c f>4 <af c f>8 <af c f>8 r8 <af c f>16 <af c f>16 <af c f>8 <af c f>8}  
  \relative c'{<af c f>4 <af c f>8 <af c f>8 r8 <af c f>16 <af c f>16 <af c f>8 <af c f>8}  
   
  
  \relative c'{r4 r8 <af c f>8 r2 r4 r8 <af c f>8 r2 r4 r8 <af c f>8 r2 r4 r8 <af c f>8 r2}  
  
  \relative c'{<af c f>4 <af c f>8 <af c f>8 r8 <af c f>16 <af c f>16 <af c f>8 <af c f>8}  
  \relative c'{<af c f>4 <af c f>8 <af c f>8 r8 <af c f>16 <af c f>16 <af c f>8 <af c f>8}  
  \relative c'{<af c f>4 <af c f>8 <af c f>8 r8 <af c f>16 <af c f>16 <af c f>8 <af c f>8}  
  \relative c'{<af c f>4-> r r r}
}


bass={
   \key f \minor
   \clef F
   \repeat volta 2{
    \relative c{
      f2 ef4 gf f r r r
      f4 f ef gf
    }
  }
  \alternative {
    \relative c{f r r r}
    \relative c{f r r r}
  }
  
  \relative c{f4. af8 c4 af8 c}
  \relative c{f4. af8 c4 af8 c}
  \relative c{f4. af8 c4 af8 c}
  \relative c{df2 ef}
  
  \relative c{ f4 r r r af8 bf af bf \tuplet 3/2 {af bf af } g4}
  \relative c{df2 ef}
  \relative c{f4. af8 c4 af8 c}
  \relative c{ f4 r r r af4. c8 ef4 c8 ef a,4. cs8 e4 cs8 e af,4 a a af}
  \relative c'{ af4 r r r}
  \relative c'{bf4. bf bf4 df,2 ef}
  \relative c{f4. af8 c4 af8 c}
  \relative c{f4. af8 c4 af8 c}
  
  \relative c{f4. af8 c4 af8 c}
  \relative c{f4. af8 c4 af8 c}
  \relative c{df2 ef}
  \relative c{f4. af8 c4 af8 c}
  \relative c{f4. af8 c4 af8 c}
  
  \relative c,{ f4. af8 c4 ef8 (f)  f,4. af8 c4 ef8 (f) }
  \relative c,{ f4. af8 c4 ef8 (f)  f4. f8 f8 f16 ef c8 af }
  \relative c{f4. af8 c4 af8 c}
  \relative c{f4. af8 c4 af8 c}
  \relative c{f4. af8 c4 af8 c}
  \relative c{f4 r r r}
  
}

chordNames=\chordmode{
  s1 s s s s
  f:m f:m f:m df2 ef 
  
  f1:m s1 df2 ef f1:m
  
  f1:m  af a af4 a a af af1
  bf:m df2 ef f1:m f:m
  
  f:m f:m f2:m/df f:m/ef f1:m f1:m
}

structure=\markup{
 % Structure: thème x N, terminer avec mesures 21 à 24 x 3
}

\book {
  \bookOutputName "solo_tapan_C"
  \score{
  <<
    \new ChordNames { 
      \transpose c c { \chordNames }
    }

    \new Staff \with { instrumentName = #"Theme (C)" } {
      \transpose c c { \theme }
    }

    \new Staff \with { instrumentName = #"Comp (C)" } {
      \transpose c c { \comp }
    }

  >>
  \layout { #(layout-set-staff-size 16) }
  %\midi { }
  }
  %\structure
}

\book {
  \bookOutputName "solo_tapan_Bb"
  \score{
  <<
    \new ChordNames { 
      \transpose c d { \chordNames }
    }

    \new Staff \with { instrumentName = #"Theme (Bb)" } {
      \transpose c d { \theme }
    }

    \new Staff \with { instrumentName = #"Comp (Bb)" } {
      \transpose c d { \comp }
    }

  >>
  \layout { #(layout-set-staff-size 16) }
  }
  %\structure
}

\book {
  \bookOutputName "solo_tapan_Eb"
  \score{
  <<
    \new ChordNames { 
      \transpose c a { \chordNames }
    }

    \new Staff \with { instrumentName = #"Theme (Eb)" } {
      \transpose c a { \theme }
    }

    \new Staff \with { instrumentName = #"Comp (Eb)" } {
      \transpose c a { \comp }
    }

  >>
  \layout { #(layout-set-staff-size 16) }
  %\midi { }
  }
  %\structure
}
 



\book {
  \bookOutputName "solo_tapan_C_bass"
  \score{
  <<
    \new ChordNames { 
      \transpose c c { \chordNames }
    }

    \new Staff \with { instrumentName = #"Theme (C)" } {
      \transpose c c { \theme }
    }

    \new Staff \with { instrumentName = #"Bass (C)" } {
      \transpose c c { \bass }
    }

  >>
  \layout { #(layout-set-staff-size 16) }
  %\midi { }
  }
  %\structure
}

\book {
  \bookOutputName "solo_tapan_Bb_bass"
  \score{
  <<
    \new ChordNames { 
      \transpose c d { \chordNames }
    }

    \new Staff \with { instrumentName = #"Theme (Bb)" } {
      \transpose c d { \theme }
    }

    \new Staff \with { instrumentName = #"Bass (Bb)" } {
      \transpose c d { \bass }
    }

  >>
  \layout { #(layout-set-staff-size 16) }
  %\midi { }
  }
  %\structure
}
