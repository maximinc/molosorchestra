\version "2.18.2"
\language "english"

\header {
  title = "Talijanska"
  composer = "Goran Bregović Orchestra"
}


theme={
  \key g \minor
  \time 3/4
  \tempo 4 = 180
  \mark \default

  \relative c''{
    d4 d8 d d d d ef d c bf a bf4 bf8 bf bf bf bf a g bf a g 
    %\break
    fs g a bf c d ef d cs d e fs g4 r r r2.
  }
  \mark \default

  \bar "||"
  \break
  \relative c''{
    \tuplet 3/2 {bf8 c bf} g4 g8 a 
    \tuplet 3/2 {bf8 c bf} g4 g8 a 
    \tuplet 3/2 {bf8 c bf} g4 g8 bf d2.
    \break
    \tuplet 3/2 {ef8 f ef} c4 c8 d
    \tuplet 3/2 {ef8 f ef} c4 c8 d
    \tuplet 3/2 {ef8 f ef} c4 c8 ef g2.
    \break
  }
  \relative c''{
    fs8 g a g fs ef d ef d c bf a g a bf a bf c d2.
    \break
    \mark \markup { \musicglyph #"scripts.segno" }
    ef8 d ef c d ef d cs d bf c d cs d ds e f fs g4^"Fine" r r
  }
  \bar "||"
  %\break
  \mark \default
  \key ef \major
  \relative c''{
    g4 g8 af bf4 g bf g 
    af4 af8 bf af g f4 r r
    %\break
    af4 af8 bf c4 af c af
    bf bf8 c bf af g4 r r
    %\break
    ef'4 ef8 d c ef d bf g4 r
    c4 c8 bf af c bf g ef4 r
    %\break
    
    f8 g af g f g af g f g af bf c bf a bf c d ef4 r r
  }
  \bar "|."
}

dchord=\relative c{r <fs a c> <fs a c>}
gmchord=\relative c'{r <g bf d> <g bf d>}
cmchord=\relative c'{r <g c ef> <g c ef>}
efchord=\relative c'{r <g bf ef> <g bf ef>}
bfchord=\relative c'{r <af d f> <af d f>}
afchord=\relative c'{r <af c ef> <af c ef>}


comp={
  \key g \minor
  \relative c'{
    <fs a c>4 \staccato r r
    <fs a c>4 \staccato r r
    <g bf d>  \staccato r r
    <g bf d>  \staccato r r
    \dchord
    \dchord
    \gmchord
    \gmchord
  }
  \relative c'{
    \gmchord \gmchord \gmchord \gmchord
    \cmchord \cmchord \cmchord \cmchord
    \dchord \dchord
    \gmchord \gmchord 
    \cmchord \gmchord \dchord \gmchord
  }
  \relative c'{
    \efchord \efchord \bfchord \bfchord
    \bfchord \bfchord \efchord \efchord
    \cmchord \gmchord \afchord \efchord
    \bfchord \bfchord \bfchord \efchord
  }
}

chordNames=\chordmode{
  d2.:7 d:7 g:m g:m
  d:7 d:7 g:m s
  
  g:m s g:m s
  c:m s c:m s
  d:7 s g:m s
  c:m g:m d:7 g:m
  
  ef s bf:7 s bf:7 s ef s
  c:m g:m af ef
  bf:7 bf:7 bf:7 ef s
}

structure=\markup{
  Structure: thème x N, terminer avec mesures 21 à 24 x 3
}

\book {
  \bookOutputName "talijanska_C"
  \score{
  <<
    \new ChordNames { 
      \transpose c c { \chordNames }
    }

    \new Staff \with { instrumentName = #"Theme (C)" } {
      \transpose c c { \theme }
    }

    \new Staff \with { instrumentName = #"Comp (C)" } {
      \transpose c c { \comp }
    }

  >>
  \layout { #(layout-set-staff-size 16) }
  %\midi { }
  }
  \structure
}

\book {
  \bookOutputName "talijanska_Bb"
  \score{
  <<
    \new ChordNames { 
      \transpose c d { \chordNames }
    }

    \new Staff \with { instrumentName = #"Theme (Bb)" } {
      \transpose c d { \theme }
    }

    \new Staff \with { instrumentName = #"Comp (Bb)" } {
      \transpose c d { \comp }
    }

  >>
  \layout { #(layout-set-staff-size 16) }
  %\midi { }
  }
  \structure
}

\book {
  \bookOutputName "talijanska_Eb"
  \score{
  <<
    \new ChordNames { 
      \transpose c a { \chordNames }
    }

    \new Staff \with { instrumentName = #"Theme (Eb)" } {
      \transpose c a { \theme }
    }

    \new Staff \with { instrumentName = #"Comp (Eb)" } {
      \transpose c a { \comp }
    }

  >>
  \layout { #(layout-set-staff-size 16) }
  %\midi { }
  }
  \structure
}