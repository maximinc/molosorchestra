\version "2.18.2"
\language "english"

\header {
  title = "Goodbye Macedonia"
  composer = "Kočani Orkestar"
}


theme={
  \tempo 4=180
  \key ef \minor
  \time 9/8
  \set Timing.beatStructure = #'(2 2 2 2 1)
  \relative c'{
    s4 s s s 
    ef8~ 
  }
  
  \relative c'{
      ef 
      <<
        {bf8 ef gf bf bf af4 gf8 }
        {gf,8 bf ef gf gf f4 ef8 }
      >>
  }
  \break
  \mark \markup { \musicglyph #"scripts.segno" }
  \repeat volta 2{
    \relative c''{
      <<
        {af bf~bf2 r4.
        r8 bf,8 ef gf bf bf af4 gf8 f16 gf af8 ~ af2 r4.
        r4 f8 gf af4 gf f8 gf af~ af2 r4.
        r4 bf8 cf bf af gf4 af8 bf2 r2 r8}

        {f gf~gf2 r4.
        r8 gf,8 bf ef gf gf f4 ef8 d16 ef f8 ~ f2 r4.
        r4 d8 ef f4 ef d8 ef f~ f2 r4.
        r4 gf8 af gf f ef4 f8 gf2 r2 r8}
      >>
          
    }
    \relative c'{
      <<
      {r8 bf8 ef gf bf bf af4 gf8 af bf~bf2 r4.
      r8 bf,8 ef gf bf bf af4 gf8 f16 gf af8 ~ af2 r4.
      r4 f8 gf af4 gf f8 gf af~ af2 r4.
      r4 bf8 cf bf af gf f gf ef2 r2 r8}
      {r8 gf,8 bf ef gf gf f4 ef8 f gf~gf2 r4.
       r8 gf,8 bf ef gf gf f4 ef8 d16 ef f8 ~ f2 r4.
       r4 d8 ef f4 ef d8 ef f~ f2 r4.
       r4 d8 ef d cf bf af bf gf2 r2 r8}
      >>
    }
    
    \relative c''{
      \stemUp 
      r8 
      <<{ef8 ef ef ef df cf4 bf8}
        {cf8 cf cf cf bf af4 gf8}
      >>
      <<
        <af f>1
        \\
        {\stemUp r8 ef'8 ef ef ef df cf4 bf8 }
      >>
      <<
        {af1}
        \\
        {\stemUp r8
         <<{df8 df df df cf bf4 af8}
           {bf8 bf bf bf af gf4 f8}>>
         }
      >>
  
      <<
        {<gf ef>1}
        \\
        {\stemUp r8 df'8 df df df cf bf4 af8}
      >>
  
      <<
        {gf1}
        \\
        {\stemUp r8
           <<{cf8 cf cf cf bf af4 gf8}
             {af8 af af af gf f4 ef8}>>}
      >>
  
      <<
        {<d f>1}
        \\
        {\stemUp r8 cf'8 cf cf cf bf af4 gf8}
      >>
  
      <<
        {f1}
        \\
        {\stemUp r8 
         <<{f8 f gf af4 gf af8}
           {d,8 d ef f4 ef f8}>>}
      >>
  
      <gf bf>2 r2 r8
    }
    \relative c''{
      \stemUp 
      r8 
      <<{ef8 ef ef ef df cf4 bf8}
        {cf8 cf cf cf bf af4 gf8}
      >>
      <<
        <af f>1
        \\
        {\stemUp r8 ef'8 ef ef ef df cf4 bf8 }
      >>
      <<
        {af1}
        \\
        {\stemUp r8
         <<{df8 df df df cf bf4 af8}
           {bf8 bf bf bf af gf4 f8}>>
         }
      >>
  
      <<
        {<gf ef>1}
        \\
        {\stemUp r8 df'8 df df df cf bf4 af8}
      >>
  
      <<
        {gf1}
        \\
        {\stemUp r8
           <<{cf8 cf cf cf bf af4 gf8}
             {af8 af af af gf f4 ef8}>>}
      >>
  
      <<
        {<d f>1}
        \\
        {\stemUp r8 cf'8 cf cf cf bf af4 gf8}
      >>
  
      <<
        {f1}
        \\
        {\stemUp r8 
         <<{f8 f gf af4 gf8 f gf}
           \\
           {d8 d ef f4 ef gf8}>>
        }
      >>
      \once \override Score.RehearsalMark.break-visibility = #begin-of-line-invisible
      \mark \markup { \musicglyph #"scripts.coda" }
      ef2 r2 r8
    }
  }
  \alternative {
    {
      \relative c'{
        r8
        <<
          {bf8 ef gf bf bf af4 gf8 }
          {gf,8 bf ef gf gf f4 ef8 }
        >>	
      }
    }
    {r2 r2 r8}
  }
  \repeat volta 2{
    \mark "Solo"
    r1 r8
    r2 r2\mark "Ad lib" r8
  }
  \break
  r1 r8
  r1 r8
  r1 r8
  r1 r8
  \break
  r1 r8
  r1 r8
  r1 r8
  r1^"End solo" r8

  \bar "||"
  \relative c'{
    r8
    <<
      {bf8 ef gf bf bf af4 
       \mark "D.S. al coda" 
       gf8 }
      {gf,8 bf ef gf gf f4 ef8 }
    >>	
  }
  \bar "||"
  \break
  \mark \markup { \musicglyph #"scripts.coda" }
  \relative c'{
    <ef gf>1 ~ <ef gf>8 ~ <ef gf>2 r4 <gf ef'>8 <gf ef'>8 <gf ef'>8 <gf ef'>4
  }
  r4 r r r8 
  \bar "|."
}

compEf={
  \relative c'{
    r8 <gf ef'> r8 <gf ef'> r8 <gf ef'> r8 <gf ef'>  <gf ef'>
  }
}
compBf={
  \relative c'{
    r8 <af d> r8 <af d> r8 <af d> r8 <af d>  <af d>
  }
}
compAf={
  \relative c'{
    r8 <af cf> r8 <af cf> r8 <af cf> r8 <af cf>  <af cf>
  }
}

comp={
  \key ef \minor
  s2 s2 \relative c'{ ef8 }
  
  r2 r2 r8
  \mark \markup { \musicglyph #"scripts.segno" }
  \repeat volta 2{
    \compEf \compEf \compBf \compBf
    \compBf \compBf \compEf \relative c'{ <gf ef'>4 r4 r2 r8} 
    \compEf \compEf \compBf \compBf
    \compBf \compBf \compEf \relative c'{ <gf ef'>4 r4 r2 r8} 
    \compAf \compAf \compEf \compEf
    \compBf \compBf \compEf \relative c'{ <gf ef'>4 r4 r2 r8} 
    \compAf \compAf \compEf \compEf
    \compBf \compBf 

    \once \override Score.RehearsalMark.break-visibility = #begin-of-line-invisible
    \mark \markup { \musicglyph #"scripts.coda" }

    \compEf 
  }
  \alternative {
    {\relative c'{ <gf ef'>4 r4 r2 r8} }
    {\compEf}
  }
  \repeat volta 2{
    \compEf \compEf
  }
  \compBf \compBf \compBf \compBf
  \compEf \compEf \compEf \compEf
  \relative c'{<gf ef'>4 r4 r4 r4 
               \mark "D.S. al coda" 
               r8}

  \bar "||"
  \break
  \mark \markup { \musicglyph #"scripts.coda" }
  
  \compEf 
  \relative c'{
    r8 <gf ef'> r8 <gf ef'> r8 <gf ef'> <gf ef'> <gf ef'>  <gf ef'> <gf ef'>4
    r4 r r r8 
  }
  \bar "|."
}

bass={
  \clef F
  \key ef\minor
  \set Timing.beatStructure = #'(2 2 2 2 1)
  \relative c,{
    s4 s s s 
    ef8
    r1 r8 
  }
  \mark \markup { \musicglyph #"scripts.segno" }
  \repeat volta 2{
    \relative c,{
      ef4 gf bf ef  ef8
      ef4 bf gf ef gf8
      bf4 d f d bf8
      bf4 f bf f bf8
      bf4 d f d bf8
      bf4 bf4 c d f8
      ef,    4 ef gf bf gf8
      ef4 r4 r r r8
    }
    \relative c{
      ef4 ef ef bf ef8 
      ef4 bf ef bf bf8
      bf4 d f d bf8 
      bf4 f bf f bf8
      bf4 d f d bf8
      bf4 bf c d ef8
      ef,4 ef gf bf gf8
      ef4 r4 r r r8
    }
    \relative c{
      af4 af af ef af8
      af4 ef af ef4 ef8
      ef4 gf bf ef ef8
      ef4 bf ef bf bf8
      bf4 d f d bf8
      bf4 bf c d ef8
      ef,4 gf bf gf ef8
      ef4 r4 r r r8
    }
    \relative c{
      af4 af af ef af8
      af4 ef af gf4 ef8
      ef4 gf bf ef ef8
      ef4 bf ef bf bf8
      bf4 d f d bf8
      bf4 bf c d ef8

      \once \override Score.RehearsalMark.break-visibility = #begin-of-line-invisible
      \mark \markup { \musicglyph #"scripts.coda" }

      ef,2 gf4 bf bf8
    }
  }
  \alternative {
    {\relative c, {ef4 r r2 r8} }
    {\relative c, {ef4 gf bf gf ef8}}
  }
  \repeat volta 2{
    \relative c,{
      ef4 gf bf ef4 ef8
      ef4 bf gf ef gf8
    }
  }
  \relative c {
    bf4 d f d bf8 
    bf4 f bf f bf8
    bf4 d f d bf8 
    bf4 bf c d ef8

    ef,4 gf bf ef4 ef8
    ef4 bf gf ef gf8
    ef4 gf bf ef4 ef8
    ef4 bf gf ef gf8
    
    ef4 r4 r r \mark "D.S. al coda" r8 
  }
  \bar "||"
  \break
  \mark \markup { \musicglyph #"scripts.coda" }
  \relative c{
    ef4 gf bf gf ef8 
    ef4 gf bf ef8 ef ef ef4
  }
  r4 r r r8 
  \bar "|."
}

grid={
  \chordmode{
    s1 s8 s1 s8
    ef1:m s8  ef1:m s8     bf1:7 s8  bf1:7 s8     bf1:7 s8  bf1:7 s8     ef1:m s8  ef1:m s8 
    ef1:m s8  ef1:m s8     bf1:7 s8  bf1:7 s8     bf1:7 s8  bf1:7 s8     ef1:m s8  ef1:m s8 
    
    af1:m s8  af1:m s8     ef1:m s8  ef1:m s8     bf1:7 s8  bf1:7 s8     ef1:m s8  ef1:m s8
    af1:m s8  af1:m s8     ef1:m s8  ef1:m s8     bf1:7 s8  bf1:7 s8     ef1:m s8  ef1:m s8 
    ef1:m s8  ef1:m s8  ef1:m s8 
    
    bf1:7 s8 bf1:7 s8 bf1:7 s8 bf1:7 s8
    ef1:m s8  ef1:m s8  ef1:m s8 ef1:m s8 ef1:m s8
    ef1:m s8 ef1:m s8 ef1:m s8
  }
}


\book {
  \bookOutputName "goodbye_macedonia_all"
  \score{
  <<
    \new ChordNames { 
      \transpose c c { \grid }
    }
    \new Staff \with { instrumentName = #"Theme (C)" } {
      \transpose c c { \theme }
    }
    
    \new Staff \with { instrumentName = #"Comp (C)" } {
      \transpose c c { \comp }
    }
     
    \new Staff \with { instrumentName = #"Bass (C)" } {
      \transpose c c { \bass }
    }

  >>
  \layout { }
  }
}

\book {
  \bookOutputName "goodbye_macedonia_C"
  \score{
  <<
    \new ChordNames { 
      \transpose c c { \grid }
    }
    \new Staff \with { instrumentName = #"Theme (C)" } {
      \transpose c c { \theme }
    }
    \new Staff \with { instrumentName = #"Comp (C)" } {
      \transpose c c { \comp }
    }
  >>
  \layout { }
  }
}

\book {
  \bookOutputName "goodbye_macedonia_Bb"
  \score{
  <<
    \new ChordNames { 
      \transpose c d { \grid }
    }
    \new Staff \with { instrumentName = #"Theme (Bb)" } {
      \transpose c d { \theme }
    }
    \new Staff \with { instrumentName = #"Comp (Bb)" } {
      \transpose c d { \comp }
    }
  >>
  \layout { }
  }
}

\book {
  \bookOutputName "goodbye_macedonia_bass_Bb"
  \score{
  <<
    \new ChordNames { 
      \transpose c d { \grid }
    }
    \new Staff \with { instrumentName = #"Theme (Bb)" } {
      \transpose c d { \theme }
    }
    \new Staff \with { instrumentName = #"Bass (Bb)" } {
      \transpose c d { \bass }
    }
  >>
  \layout { }
  }
}

\book {
  \bookOutputName "goodbye_macedonia_Eb"
  \score{
  <<
    \new ChordNames { 
      \transpose c a { \grid }
    }
    \new Staff \with { instrumentName = #"Theme (Eb)" } {
      \transpose c a { \theme }
    }    
    \new Staff \with { instrumentName = #"Comp (Eb)" } {
      \transpose c a { \comp }
    }     
  >>
  \layout { }
  }
}

\book {
  \bookOutputName "goodbye_macedonia_C_clef_Ut"
  \score{
  <<
    \new ChordNames { 
      \transpose c c { \grid }
    }
    \new Staff \with { instrumentName = #"Theme (C)" } {
      \clef C
      \transpose c c { \theme }
    }
    \new Staff \with { instrumentName = #"Comp (C)" } {
      \clef C
      \transpose c c { \comp }
    }
  >>
  \layout { }
  }
}
