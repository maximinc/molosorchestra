
\version "2.18.2"
\language "english"

\header {
  title = "Kustino Oro"
  composer = "Goran Bregović Orchestra"
}

global = {
  \set Score.skipBars = ##t
  \time 4/4
}




theme={
  \key bf \minor
  \tempo 4 = 200
  
  \relative c''{
    \repeat volta 2{
      <<
      {a1  f'2 ef4 df c8 df ef2.~ ef2 r
       gf,2. bf4 df2 c4 bf c8 bf a2.~ a2 r}
        \\
      {f1  df'2 c4 bf a8 bf c2. ~ c2 r \break
      ef,2. gf4 bf2 a4 gf a8 gf f2.~ f2 r}
      >>
    }
  }
  \break

  \relative c''{
    \repeat volta 2{
      <<
      {gf2. bf4 df2 c4 bf c8 bf a2.~ a2 r}
        \\
      {ef2. gf4 bf2 a4 gf a8 gf f2.~ f2 r}
      >>
    }
  }\break
  
  \relative c''{
    \repeat volta 2{
    <<
    {df4 df df ef f f f ef af c, c c c2 r \break
    df4 bf bf bf bf2 r 
    gf2 a8 bf gf bf a1  }
    \\
    {bf4 bf bf c df df df c ef af, af af af2 r
    bf4 gf gf gf gf2  r
    ef2 f8 gf ef gf f1
    }
    >>
    }
  }
  \break
  \relative c''{
    r1  
    
    \repeat volta 2{
    <<
    {r4 f f f f2 df4 ef \appoggiatura ef8 f1~ f2 r \break
     r4 f gf f ef2 df4 ef ef8 df c4~ c2~ c2 r2
    }
    \\
    {r4 df df df df2 bf4 c \appoggiatura c8 df1~ df2 r
     r4 df ef df c2 bf4 c c8 bf a4~ a2~ a2 r2
    }
    >>
    }
  }
  \break
  \relative c''{
    { r4 bf bf bf bf2 c4 df c8 bf af4~ af2~ af2 r \break
      r4 gf gf gf gf2 af4 bf af8 gf f4~ f2~ f r}      
  }
  \break
  \relative c''{
    <<
    {r4 df df df df2 ef4 f ef8 df c4~ c2~ c2 r \break
     r4 bf bf bf bf2 c4 df c8 bf a4~ a2~ a1}
    \\
    { r4 bf bf bf bf2 c4 df c8 bf af4~ af2~ af2 r
      r4 gf gf gf gf2 af4 bf af8 gf f4~ f2~ f1^"Fine" }
    >>
  }
  \break
  
  \relative c'{
    \repeat volta 2{
      r1^"Ad lib (solo)" r r r^"fin solo: D.C. al Fine"
    }
    \break
  }
}


bass={
  \clef "F"
  \key bf \minor
  \relative c,{
    \repeat volta 2{
    f4. a8~ a4 c4
    f,4. a8~ a4 c4
    f,4. a8~ a4 c4
    f,4. a8~ a4 c4
    
    ef,4. gf8~ gf4 bf4 
    ef,4. gf8~ gf4 bf4 
    f4. a8~ a4 c4
    f,4. a8~ a4 c4
    }
    \repeat volta 2{
    ef,4. gf8~ gf4 bf4 
    ef,4. gf8~ gf4 bf4 
    f4. a8~ a4 c4
    f,4. a8~ a4 c4
    }
  }
  \relative c{
    \repeat volta 2{
    bf4. f8~ f4 bf4
    bf4. f8~ f4 bf4
    ef,4. f8~ f4 g4
    af4. ef8~ ef4 af4
    gf4. df8~ df4 gf4
    gf4. bf8~ bf4 df4
    ef,4. gf8~ gf4 bf
    f4. a8~ a4 c4
    }
  }
  \relative c,{
    f4. a8~ a4  c4
    f,4. df8~ df4 f4
    f4. df8~ df4 f4
    df4. f8~ f4 af4
    df,4. f8~ f4 af4
    df,4. f8~ f4 af4
    df,4. f8~ f4 af4
    f4. a8~ a4  c4
    f,4. a8~ a4  c4
  }
  \relative c{
    bf4. f8~ f4 bf 
    bf4. f8~ f4 bf 	
    af4. ef8~ ef4 af
    af4. ef8~ ef4 af
    gf4. df8~ df4 gf
    ef4. bf8~ bf4 ef
    f4. c8~ c4 f4
    f4 f g a

    bf4. f8~ f4 bf 
    bf4. f8~ f4 bf 	
    af4. ef8~ ef4 af
    af4. ef8~ ef4 af
    gf4. df8~ df4 gf
    ef4. bf8~ bf4 ef
    f4. c8~ c4 f4
    f4. c8~ c4 f4^"Fine"
  }
  \relative c{
    f,4. a8~ a4 c4
    f,4. a8~ a4 f'4
    f,4. a8~ a4 c4
    f,4.^"fin solo: D.C. al Fine" a8~ a4 f'4
  }
}


chordf={ <a c f>8-. r r <a c f>8-. r <a c f>8 <a c f>4-. }
chordfine={ <a c f>8-. r r <a c f>8-. r <a c f>8 <a c f>4-.^"Fine" }
chordefm={ <gf bf ef>8-. r r <gf bf ef>8-. r <gf bf ef>8 <gf bf ef>4-. }

chordbfm={ <bf df f>8-. r r <bf df f>8-. r <bf df f>8 <bf df f>4-. }
chordaf={ <af c ef>8-. r r <af c ef>8-. r <af c ef>8 <af c ef>4-. }
chordgf={ <gf bf df>8-. r r <gf bf df>8-. r <gf bf df>8 <gf bf df>4-. }
chorddf={ <af df f>8-. r r <af df f>8-. r <af df f>8 <af df f>4-. }


comp={
  \key bf \minor
  
  \relative c'{
    \repeat volta 2{
      \repeat percent 4 \chordf \break
      \repeat percent 2 \chordefm
      \repeat percent 2 \chordf \break
    }
    \repeat volta 2{
      \repeat percent 2 \chordefm
      \repeat percent 2 \chordf
    }
    \break
    
    \repeat volta 2{
      \repeat percent 2 \chordbfm
      \repeat percent 2 \chordaf
      \repeat percent 2 \chordgf
      \chordefm
      \chordf
    }
    \break
    \chordf
    
    
    \repeat volta 2{
      \repeat percent 4 \chorddf \break
      \repeat percent 2 \chorddf
      \repeat percent 2 \chordf
    }
    \break

    \repeat percent 2 \chordbfm
    \repeat percent 2 \chordaf
    \break
    
    \chordgf
    \chordefm
    \chordf
    <a c f>4
    <f a c>4
    <g bf df>4
    <a c ef>
    \break

    \repeat percent 2 \chordbfm
    \repeat percent 2 \chordaf
    \break
    
    \chordgf
    \chordefm
    \chordf
    \chordfine
    \break
  }

  \relative c'{
    \repeat volta 2{
      <a c f>8^"Ad lib" r r <a c f>8 r <a c f>8 <a c f>8 <a c f>8
      <a c f>8 r r <a c f>8 r <a c f>8 <a c f>8 <a c f>8
      <a c f>8 r r <a c f>8 r <a c f>8 <a c f>8 <a c f>8
      <a c f>8^"fin solo: D.C. al Fine" r r <a c f>8 r <a c f>8 <a c f>8 <a c f>8
%       <a c ef f>4^"Ad lib" <a c ef f>8 <a c ef f>8 r8 <a c ef f>8 <a c ef f>8 <a c ef f>8
%       <a c ef f>4 <a c ef f>8 <a c ef f>8 r8 <a c ef f>8 <a c ef f>8 <a c ef f>8
%       <a c ef f>4 <a c ef f>8 <a c ef f>8 r8 <a c ef f>8 <a c ef f>8 <a c ef f>8
%       <a c ef f>4^"fin solo: D.C. al Fine" <a c ef f>8 <a c ef f>8 r8 <a c ef f>8 <a c ef f>8 <a c ef f>8
    }
  }
}

chordNames=\chordmode{
  f1:7 s f1:7 s
  ef:m s f1:7 s
  
  ef:m s f1:7 s
  
  bf:m bf:m af af
  gf gf ef:m f:7
  
  f:7
  
  df/f s df s
  df s f:7 s

  bf:m bf:m af af
  gf ef:m f:7 s

  bf:m bf:m af af
  gf ef:m f:7 s

  f:7
}


\book{
  \bookOutputName "kustino_oro_all"
  \score {
    <<
      \global 
      \new ChordNames { 
        \transpose c c { \chordNames }
      }
  
      \new Staff \with {instrumentName = #"(C)"} {
        \theme
      }
  
      \new Staff \with {instrumentName = #"(C)"} {
        \comp
      }
      
      \new Staff \with {instrumentName = #"(C)"} {
        \bass
      }
            
      
    >>
    \layout { }
  }
}

\book{
  \bookOutputName "kustino_oro_Bb"
  \score {
  <<
    \global 
    \new ChordNames { 
      \transpose c d { \chordNames }
    }

    \new Staff \with {instrumentName = #"(Bb)"} {
      \transpose c d { \theme }
    }
    \new Staff \with {instrumentName = #"(Bb)"} {
      \transpose c d { \comp }
    }
    
  >>
  \layout { }
  }
}
\book{
  \bookOutputName "kustino_oro_C"
  \score {
    <<
      \global 
      \new ChordNames { 
        \transpose c c { \chordNames }
      }
      \new Staff \with {instrumentName = #"(C)"} {
        \theme
      }      
      \new Staff \with {instrumentName = #"(C)"} {
        \bass
      }
    >>
    \layout { }
  }
}

\book{
  \bookOutputName "kustino_oro_C_clef_C"
  \score {
    <<
      \global 
      \new ChordNames { 
        \transpose c c { \chordNames }
      }
  
      \new Staff \with {instrumentName = #"(C)"} {
        \clef C
        \transpose c c,{
          \theme
        }
      }
  
      \new Staff \with {instrumentName = #"(C)"} {
        \clef C
        \comp
      }
      
    >>
    \layout { }
  }
}

\book{
  \bookOutputName "kustino_oro_Eb"
  \score {
    <<
      \global 
      \new ChordNames { 
        \transpose c a { \chordNames }
      }
  
      \new Staff \with {instrumentName = #"(Eb)"} {
        \transpose c a { \theme }
      }
  
      \new Staff \with {instrumentName = #"(Eb)"} {
        \transpose c a { \comp }
      }
  
    >>
    \layout { }
  }
}