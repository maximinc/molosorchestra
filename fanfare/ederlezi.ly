
\version "2.18.2"
\language "english"

\header {
  title = "Ederlezi"
  composer = "Goran Bregovic / trad"
}



chordNames={
  \chordmode{
    s1 s s s s s s s
  
    e:m s e:m s 
    e:m s e:m s 
    
    e2:m d g1 a:m c
    a:m e2:m d b1:7  e:m
    
    g a:m g2 a:m e:m a:m
    e:m a:m c1
    a:m g2 b:7 e1:m

    g a:m g2 a:m e:m a:m
    e:m a:m c1
    a:m g2 b:7 e1:m
    
    e:m s
    e2 fs:m b1:7
    b1:7 e2 fs:m b:7

  }
}

theme_main={
  \relative c''{   
    r4 b8 c d c b a a b~ b4 a16 g a8 g4
    \new Voice{
      <<
      {r4 g8 a a4 a8 fs g4. a8 fs16 e fs8 e4
       \break
      r4 g8 a a4 a8 fs g4. a8 fs16 e fs8 e4
      r4 g8 fs a4 fs e8 e~ e4~e2}
      {r4 b'8 c c4 c8 a b4. c8 a16 g a8 g4
      r4 b8 c c4 c8 a b4. c8 a16 g a8 g4
      r4 b8 a c4 a g8 g~ g4~ g2}
      >>
    }
  }
}

theme={
  
  \time 4/4
  \tempo 4=90

  \key e \minor
  \theme_main
  \break
  %\bar "||"
  \repeat volta 2{
  \theme_main
  \break
  \bar "||"
  \theme_main	
  
  \relative c''{
    \new Voice {
      <<
        {b2. a16 b a g a1 
        g4 b8 b a g fs fs g g fs g b b a4 \break
        g4 b8 b a g fs e e'2
        d8 c b a r4 d8 c b c16 b a4
        r g8 fs a4 fs e8 e~ e4 r r \break

        b'2. a16 b a g a1 
        g4 b8 b a g fs fs g g fs g b b a4 \break
        g4 b8 b a g fs e e'4 \fermata
        d8 c b c16 b a4  r
        d8 c b4 a r
        g8 fs a4 fs e8 e~ e4 r r}
        
        {g2. fs16 g fs e e1
         e4 g8 g e e e e e e e e e e e4
         e4 g8 g e e e e e2
         b'8 a g fs
         r4 b8 a g a16 g e4
         r4 e8 ds fs4 ds e8  e~ e4 r r
         g2. fs16 g fs e e1
         e4 g8 g e e e e e e e e e e e4
         e4 g8 g e e e e e4 s2.
         
         
         s4 b'8 a g4  e4
         r4 e8 ds fs4 ds e8  e~ e4 r r
        }
      >>
    }
  }
  \break
  \bar "||"
  
  r1
  \relative c'{
    ds8 e fs4 r2
    b8 a a gs gs fs fs e
    ds e fs4 r2
    ds8 e fs4 r2
    
    b8 a a gs gs fs fs e
    ds1
  }
  }
}
bass={
  \time 4/4
  \key e \minor
  \clef "F"
  
  r1 r r r
  r  r r r
  \break
  \repeat volta 2{
  \relative c{
    e,1~ e e1~ e
    e1~ e e1~ e
    \break
    
    
    e'4 r d r g, r g r
    a r a r c r c r \break
    a r a r e' r d r
    b r ds r
    e r e r 
    \bar "||"
    \break
    
    g, r g r a r a r
    g r a r e r a r
    \break
    e r a r c r c r
    a r a r g r b ds 
    e r e r
    \break

    g, r g r a r a r
    g r a r e r a r
    \break
    e r a r c4\fermata 
    r r r
    a4 r a r g r b ds 
    e r e r
    \break

    \bar "||"
    
    e r e r
    
    ds8 e fs4 r2
    
    e4 r fs r b r b r
    ds,8 e fs4 
    b,8 b b4 
    
    e4 r fs r b r b r
    
  }
  }
}

chordEm={ r4  <e g b> }
chordEmB={ r4  <g b e> }
chordD={r4 <fs a d> }
chordG={r4 <g b d> }
chordAm={r4 <e a c>}
chordC={r4 <e g c>}
chordB={r4 <fs b ds>}

chordE={ r4  <gs b e> }
chordFsm={ r4  <fs a cs> }

comp={
  \time 4/4
  \key e \minor
  r1 r r r
  r  r r r
  \break

  \repeat volta 2{
    <e b e'> \fermata ~ <e b e'> 
    <e b e'> \fermata ~ <e b e'> 
    <e b e'> \fermata ~ <e b e'> 
    <e b e'> \fermata ~ <e b e'> 
    \break
    
    \bar "||"
    \relative c{
      \chordEm
      \chordD
      \chordG
      \chordG
      \chordAm
      \chordAm
      \chordC
      \chordC
      
      \chordAm
      \chordAm
      \chordEm
      \chordD
      \chordB
      \chordB
      \chordEmB
      \chordEmB
      
      \chordG
      \chordG
      \chordAm
      \chordAm
      \chordG
      \chordAm
      \chordEm
      \chordAm
      \chordEm
      \chordAm
      \chordC
      \chordC
      \chordAm
      \chordAm
      \chordG
      \chordB
      \chordEmB
      \chordEmB
      
      \chordG
      \chordG
      \chordAm
      \chordAm
      \chordG
      \chordAm
      \chordEm
      \chordAm
      \chordEm
      \chordAm
      <e g c>4 \fermata
      r r r 
      \chordAm
      \chordAm
      \chordG
      \chordB
      \chordEmB
      \chordEmB
      \chordEmB
      \chordEmB
    }
    \relative c'{
      ds8 e fs4 r2
    }
    \relative c'{
      \chordE
      \chordFsm
      \chordB
      \chordB
    }
    \relative c'{
      ds8 e fs4 r2
    }
    \relative c'{
      \chordE
      \chordFsm
      \chordB
      \chordB
    }
  }
}



\book {
  \bookOutputName "ederlezi_all"
  \score{
    <<  
      \new ChordNames { 
        \transpose c c { \chordNames }
      }
  
      \new Staff \with {instrumentName = "(C)"}{
         \transpose c c { \theme }
      }
    
      \new Staff \with {instrumentName = "(C)"}{
        \transpose c c { \comp }
      }
       
      \new Staff \with {instrumentName = "(C)"}{
        \transpose c c {  \bass }
      }
    >>
    \layout { }
  }
}

\book {
  \bookOutputName "ederlezi_Bb"
  \score {
    <<  
      \new ChordNames {   
        \transpose c d { \chordNames }
      }
  
      \new Staff \with {instrumentName = "(Bb)"}{
        \transpose c d { \theme }
      }
    
      \new Staff \with {instrumentName = "(Bb)"}{
        \transpose c d { \comp }
      }
     >>
    \layout { }
  }
}

\book {
  \bookOutputName "ederlezi_C"
  \score {
    <<  
      \new ChordNames { 
     
        \transpose c c { \chordNames }
      }
  
      \new Staff \with {instrumentName = "(C)"}{
       \transpose c c { \theme }
      }
    
      \new Staff \with {instrumentName = "(C)"}{
        \transpose c c {  \bass }
      }
    >>
    \layout { }
  }
}

\book {
  \bookOutputName "ederlezi_clefUt"
  \score {
    <<  
      \new ChordNames { 
        \transpose c c { \chordNames }
      }
  
      \new Staff \with {instrumentName = "(C)"}{
        \clef C
        \transpose c c, { \theme }
      }
    
      \new Staff \with {instrumentName = "(C)"}{
        \clef C
        \transpose c c { \comp }
      }
       
    >>
    \layout { }
  }
}

\book{
  \bookOutputName "ederlezi_Eb"
  \score {
    <<  
      \new ChordNames { 
        \transpose c a { \chordNames }
      }
  
      \new Staff \with {instrumentName = "(Eb)"}{
        \transpose c a { \theme }
      }
    
      \new Staff \with {instrumentName = "(Eb)"}{
        \transpose c a { \comp }
      }
    >>
    \layout { }
  }
}