\version "2.20"
\language "english"


\header {
  title = "Borino Oro"
  composer = "Goran Bregović Orchestra"
}

global = {
  \set Score.skipBars = ##t
  \time 4/4
}


cBfm={<bf df f>4. <bf df f>8~ <bf df f>4 <bf df f>4}
cF={<a c f>4. <a c f>8~ <a c f>4 <a c f>4}
cEfm={<gf bf ef>4. <gf bf ef>8~ <gf bf ef>4 <gf bf ef>4}
cgf={<bf df gf>4. <bf df gf>8~ <bf df gf>4 <bf df gf>4}

comp={
  \key bf \minor
  \relative c'{
    \repeat volta 2{
      \cBfm \cF \cEfm 
      <a c f>4 
      <<
        {c8 df ef c df4}
        \\
        {a8 bf c a bf4}
      >>
      \break
      \cBfm \cF \cEfm 
    }
    \alternative {
      { \cF }
      { \cF }
    }
    \break
    \cF \bar "||"
    \cF \cF
    
    \cF \cF  <bf df gf>2 <gf bf ef>2 \cF \break
    \cF \cF  <bf df gf>2 <gf bf ef>2 \cF \break
    
    \cF \cBfm <bf df gf>2 <gf bf ef>2 \cF \break
    \cF \cBfm <bf df gf>2 <gf bf ef>2 \cF \break


    \repeat volta 2{
      \cBfm \cF \cEfm 
      <a c f>4 
      <<
        {c8 df ef c df4}
        \\
        {a8 bf c a bf4}
      >>
      \break
      \cBfm \cF \cEfm 
      \once \override Score.RehearsalMark.font-size = #4
      \mark \markup { \musicglyph #"scripts.coda" }

    }
    \alternative {
      { \cF }
      { \cF }
    }    
    \break
    \repeat volta 2{
      \cF \cF  <bf df gf>2 <gf bf ef>2 \cF \break
    }

    \once \override Staff.KeySignature.break-visibility = #end-of-line-invisible
    \once \override Staff.Clef.break-visibility = #end-of-line-invisible
    \once \override Score.RehearsalMark.extra-offset = #'( -4.5 . 0 )

    \once \override Score.RehearsalMark.font-size = #5
    \mark \markup { \musicglyph #"scripts.coda" }

    \mark \markup { \musicglyph #"scripts.coda" }

    \cF \cF <a c f>4-> r4 r2 \bar "|."
  }
}


theme={
  \key bf \minor
  \tempo 4 = 180
  \repeat volta 2{
  \relative c''{
    \repeat volta 2{
      <<
      {df4 df8 c bf a bf gf a16 bf a gf a8 f a bf c4
      r8 ef df\mordent c bf a bf gf a4 c8 df ef c df4
      \break
      r8 df8 df\mordent  c bf a bf gf a16 bf a gf a8 f a bf c4
      r8 ef df\mordent c bf a bf gf }
      \\
      {bf4 bf8 a gf f gf ef f16 gf f ef f8 df f gf a4
      r8 c bf \mordent a gf f gf ef f4 a8 bf c a bf4
      r8 bf bf\mordent a gf f gf ef f16 gf f ef f8 df f gf a4
      r8 c bf\mordent a gf f gf ef
      }
      >>
    }
    \alternative{
      {<a f>1}
      {<a f>1}
    }
    
    \break
  }
  r1
  \bar "||"
  \relative c''{
    bf8 c8~ c4~ c2 c16 df c bf c8 df c\mordent bf c4 \break
    r4 a8 bf c4 c c c8 ef df\mordent c bf4 
    ef4 ef8 d ef8 ef df4\mordent c4. df8 c\mordent bf c4 \break
    r4 a8 bf c4 c c c8 ef df\mordent c bf4 
    ef4 ef8 d ef8 ef df4\mordent c4. df8 c\mordent bf c4  \break
    r4 c8 d ef4 ef8 d f4 f8 ef df\mordent c bf4
    r8 c bf a gf f gf ef f16 gf f ef f8 gf a bf c4 \break
    r4 c8 d ef4 ef8 d f4 f8 ef df\mordent c bf4
    r8 c bf a gf f gf ef f1 \break    
  } 
  }

  \relative c''{
    \repeat volta 2{
      <<
      {df4 df8 c bf a bf gf a16 bf a gf a8 f a bf c4
      r8 ef df\mordent c bf a bf gf a4 c8 df ef c df4
      \break
      r8 df8 df\mordent  c bf a bf gf a16 bf a gf a8 f a bf c4
      r8 ef df\mordent c bf a bf gf 
      \once \override Score.RehearsalMark.font-size = #4
      \mark \markup { \musicglyph #"scripts.coda" }
      }
      \\
      {bf4 bf8 a gf f gf ef f16 gf f ef f8 df f gf a4
      r8 c bf \mordent a gf f gf ef f4 a8 bf c a bf4
      r8 bf bf\mordent a gf f gf ef f16 gf f ef f8 df f gf a4
      r8 c bf\mordent a gf f gf ef
      }
      >>
    }
    \alternative{
      {<f a>1}
      {<f a>1}
    }
  }
  \break

  \repeat volta 1 { r1^"Solo! " r r  \mark "Ad lib then DC al coda with repeats" r }
  
  \break


    \once \override Staff.KeySignature.break-visibility = #end-of-line-invisible
    \once \override Staff.Clef.break-visibility = #end-of-line-invisible
    \once \override Score.RehearsalMark.extra-offset = #'( -4.5 . 0 )
    
    \once \override Score.RehearsalMark.font-size = #5
    \mark \markup { \musicglyph #"scripts.coda" }
    
    \mark \markup { \musicglyph #"scripts.coda" }
    
    
    \relative  c'{
     <f a>1 ~ <f a>1 ~ <f a>4 r4 r2 \bar "|."
    }


}

bass={
  \clef "F"
  \key bf \minor
  \relative c{
    
    \repeat volta 2{
      <<
        \new CueVoice {f,4-._"1st time" r4 r2 }
              \\
        {bf'4.^"2nd time" f8~ f4 bf4 }
      >>
      f4. c8~ c4 f4
      ef4. bf8~ bf4 ef4
      f4. g8~ g4 a \break
      
      bf4. f8~ f4 bf4
      f4. c8~ c4 f4
      ef4. bf8~ bf4 ef4
      
    }
    \alternative{
      {f4 f g a }
      {f4. c8~ c4 f4}
    }
    \break
    
    f4. c8~ c4 f4
    
    \bar "||"
    f4. c8~ c4 f4 f4. c8~ c4 f4 
    \break
    
    f4. c8~ c4 f4 f4. c8~ c4 f4 
    gf2 ef  f4. c8~ c4 f4
    \break
  
    f4. c8~ c4 f4 f4. c8~ c4 f4 
    gf2 ef  f4. c8~ c4 f4
    \break
    
    f4. c8~ c4 f4 bf4. f8~ f4 bf4
    gf2 ef  f4. c8~ c4 f4
    \break
  
    f4. c8~ c4 f4 bf4. f8~ f4 bf4
    gf2 ef  f4. c8~ c4 f4
    \break
    
    
    \repeat volta 2{
      bf4. f8~ f4 bf4
      f4. c8~ c4 f4
      ef4. bf8~ bf4 ef4
      f4 f g a
      \break
      
      bf4. f8~ f4 bf4
      f4. c8~ c4 f4
      ef4. bf8~ bf4 ef4
      \once \override Score.RehearsalMark.font-size = #4
      \mark \markup { \musicglyph #"scripts.coda" }
    }
    \alternative{
      {f4 f g a}
      {f4. c8~ c4 f4}
    }
    \break
    
    \repeat volta 2{
      f4. c8~ c4 f4 f4. c8~ c4 f4 
      gf2 ef  
      f4. a8~ a4 c
    }
    \break
    

    \once \override Staff.KeySignature.break-visibility = #end-of-line-invisible
    \once \override Staff.Clef.break-visibility = #end-of-line-invisible
    \once \override Score.RehearsalMark.extra-offset = #'( -4.5 . 0 )
    
    \once \override Score.RehearsalMark.font-size = #5
    \mark \markup { \musicglyph #"scripts.coda" }
    
    \mark \markup { \musicglyph #"scripts.coda" }
    
    f,4. c8~ c4 f4 f4. c8~ c4 f4  f4-> r4 r2 \bar "|."
  }
}


chordNames=\chordmode{
  bf1:m f:7 ef:m f:7
  bf1:m f:7 ef:m f:7
  
  f:7
  f:7 f:7 f:7
  
  f:7 f:7 gf2 ef:m f1:7
  f:7 f:7 gf2 ef:m f1:7
  f:7 bf:m gf2 ef:m f1:7
  f:7 bf:m gf2 ef:m f1:7

  bf1:m f:7 ef:m f:7
  bf1:m f:7 ef:m f:7
  
  f:7
  
  f:7 f:7 gf2 ef:m f1:7
  
  f:7 f:7 f:7

}

\book {
  \bookOutputName "borino_oro_all"
  \score{
  <<
    \global 
    \new ChordNames { 
      \transpose c c { \chordNames }
    }

    \new Staff \with { instrumentName = #"Theme (C)" } {
      \transpose c c { \theme }
    }
    
    \new Staff \with { instrumentName = #"Comp (C)" } {
      \transpose c c { \comp }
    }
    
    \new Staff \with { instrumentName = #"Bass (C)" } {
      \transpose c c { \bass }
    }
             
  >>
  \layout { }
  }
}

\book {
  \bookOutputName "borino_oro_Bb"
  \score{
  <<
    \global 
    \new ChordNames { 
      \transpose c d { \chordNames }
    }

    \new Staff \with { instrumentName = #"Theme (Bb)" } {
      \transpose c d { \theme }
    }
    
    \new Staff \with { instrumentName = #"Comp (Bb)" } {
      \transpose c d { \comp }
    }
                 
  >>
  \layout { }
  }
}

\book {
  \bookOutputName "borino_oro_Eb"
  \score{
  <<
    \global 
    \new ChordNames { 
      \transpose c a { \chordNames }
    }

    \new Staff \with { instrumentName = #"Theme (Eb)" } {
      \transpose c a { \theme }
    }
    
    \new Staff \with { instrumentName = #"Comp (Eb)" } {
      \transpose c a { \comp }
    }
                 
  >>
  \layout { }
  }
}

\book {
  \bookOutputName "borino_oro_C"
  \score{
  <<
    \global 
    \new ChordNames { 
      \transpose c c { \chordNames }
    }

    \new Staff \with { instrumentName = #"Theme (C)" } {
      \transpose c c { \theme }
    }
    
    \new Staff \with { instrumentName = #"Bass (C)" } {
      \transpose c c { \bass }
    }
                 
  >>
  \layout { }
  }
}

\book {
  \bookOutputName "borino_oro_clefUt"
  \score{
  <<
    \global 
    \new ChordNames { 
      \transpose c c { \chordNames }
    }

    \new Staff \with { instrumentName = #"Theme (C)" } {
      \clef C
      \transpose c c, { \theme }
    }
    
    \new Staff \with { instrumentName = #"Comp (C)" } {
      \clef C
      \transpose c c { \comp }
    }
                 
  >>
  \layout { }
  }
}



