\version "2.18.2"
\language "english"


\header {
  title = "Sunet Oro"
  composer = "Kočani Orkestar"
}

theme={
  \tempo 4=170
  \time 7/8
  \set Timing.beatStructure = #'(3 2 2)
  \key f \minor
  \relative c'{
    \repeat volta 2{
      <<
      {f4. ~f4 e8 f g4. r4 r
      g8 af bf af4 g f4 e8 df4 c
      f4. ~f4 e8 f g4. af8 g f4
      g8 af bf af g f e f4 g8 af g f e 

      f4. r4 r r4. e8 f g af bf4.~ bf 4af8 g af4. g8 f r4
      g8 af bf af4 g f4 e8 df4 c
      f4. ~f4 e8 f g4. af8 g f4
      g8 af bf af g f e f4 g8 af g f e 
      f4. r4 r r4. e8 f g af bf4.~ bf 4af8 g af4. g8 f r4
      g8 af bf af4 g f4 e8 df4 c
      f4. ~f4 e8 f g4. af8 g f4
      g8 af bf af g f e f4 g8 af g f e f4. r4 r4}
      
      {af4. ~af4 g8 af bf4. r4 r
      bf8 c df c4 bf af4 g8 f4 e
      af4. ~af4 g8 af bf4. c8 bf af4
      bf8 c df c bf af g af4 bf8 c bf af g 

      af4. r4 r r4. g8 af bf c df4.~ df 4c8 bf c4. bf8 af r4
      bf8 c df c4 bf af4 g8 f4 e
      af4. ~af4 g8 af bf4. c8 bf af4
      bf8 c df c bf af g af4 bf8 c bf af g 
      af4. r4 r r4. g8 af bf c df4.~ df 4c8 bf c4. bf8 af r4
      bf8 c df c4 bf af4 g8 f4 e
      af4. ~af4 g8 af bf4. c8 bf af4
      bf8 c df c bf af g af4 bf8 c bf af g af4. r4 r4}
      >>
    }
    \alternative{
      {r4. r4 c,4}
      {r4. c 8 df e f}
    }
  }
  \break 
  \relative c''{
    g8 af bf af g f af g4\mordent r8
    
    c,8 df e f
    g f g e4\mordent df c4 r8
    
    c8 df e f
    g8 af bf af g f af g4\mordent r8

    c,8 df e f
    g f g e4\mordent df c4 r8
  }
  \relative c'{
  <<
    {c8 df e f
    g8 af bf af g f af g4\mordent r8
    
    c,8 df e f
    g f g e4\mordent df c4 r8
    
    c8 df e f
    g8 af bf af g f af g4\mordent r8

    c,8 df e f
    g f g e4\mordent df c4 r8}

    {e8 f g af
    bf8 c df c bf af c bf4\mordent r8
    
    e,8 f g af
    bf af bf g4\mordent f e4 r8
    
    e8 f g af
    bf8 c df c bf af c bf4\mordent r8

    e,8 f g af
    bf af bf g4\mordent f e4 r8}
  >>
  r4 r
  }
  \bar "||"
  \break
  \relative c'{
    <<
    {f4. ~f4 e8 f g4. r4 r
    g8 af bf af4 g f4 e8 df4 c
    f4. ~f4 e8 f g4. af8 g f4
    g8 af bf af g f e f4 g8 af g f e 

    f4. r4 r r4. e8 f g af bf4.~ bf 4af8 g af4. g8 f r4
    g8 af bf af4 g f4 e8 df4 c
    f4. ~f4 e8 f g4. af8 g f4
    g8 af bf af g f e f4 g8 af g f e 
    f4. r4 r r4. e8 f g af bf4.~ bf 4af8 g af4. g8 f r4
    g8 af bf af4 g f4 e8 df4 c
    f4. ~f4 e8 f g4. af8 g f4
    g8 af bf af g f e f4 g8 af g f e f4. r4 r4}
    
    {af4. ~af4 g8 af bf4. r4 r
    bf8 c df c4 bf af4 g8 f4 e
    af4. ~af4 g8 af bf4. c8 bf af4
    bf8 c df c bf af g af4 bf8 c bf af g 

    af4. r4 r r4. g8 af bf c df4.~ df 4c8 bf c4. bf8 af r4
    bf8 c df c4 bf af4 g8 f4 e
    af4. ~af4 g8 af bf4. c8 bf af4
    bf8 c df c bf af g af4 bf8 c bf af g 
    af4. r4 r r4. g8 af bf c df4.~ df 4c8 bf c4. bf8 af r4
    bf8 c df c4 bf af4 g8 f4 e
    af4. ~af4 g8 af bf4. c8 bf af4
    bf8 c df c bf af g af4^"Fine" bf8 c bf af g af4. r4 r4}
    >>
    r4. r2
  }
  %\bar "||"
  \break
  
  \repeat volta 2{
    \relative c'{
      r8 <f a c>16 <f a c>16 <f a c>8 <f a c>8 <f a c>8 <f a c>8 <f a c>8
      r8 <f a c>16 <f a c>16 <f a c>8 <f a c>8 <f a c>8 <f a c>4^"Ad lib. then D.C. al fine"
    }
  }
}



grid={
  \chordmode{
    f4.:m s2 c4.:7 s2 c4.:7 s2 c4.:7 s2 f4.:m s2
    c4.:7 s2 c4.:7 s2
    f4.:m s2
    f4.:m s2 f4.:m s2 
    bf4.:m s2  f4.:m/af s2
    c4.:7 s2
    c4.:7 s2
    f4.:m s2 c4.:7 s2 c4.:7 s2 f4.:m s2
    f4.:m s2 f4.:m s2
    
    bf4.:m s2  f4.:m/af s2
    c4.:7 s2
    c4.:7 s2
    f4.:m s2 c4.:7 s2 c4.:7 s2 f4.:m s2
    f4.:m s2 f4.:m s2
    
    f4.:m s2
    
    c4.:7 s2 c4.:7 s2
    c4.:7 s2 c4.:7 s2
    c4.:7 s2 c4.:7 s2
    c4.:7 s2 c4.:7 s2
    c4.:7 s2 c4.:7 s2
    c4.:7 s2 c4.:7 s2
    c4.:7 s2 c4.:7 s2
    c4.:7 s2 c4.:7 s2

    f4.:m s2 c4.:7 s2 c4.:7 s2 c4.:7 s2 f4.:m s2
    c4.:7 s2 c4.:7 s2
    f4.:m s2
    f4.:m s2 f4.:m s2 
    bf4.:m s2  f4.:m/af s2
    c4.:7 s2
    c4.:7 s2
    f4.:m s2 c4.:7 s2 c4.:7 s2 f4.:m s2
    f4.:m s2 f4.:m s2
    
    bf4.:m s2  f4.:m/af s2
    c4.:7 s2
    c4.:7 s2
    f4.:m s2 c4.:7 s2 c4.:7 s2 f4.:m s2
    f4.:m s2 f4.:m s2

    f:7
  }
}


\book {
  \bookOutputName "sunet_oro_C"
  \score{
  <<
    \new ChordNames { 
      \transpose c c { \grid }
    }

    \new Staff \with { instrumentName = #"Theme (C)" } {
      \transpose c c	 { \theme }
    }	
  >>
  \layout { #(layout-set-staff-size 15)}
  }
}

\book {
  \bookOutputName "sunet_oro_Bb"
  \score{
  <<
    \new ChordNames { 
      \transpose c d { \grid }
    }

    \new Staff \with { instrumentName = #"Theme (Bb)" } {
      \transpose c d	 { \theme }
    }	
  >>
  \layout { #(layout-set-staff-size 18)}
  }
}

\book {
  \bookOutputName "sunet_oro_Eb"
  \score{
  <<
    \new ChordNames { 
      \transpose c a { \grid }
    }

    \new Staff \with { instrumentName = #"Theme (Eb)" } {
      \transpose c a	 { \theme }
    }	
  >>
  \layout { #(layout-set-staff-size 18)}
  }
}


\book {
  \bookOutputName "sunet_oro_clefUt"
  \score{
  <<
    \new ChordNames { 
      \transpose c c { \grid }
    }

    \new Staff \with { instrumentName = #"Theme (C)" } {
      \clef C
      \transpose c c,	 { \theme }
    }	
  >>
  \layout { #(layout-set-staff-size 16)}
  }
}

