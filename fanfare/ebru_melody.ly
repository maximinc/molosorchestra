\version "2.18.2"
\language "english"

\header {
  title = "Ebru Melody"
  composer = "Kočani Orkestar"
}


theme={
  \tempo 4=120
  \key d \minor


  \repeat volta 2{
    <<
      {
        \relative c'{
          d16 e d e f4. f16 g f8\mordent e16 d cs8 cs16 d e8 e16 cs a4 r    
          e'16 f e f g4. g16 a f8 f16 e a16 bf a\mordent g f g f\mordent  e f8 g a4
        }
        \break
      }
      {
        \relative c'{
          f16 g f g a4. a16 bf a8\mordent g16 f e8 e16 f g8 g16 e cs4 r    
          g'16 a g a bf4. bf16 cs a8 a16 g cs16 d cs\mordent bf a bf a\mordent  g a8 bf cs4
        }
      }
    >>
  }
  \break
  <<
    {
      \relative c''{
        r16 bf a\mordent g f8 a g16 a g\mordent f e8 g 
        f16 g f\mordent e d8 f e16 f e\mordent d cs d cs8
      }
      \break
      \relative c''{
        r16 bf a\mordent g f8 a g16 a g\mordent f e8 g 
        r8 f f f f16\mordent g g f f\mordent g f\mordent e
        
        \break
        d8
      }
    }
    {
      \relative c''{
        r16 d cs\mordent bf a8 cs bf16 cs bf\mordent a g8 bf 
        a16 bf a\mordent g f8 a g16 a g\mordent f e f e8
      }
      \break
      \relative c''{
        r16 d cs\mordent bf a8 cs bf16 cs bf\mordent a g8 bf 
        r8 a a a a16\mordent bf bf a a\mordent bf a\mordent g
        
        \break
        f8
      }
    }
  >>
  <<
    {
      \relative c'{
         a'16\mordent g f8 a g16 a g\mordent f e8 g 
        r 16 g f\mordent e d8 f e16 f e\mordent d cs d cs8
      }
      \break
      \relative c''{
        r16 bf a\mordent g f8 a g16 a g\mordent f e8 g 
        r8 f f f g8. f16 f\mordent g f e 
        \break
        d8
      }
    }
    {
      \relative c''{
         cs16\mordent bf a8 cs bf16 cs bf\mordent a g8 bf 
        r 16 bf a\mordent g f8 a g16 a g\mordent f e f e8
      }
      \break
      \relative c''{
        r16 d cs\mordent bf a8 cs bf16 cs bf\mordent a g8 bf 
        r8 a a a bf8. a16 a\mordent bf a g 
        \break
        f8
      }
    }
  >>
  <<
    {
      \relative c'{
        f8 f f f16 g8 f16 f16\mordent g f e 
        d8 g g g g16 a8 g16 g\mordent a g f
      }
      \break	
      \relative c''{
        r8 a a a a16 bf8 a16 a\mordent bf a g
        r8 g a bf cs bf16 bf a4
      }
      \relative c'{
        r8 f8 f f f16 g8 f16 f16\mordent g f e 
        d8 g g g g16 a8 g16 g a g f
      }
      \break	
      \relative c''{
        r8 a a a a16 bf8 a16 a\mordent  bf a g
        r8 g a bf cs bf16 bf a4
      }
    }
    {
      \relative c''{
        a8 a a a16 bf8 a16 a16\mordent bf a g 
        f8 bf bf bf bf16 cs8 bf16 bf\mordent cs bf a
      }
      \break	
      \relative c''{
        r8 cs cs cs cs16 d8 cs16 cs\mordent d cs bf
        r8 bf cs d e d16 d cs4
      }
      \relative c''{
        r8 a8 a a a16 bf8 a16 a16 bf a g 
        f8 bf bf bf bf16 cs8 bf16 bf cs bf a
      }
      \break	
      \relative c''{
        r8 cs cs cs cs16 d8 cs16 cs\mordent  d cs bf
        r8 bf cs d e d16 d cs4
      }
    }
  >>
  \break
  \relative c'{
    \repeat volta 2{
      <<
        {
          r4 f8 g a4 a8 \mordent g16 f g8 g g8 \mordent f16 ef ef g f ef\mordent ef d ef8 	   
          r4 ef8 f g4 g8\mordent f16 ef 
        }
        {
          r4 a8 bf c4 c8 \mordent bf16 a bf8 bf bf8 \mordent a16 g g bf a g\mordent g f g8 	   
          r4 g8 a bf4 bf8\mordent a16 g
        }
      >>
      }
    \alternative {
      {<<
        {f8 f f8 \mordent ef16 d d f ef d d c d8 }
        {a'8 a a8 \mordent g16 f f a g f f e f8 }
       >>}
       {<<
        {f8 f f8 \mordent ef16 d d f ef d d4 }
        {a'8 a a8 \mordent g16 f f a g f f4^"fine"
          \once \override Score.RehearsalMark.break-visibility = #end-of-line-visible
          \once \override Score.RehearsalMark.self-alignment-X = #RIGHT
          \mark \markup "D.C. al solo"

        }
       >>}
    }
  }
  \break

  \repeat volta 2{
    \relative c'{
       d8^"(impro scale)" e f gs a bf cs d
       s1

       \once \override Score.RehearsalMark.break-visibility = #end-of-line-visible
       \once \override Score.RehearsalMark.self-alignment-X = #RIGHT
       \mark \markup "D.C. al fine"
       
    }
  }
}

compDm={
  \relative c'{<d f>8 <d f>16 <d f> r <d f>16 <d f>16 <d f>16 }
}
compA={
  \relative c'{ <a cs>8 <a cs>16 <a cs>16 r <a cs>16 <a cs>16 <a cs>16 }
}

compAbis={
  \relative c'{ <a cs>8 <a cs>16 <a cs>16 <cs e>8 <cs e>16 <cs e>16 }
}

compGm={
  \relative c'{ <g bf>8 <g bf>16 <g bf>16 r <g bf>16 <g bf>16 <g bf>16 }
}
compF={
  \relative c'{ <f a>8 <f a>16 <f a>16 r <f a>16 <f a>16 <f a>16 }
}

compEf={
  \relative c'{ <ef g>8 <ef g>16 <ef g>16 r <ef g>16 <ef g>16 <ef g>16 }
}

compEfp={
  \relative c'{ <bf ef>8 <bf ef>16 <bf ef>16 r <bf ef>16 <bf ef>16 <bf ef>16 }
}

compEfs={
  \relative c'{ <g ef'>8 <g ef'>16 <g ef'>16 r <g ef'>16 <g ef'>16 <g ef'>16 }
}

compCp={
  \relative c'{ <g c>8 <g c>16 <g c>16 r <g c>16 <g c>16 <g c>16 }
}
compDp={
  \relative c'{ <a d>8 <a d>16 <a d>16 r <a d>16 <a d>16 <a d>16 }
}

compFp={
  \relative c'{ <c f>8 <c f>16 <c f>16 r <c f>16 <c f>16 <c f>16 }
}

compFs={
  \relative c'{ <a f'>8 <a f'>16 <a f'>16 r <a f'>16 <a f'>16 <a f'>16 }
}

% compAm={
%   \relative c'{ <a c>8 <a c>16 <a c>16 r <a c>16 <a c>16 <a c>16 }
% }


comp={
  \key d \minor
  \repeat volta 2{
    \compDm \compDm
    \relative c'{ <cs e>4 <bf d> }
    \compA
    \compGm \compGm
    \compAbis
    \compDm
  } 
  \compDm \compDm \compDm \compA
  \compA	  \compA  \compA  \compDm
  \compDm \compDm \compDm \compA
  \compA	  \compA  \compA  \compDm

  \compDm \compDm \compGm \compGm
  \compA	  \compA  \compGm \compAbis

  \compDm \compDm \compGm \compGm
  \compA	  \compA  \compGm \compAbis
  
  \repeat volta 2{
    \compFp \compFp \compFp \compEfs
    \compEfs \compEfs
  }
  \alternative{
    {\compCp \compDp}
    {\compCp \compDp}
  }
  \once \override Score.RehearsalMark.break-visibility = #end-of-line-visible
  \once \override Score.RehearsalMark.self-alignment-X = #RIGHT
  \mark \markup "D.C. al solo"

  
  \repeat volta 2{
    \relative c'{
      <a d>8 <d f>16 <d f> <d f> <a d> <d f> <a d>32 <a d>32
      <a d>8 <d f>16 <d f> <d f> <a d> <d f> <a d>32 <a d>32
      <a d>8 <d f>16 <d f> <d f> <a d> <d f> <a d>32 <a d>32
      <a d>8 <d f>16 <d f> <d f> <a d> <d f> <a d>32 <a d>32
    }
  }

  \once \override Score.RehearsalMark.break-visibility = #end-of-line-visible
  \once \override Score.RehearsalMark.self-alignment-X = #RIGHT
  \mark \markup "D.C. al fine"

}

bass={
  \clef F
  \key d \minor
  \relative c{
    \repeat volta 2{
      d4 a d f8 a g,4 gs a a8 a
      g4 g g g8 g a4 a8 a d4 f8 a
    }
    
    d,4 a d f8 a d,4 a a cs8 e
    a,4 e a a8 a a4 a8 a d4 f8 a
    
    d,4 a d d8 d d4 a
    a cs8 e a,4 a a cs8 e a,4 a8 a 
    d4 d8 a 
    
    d4 d d a8 d g,4 g g g8 g
    a4 a' a, cs8 e g,4 g8 g a4 a

    d4 d8 a d4 d8 d g,4 bf8 d g,4 g'
    a,4 a8 a a4 a' g, bf8 d a4 a 
  }
  \relative c{
    \repeat volta 2{
      f4 c f c g' g8 g ef4 bf ef g8 bf  ef,4 bf 
    }
    \alternative{
      {c4 c8 c d4 a}
      {c4 c8 c d4 a^"fine"}
    }
  }
  \once \override Score.RehearsalMark.break-visibility = #end-of-line-visible
  \once \override Score.RehearsalMark.self-alignment-X = #RIGHT
  \mark \markup "D.C. al solo"

  \repeat volta 2{
    \relative c{
      d4 a d f8 a d,4  a'8 a d,4 a8 cs
    }
  }
  \once \override Score.RehearsalMark.break-visibility = #end-of-line-visible
  \once \override Score.RehearsalMark.self-alignment-X = #RIGHT
  \mark \markup "D.C. al fine"

}

grid={
  \chordmode{
    d1:m  g4:dim7 bf:7/af a2:7
    g1:m a2:7 d:m
    
    d1:m d2:m a:7
    a1:7 a2:7 d:m
    
    d1:m d2:m a:7
    a1:7 a2:7 d:m
    
    d1:m g:m
    a:7 g2:m a:7
    
    d1:m g:m
    a:7 g2:m a:7
    
    f1 g2:m11
    ef2
    ef1
    c2:m d:m
    c2:m d:m
  }
}


\book {
  \bookOutputName "ebru_melody_all"
  \score{
  <<
    \new ChordNames { 
      \transpose c c { \grid }
    }
    \new Staff \with { instrumentName = #"Theme (C)" } {
      \transpose c c { \theme }
    }
    
    \new Staff \with { instrumentName = #"Comp (C)" } {
      \transpose c c { \comp }
    }
    
    \new Staff \with { instrumentName = #"Bass (C)" } {
      \transpose c c { \bass }
    }

  >>
  \layout { }
  \midi{}
  }
}

\book {
  \bookOutputName "ebru_melody_C"
  \score{
  <<
    \new ChordNames { 
      \transpose c c { \grid }
    }
    \new Staff \with { instrumentName = #"Theme (C)" } {
      \transpose c c { \theme }
    }
    
    \new Staff \with { instrumentName = #"Comp (C)" } {
      \transpose c c { \comp }
    }
  >>
  \layout { }
  }
}

\book {
  \bookOutputName "ebru_melody_C_clef_C"
  \score{
  <<
    \new ChordNames { 
      \transpose c c { \grid }
    }
    \new Staff \with { instrumentName = #"Theme (C)" } {
      \transpose c c { \clef C \theme }
    }
    
    \new Staff \with { instrumentName = #"Comp (C)" } {
      \transpose c c { \clef C  \comp }
    }
  >>
  \layout { }
  }
}

\book {
  \bookOutputName "ebru_melody_Bb"
  \score{
  <<
    \new ChordNames { 
      \transpose c d { \grid }
    }
    \new Staff \with { instrumentName = #"Theme (Bb)" } {
      \transpose c d { \theme }
    }
    
    \new Staff \with { instrumentName = #"Comp (Bb)" } {
      \transpose c d { \comp }
    }
  >>
  \layout { }
  }
}

\book {
  \bookOutputName "ebru_melody_Eb"
  \score{
  <<
    \new ChordNames { 
      \transpose c a { \grid }
    }
    \new Staff \with { instrumentName = #"Theme (Eb)" } {
      \transpose c a { \theme }
    }
    
    \new Staff \with { instrumentName = #"Comp (Eb)" } {
      \transpose c a { \comp }
    }    
  >>
  \layout { }
  }
}

\book {
  \bookOutputName "ebru_melody_bass_C"
  \score{
  <<
    \new ChordNames { 
      \transpose c c { \grid }
    }
    \new Staff \with { instrumentName = #"Theme (C)" } {
      \transpose c c { \theme }
    }
        
    \new Staff \with { instrumentName = #"Bass (C)" } {
      \transpose c c { \bass }
    }

  >>
  \layout { }
  }
}

\book {
  \bookOutputName "ebru_melody_bass_Bb"
  \score{
  <<
    \new ChordNames { 
      \transpose c d { \grid }
    }
    \new Staff \with { instrumentName = #"Theme (Bb)" } {
      \transpose c d { \theme }
    }
        
    \new Staff \with { instrumentName = #"Bass (Bb)" } {
      \transpose c d { \bass }
    }

  >>
  \layout { }
  }
}