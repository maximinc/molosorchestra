\version "2.18.2"
\language "english"

\header {
  title = "Papigo"
  composer = "Kočani Orkestar"
}

fine={
    \mark \markup { \musicglyph #"scripts.coda" }
    c4 c8 c g4 bf 
    c4 c8 c g4 bf 
    c4 c8 c g4 bf 
    c4 ef8 d c4^"Fine!"    
    g16 bf c g^"D.S. al coda"
    \bar "|."
}
theme={
  \tempo 4=128
  \key c \minor
  r2.

  \relative c''{
    g16 bf c g  
    \mark "Intro"
    ef'8.\trill f16 ef8. f16 ef4 f16 ef c bf g4. bf8 g4 g16 bf c g 
    ef'16 f ef f ef f ef\mordent f ef4 f16 ef c bf g4. bf8 g4 g16 bf c g 
    %\break
    ef' df f ef df4~ df ef16 df bf af
    g4. bf8  af4. c16 bf
    %\break
    c8 bf~ bf4 d  d16 c bf8 d8\mordent c16 bf c4~ c8 bf8 d16 ef f g 
    c,1\fermata
  }
  \bar "||"
  
  r1 \mark "Entrée tapan" r1 r1 r1
  \bar "||"
  \break
  r1 r r r2.
  
  \relative c''{
    g16 bf c g 
    %\break
    \repeat volta 2{
      \mark \markup { \musicglyph #"scripts.segno" }
      ef'8. f16 ef8. f16 ef4 f16 ef c bf g4. bf8 g4 g16 bf c g 
      \break
      ef'16 f ef f ef f ef\mordent f ef4 f16 ef c bf g4. bf8 g4 g16 bf c g 
      %\break
      ef' df f ef df4~ df ef16 df bf af
      g4. bf8  af4. c16 bf
      %\break
      c8 bf~ bf4 d  d16 c bf8 d8\mordent c16 bf c4~ c8 bf8 d16 ef f g 
      %\break
      
    }
    \alternative{
      {c,4. ef8 c4 g16 bf c g }
      {c4. ef8 c4 
       <<
       {c,16 ef f ef}
       {ef g af g }
       {c16 ef f ef}
       >>
       }
    }
  }
  \break
  \relative c''{
    \repeat volta 2{
    <<
    {g'4 g g16 bf g f ef8 f f c4 ef8 c8. g16 c ef f ef
    \break
    g4 g g16 bf g f ef8 f f c4 ef8 c4 c16 ef f bf
    \break
    af2. af8 g
    c g4 c8 g4 c,16 ef f ef
    \break
    g4 g g16 bf g f ef8 f f c4 ef8 c8. g16 c ef f ef
    \break
    g4 g g16 bf g f ef8 f f c4 ef8 c4 c16 ef f bf
    \break
    af2. af8 g

    c8 g4 c8 g4
    %\break
    }
    {bf,4 bf bf16 c bf af g8 af g ef4 f8 ef8. 
    bf16 ef16 g af g 
    bf4 bf bf16 c bf af g8 af g ef4 f8 ef4
    f16 g af bf c2. c8 c ef c4 ef8 c4
    ef,16 g af g bf4 bf bf16 c bf af g8 af g ef4 f8 ef8. bf16 ef g af g
    
    bf4 bf bf16 c bf af g8 af g ef4 f8 ef4
    f16 g af bf c2. c8 c ef c4 ef8 c4
    }
    {g4 g g16 bf g f ef8 f f c4 ef8 c8. g16 c ef f ef
    
    g4 g g16 bf g f ef8 f f c4 ef8 c4 c16 ef f bf
    
    af2. af8 g
    c g4 c8 g4 c,16 ef f ef
    
    g4 g g16 bf g f ef8 f f c4 ef8 c8. g16 c ef f ef
    
    g4 g g16 bf g f ef8 f f c4 ef8 c4 c16 ef f bf
    
    af2. af8 g

    c8 g4 c8 g4}
    >>  
    
    <<
    {c8 d 
    ef16 d ef f ef8 d d16 c d ef d8 c c16 bf af bf af8 bf8 af4 c8 d
    \break
    ef16 d ef f ef8 d d16 c d ef d8 c c16 bf af bf af8 bf8 af4 d8 bf
    %\break
    d c r c8~c 
    bf d bf d c r c~c4
    }
    {ef8 f 
    g16 f g af g8 f f16 ef f g f8 ef ef16 d c d c8 d8 c4 ef8 f
    
    g16 f g af g8 f f16 ef f g f8 ef ef16 d c d c8 d8 c4 f8 d
     
    f ef r ef8~ef 
    d f d f ef r ef~ef4
    }
    >>
    
    c,16^"First time only" ef f ef
    }
    
  }
  \break
  \relative c''{
    ef16 f ef c ef f ef c  bf c bf af bf af bf af 
    ef'16 f ef c ef f ef c  bf c bf af bf af bf af
    %\break
    f8 af4 c ef16 ef ef8 c ef c4 ef8 c4 r
    \break

    ef16 f ef c ef f ef c  bf c bf af bf af bf af 
    ef'16 f ef c ef f ef c  bf c bf af bf af bf af
    \break
    f8 af4 c ef16 ef ef8 c ef c4 ef8 c4  ef8 f
    \break
    gf16 f ef f gf f ef f gf f ef f gf f ef c f ef c bf c bf af f af4 r
    %\break
    f8 af4 c ef16 ef ef8 c ef c4 ef8 c4  ef8 f
    \break
    gf16 f ef f gf f ef f gf f ef f gf f ef c f ef c bf c bf af f af4 r
    \break
    
    f8 af4 c ef16 ef ef8 c ef c4 ef8 c2
    \once \override Score.RehearsalMark.break-visibility = #begin-of-line-invisible
    \mark \markup { \musicglyph #"scripts.coda" }
  }
  
  
  %\bar "||"
  \break
  \relative c'{
    \repeat volta 1{
      r1^"Solos"
      r1
      r1
      r1
    }
  }
  \break
  \relative c''{
    \fine
%     \mark \markup { \musicglyph #"scripts.coda" }
%     c4 c8 c g4 bf 
%     c4 c8 c g4 bf 
%     c4 c8 c g4 bf 
%     c4 ef8 d c4^"Fine!"
%     
%     g16 bf c g^"D.S. al coda"
  }
}

cm={
  r8 <g c ef> <g c ef> <g c ef> r <g c ef> r <g c ef>
}

dfl={
  r8 <af df f> <af df f> <af df f> r <af df f> r <af df f>
}

gms={
  r8 <bf d f> <bf d f> <bf d f> r <bf d f> r <bf d f>
}
bfm={
  r8 <f af df> r8 <f af df> r8 <f af df> r8 <f af df>
}

comp={
  \key c \minor
  \tempo 4=128
  r1
  \relative c'{
    \mark "Intro"
    r4 <g c ef>2.
    r4 <bf d f>2.
    r4 <g c ef>2.
    r4 <bf d f>2.
    <f af df>1
    <ef g bf>2
    <f af c>2
    <f bf d>1
    <af c ef>
    <g c ef>\fermata
  }
  \bar "||"
  r1\mark "Entrée tapan" r1 r1 r1
  \relative c'{
    \repeat percent 2{
      r8 <g c ef> <g c ef> <g c ef> r <g c ef> r <g c ef>
      r8 <g c ef> <g c ef> <g c ef> r <g c ef> r <g c ef>
    }
  }
  
  \relative c'{
    \mark \markup { \musicglyph #"scripts.segno" }
    \repeat volta 2{
      \cm  \gms
      \cm  \gms
      \bfm
      r8 <ef g bf>4. r8 <f af c>4. 
      r8 <bf d f>4. r8 <bf d f>8 r <bf d f>
      <af c ef>4 r8 <af c ef>8 r2
    }
    \alternative{
      {<ef g c>8 <ef g c >8 r8 <ef g c>8 r2}
      {<ef g c>8 <ef g c >8 r8 <ef g c>8 r2}
    }
  }
  \relative c'{
    \repeat volta 2{
      \cm \cm \cm \cm
      \dfl \cm
      \cm \cm  \cm \cm
      \dfl \cm
    
      r8 <bf ef>4. r8 <bf f'>4. 
      r8 <c ef>4. r8 <c ef>8 r8 <c ef>8
      r8 <bf ef>4. r8 <bf f'>4. 
      r8 <c ef>4. r8 <c ef>8 r8 <c ef>8
      <bf ef >4. <bf ef >8 r2
      <c ef >8 <c ef > r <c ef > r2
    }
  }
  \relative c'{
    r8 <g bf>4. r8 <c ef> <c ef>  <c ef> 
    r8 <g bf>4. r8 <c ef> <c ef>  <c ef> 
    r8 <af c> <af c> <af c> r8 <af c> r8 <af c>
    r8 <c ef> <c ef> <c ef> r8 <c ef> r8 <c ef>
    r8 <g bf>4. r8 <c ef> <c ef>  <c ef> 
    r8 <g bf>4. r8 <c ef> <c ef>  <c ef> 
    r8 <af c> <af c> <af c> r8 <af c> r8 <af c>
    r8 <c ef> <c ef> <c ef> r8 <c ef> r8 <c ef>

    <gf bf>1 <c ef>1

    r8 <af c> <af c> <af c> r8 <af c> r8 <af c>
    r8 <c ef> <c ef> <c ef> r8 <c ef> r8 <c ef>

    <gf bf>8 r r <gf bf>8 r2
    <c ef>8 r r <c ef>8 r2
    
    r8 <af c> <af c> <af c> r8 <af c> r8 <af c>
    r8 <c ef> <c ef> <c ef> r8 <c ef> r8 <c ef>
    \once \override Score.RehearsalMark.break-visibility = #begin-of-line-invisible
    \mark \markup { \musicglyph #"scripts.coda" }

  }
  
  \relative c'{
    \repeat volta 2{
      
      <c ef g>8 <c ef g>8 <c ef g>8 <c ef g>8 <c ef g>4 r
      <c ef g>8 <c ef g>8 <c ef g>8 <c ef g>8 <c ef g>4 r
      <c ef g>8 <c ef g>8 <c ef g>8 <c ef g>8 <c ef g>4 r
      <c ef g>8 <c ef g>8 <c ef g>8 <c ef g>8 <c ef g>4^"Ad lib" r
      
    }
  }
  \relative c'{
    \fine
  }
}

bass={
  \clef F
  \tempo 4=128
  \key c \minor
  r1
  \relative c,{
    \mark "Intro"
    r4 c2. r4 g'2. r4 c,2. r4 g'2.
    bf 1	   ef,2 f
    bf4. (g8~ g2) af1 c,\fermata
  }
  \bar "||"
  r1 \mark "Entrée tapan" r1 r1 r1
  \relative c,{
    %\repeat percent 3{
      c4. ef8 g4 bf8. bf16 c4. bf8 g8. f16 ef8. g16
    %}
    c,4. ef8 g4 bf8. bf16 c4. bf8 g8. bf16 c bf g ef
  }
  \relative c,{
    \mark \markup { \musicglyph #"scripts.segno" }
    \repeat volta 2{
      c4. ef8 g4 bf8 c
      g4. g8 d4 g8. g16
      c,4. ef8 g4 bf8 c
      g4. g8 d4 g8. g16
      bf4. bf8 f4 bf,
      ef4 r8 ef16 e
      f4 r8 af16 a 
      bf4. d8 f d f16 d bf a
      af4 r8 af8 r2
    }
    \alternative{
      {c8 c r c r2}
      {c8 c r c r2}
    }
  }
  
  \relative c,{
    \repeat volta 2{
      c4. ef8 g4 bf8. bf16 c4. bf8 g4 ef8. g16
      c,4. ef8 g4 bf8. bf16 c4. bf8 g4 c8. c16
      df4 af8. af16 df,4 df' c4. g8 g g bf c
      c,4. ef8 g4 bf8. bf16 c4. bf8 g8. bf16 c bf g ef
      c4. ef8 g4 bf8. bf16 c4. bf8 g4 c8. c16
      df4 af8. af16 df,4 df' c4. g8 g8 g 
      c16 bf c d
    
      ef4 r16 ef16 d c bf8 d f16 d bf a
      af4. af8 ef4 af8. af16
      ef4 r8 g bf g r bf16 a
      af4. af8 ef af r bf16 a 
      af4. f8 r2
      c8 c r c~ c4 r
    }
  }
  
  \relative c,{
    ef8. ef16 g8 bf
    af8. af16 c8 ef
    ef,8. ef16 g8 bf
    af8. af16 c8 ef
    f,4. g8 af4 bf c4. g8 g4 f 
    ef8. ef16 g8 bf
    af8. af16 c8 ef
    ef,8. ef16 g8 bf
    af8. af16 c8 ef
    f,4. g8 af4 bf c4. bf8 g4 f
    
    ef1 af2. \tuplet 3/2 { af8 g gf }
    
    f 4. g8 af4 bf c4. g8 g4 f 
    ef4. ef8 r2 
    af8 af r af r4 \tuplet 3/2 { af8 g gf }
    f 4. g8 af4 bf c4. bf8 g4 ef
    \once \override Score.RehearsalMark.break-visibility = #begin-of-line-invisible
    \mark \markup { \musicglyph #"scripts.coda" }
  }
  \relative c{
    \repeat volta 2{
      c8 c c c c4 r c8 c c c c4 r c8 c c c c4 r c8 c c c c4^"Ad lib" r
    }
  }
  \relative c{
    \fine
  }
}

gridA={
  \chordmode{  
    c1:m g:m7 c:m g:m7
    bf:m 7 ef2 f:m
    bf1 af c:m
  }
}

grid={
  s1 
  \gridA
  s1 s s s
  \chordmode{
    c:m c:m c:m c:m
  }
  \gridA
  \chordmode{
    c:m
    
    c:m c:m c:m c:m
    df c:m
    c:m c:m c:m c:m
    df c:m

    ef2 bf af1
    ef2 bf af1
    s  s
  }
  \chordmode{
    ef2 af ef af  f1:m c:m
    ef2 af ef af  f1:m c:m
    ef:m af f:m c:m
    ef:m af f:m c:m
    
    c:m
  }
}


\book {
  \bookOutputName "papigo_all"
  \score{
  <<
    \new ChordNames { 
      \transpose c c { \grid }
    }

     \new Staff \with { instrumentName = #"Theme (C)" } {
       \transpose c c	 { \theme }
     }
 
      \new Staff \with { instrumentName = #"Comp (C)" } {
        \clef F
        \transpose c c	 { \comp }
      }
 
     \new Staff \with { instrumentName = #"Bass (C)" } {
       \transpose c c	 { \bass }
     }

  >>
  \layout { }
  \midi { }
  }
}

\book {
  \bookOutputName "papigo_all_Bb"
  \score{
  <<
    \new ChordNames { 
      \transpose c d { \grid }
    }

     \new Staff \with { instrumentName = #"Theme (Bb)" } {
       \transpose c d	 { \theme }
     }
 
      \new Staff \with { instrumentName = #"Comp (Bb)" } {
        \clef F
        \transpose c d	 { \comp }
      }
 
     \new Staff \with { instrumentName = #"Bass (Bb)" } {
       \transpose c d	 { \bass }
     }

  >>
  \layout { }
  }
}

\book {
  \bookOutputName "papigo_all_Eb"
  \score{
  <<
    \new ChordNames { 
      \transpose c a { \grid }
    }

     \new Staff \with { instrumentName = #"Theme (Eb)" } {
       \transpose c a	 { \theme }
     }
 
      \new Staff \with { instrumentName = #"Comp (Eb)" } {
        \clef F
        \transpose c a	 { \comp }
      }
 
     \new Staff \with { instrumentName = #"Bass (Eb)" } {
       \transpose c a	 { \bass }
     }

  >>
  \layout { }
  }
}

% \book {
%   \bookOutputName "papigo_Bb"
%   \score{
%   <<
%     \new ChordNames { 
%       \transpose c d { \grid }
%     }
% 
%     \new Staff \with { instrumentName = #"Theme (Bb)" } {
%       \transpose c d { \theme }
%     }
% 
%   \new Staff \with { instrumentName = #"Comp (Bb)" } {
%       \transpose c d { \comp }
%     }
%   >>
%   \layout { #(layout-set-staff-size 18) }
%   }
% }
% 
% \book {
%   \bookOutputName "papigo_Eb"
%   \score{
%   <<
%     \new ChordNames { 
%       \transpose c a { \grid }
%     }
% 
%     \new Staff \with { instrumentName = #"Theme (Eb)" } {
%       \transpose c a { \theme }
%     }
% 
%   \new Staff \with { instrumentName = #"Comp (Eb)" } {
%       \transpose c a { \comp }
%     }
%   >>
%   \layout { #(layout-set-staff-size 18) }
%   }
% }
% 
% 
% \book {
%   \bookOutputName "papigo_C"
%   \score{
%   <<
%     \new ChordNames { 
%       \transpose c c { \grid }
%     }
% 
%     \new Staff \with { instrumentName = #"Theme (C)" } {
%       \transpose c c { \theme }
%     }
% 
%   \new Staff \with { instrumentName = #"Comp (C)" } {
%       \transpose c c { \comp }
%     }
%   >>
%   \layout { #(layout-set-staff-size 17) }
%   }
% }
% 
% \book {
%   \bookOutputName "papigo_bass_Bb"
%   \score{
%   <<
%     \new ChordNames { 
%       \transpose c d { \grid }
%     }
% 
%     \new Staff \with { instrumentName = #"Theme (Bb)" } {
%       \transpose c d { \theme }
%     }
% 
%    \new Staff \with { instrumentName = #"Bass (Bb)" } {
%      \transpose c d	 { \bass }
%    }
% 
%   >>
%   \layout { #(layout-set-staff-size 18) }
%   }
% }
