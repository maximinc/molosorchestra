
\version "2.18.2"
\language "english"

\paper {
  system-system-spacing #'basic-distance = #1
}



\header {
  title = "Come On! Feel the Illinoise!"
  subtitle="(Part I: The World's Columbian Exposition – Part II: Carl Sandburg Visits Me in a Dream)"
  composer = "Sufjan Stevens"
}

global = {
  \set Score.skipBars = ##t
  \time 5/4
}

sl = {
  \override NoteHead.style = #'slash
  \hide Stem
}
nsl = {
  \revert NoteHead.style
  \undo \hide Stem
}


verse={
  \relative c''{
    c4. b8~ b4 r a4 g4. e8~ e4 r2
    e'4. d8~ d4 r c4 b4 c8 b8~b c b4 a
    c4. b8~ b4 r a4 g4. e8~ e4 r4 g 
    g g8 e~ e4 r d4 e4 r2 r
  }
}

chorus={
  \relative c'''{
    <<
      {g8 g r g r g f4 e
      e8 e r e r e d4 c 
      c8 c r c r c a4 g4
      g8 g r g r g f4 e}
      {e'8 e r e r e d4 c 
      c8 c r c r c a4 g4
      g8 g r g r g f4 e
      e8 e r e r e d4 c}
    >>
  }
  \relative c'''{
    <<
    {g8 g r g r g f4 e
    e8 e r e r e d4 c 
    c8 c r c r c a4 g4
    g8 g r g r g c4 r}
    {e8 e r e r e d4 c 
    c8 c r c r c a4 g4
    g8 g r g r g f4 e
    e8 e r e r e g4 r}
    >>
  }
}

chorusShort={
  \relative c'''{
    <<
      {g8^"Wheel" g r g r g f4 e
      e8 e r e r e d4 c 
      c8 c r c r c a4 g4
      g8 g r g r g f4 e}
      {e'8 e r e r e d4 c 
      c8 c r c r c a4 g4
      g8 g r g r g f4 e
      e8 e r e r e d4 c}
    >>
  }
}

bridgeA={
  \relative c''{
    <<
      {r4. a4. b4 c r4. c4. d4 e
       r4. g,4. a4 b r4. b4. c4 d
       c4. c4. d4 e e2. r2}
      {r4. f,4. g4 a r4. a4. b4 c
       r4. e,4. f4 g r4. gs4. a4 b
       a4. a4. b4 c c2. r2}
    >>
  }
}
bridgeB={
  \relative c''{
    <<
      {r4. a4. b4 c r4. c4. d4 e
       r4. g,4. a4 b b4. b4. c4 d
       r4. c4. d4 e e2.~ e2
      }
      {r4. f,4. g4 a r4. a4. b4 c
       r4. e,4. f4 g gs4. gs4. a4 b
       r4. a4. b4 c c2.~ c2}
    >>
  }  
}
bridgeC={
 \relative c''{
    <<
      {r4. a4. b4 c r4. c4. d4 e
       r4. g,4. a4 b b4. b4. c4 d
       c4. c4. d4 e e2.~ e2
      }
      {r4. f,4. g4 a r4. a4. b4 c
       r4. e,4. f4 g gs4. gs4. a4 b
       a4. a4. b4 c c2.~ c2}
    >>
  }  
}
bridgeD={
 \relative c''{
    <<
      {r4. a4. b4 c r4. c4. d4 e
       r4. g,4. a4 b b4. b4. c4 d
       c4. c4. d4 e}
      {r4. f,4. g4 a r4. a4. b4 c
       r4. e,4. f4 g gs4. gs4. a4 b
       a4. a4. b4 c}
    >>
  }  
}


themePartA={
  \tempo 4=210
  \key c \major
 
  r2. r2 r2. r2 r2. r2 r2. r2
  r2. r2 r2. r2 r2. r2 r2. r2
  r2. r2 r2. r2 r2. r2 r2. r2
  \verse
  \verse
  \chorus
  \verse
  \bridgeA
  \bridgeB
  r2. r2 r2. r2 r2. r2 r2. r2
  r2. r2 r2. r2 r2. r2
  
  \verse
  \verse
  \chorus
  \bridgeC
  \bridgeD
  \chorusShort 

  \relative c''{
    r4. 
    <<{a4. b4 c}
      {c4. d4 e}
    >>
  }
}

themePartB={
  \time 4/4
  \key d \major
  \tempo 4=105 
%   \tempo \markup {
%     \concat {
%     \smaller \general-align #Y #DOWN \note #"4" #1 = 105 
%     (
%     \smaller \general-align #Y #DOWN \note #"4" #1
%     " = "
%     \smaller \general-align #Y #DOWN \note #"2" #1
%     )
%     }
%   }

  \relative c''{
    <a' fs d a>1
    r r r
    r  r  r  r
    r2 r16 a,16 g a fs a e a
    r2 r16 a16 g a fs a e a
    r2 r16 a16 g a fs a e a
    r2 r16 a16 g a fs a e a
  }
  \relative c''{
    r2 r16 a16 a8 a16 a a a 
    r2 r16 a16 a8 a16 a a a
    r2 r16 a16 a8 a16 a a a
    r2 r16 a16 a8 a16 a a a
  }
  r1 r1 r1 r1
  r1 r1 r1 r1	r1 r1 r1 r1
  r1 r1 r1 r1	\repeat volta 2 {r1 r1 r1 r1}
  r1 r1 r1 r1	r1 r1 r1 r1
  r1 r1 r1	
  
  r2. 
  \relative c'{
    fs8 a8~ a4 r r 
    fs8 a8~ a4 r r
    fs8 cs' 8~ cs2 r8 fs,8 r e~ e4
    r r8 fs fs a~ a4 r r
    fs8 a8~ a4 r r8 fs fs cs'~ cs2 r4 e,8 e fs g16 fs e8 r r4 
    fs8 fs a b16 a r4
    r8
    fs8 fs a8 
    r2 r4 fs8 cs'~ cs2	
    r8 fs,4 e8 fs g fs e8 r8
    
    fs8 fs a b16 a r4.
    r8
    fs8 fs a8 
    r2 r8 fs8 fs cs'~ cs2	 
    r8 fs,4 e8 fs16 g16~ g8~ g4 r8 fs4 d8
  }

  \relative c''{
    <<
      {a8 a g g fs fs e e g g fs fs e e d d cs cs cs cs cs d e4 cs8 cs cs cs cs d e4}
      {fs8 fs e e d d cs cs e e d d cs cs b b a a a a a b cs4 a8 a a a a b cs4}
    >>
  }
  \relative c''{
    <<
      {a8 a g g fs fs e e g g fs fs e e d d cs cs cs cs cs d e4 cs8 cs cs cs cs}  
      {fs8 fs e e d d cs cs e e d d cs cs b b a a a a a b cs4 a8 a a a a}       
    >>
    <<
      {fs'8^"And we laughed" fs a} 
      \\
      {d,8 e4}
      {b8_"a lone" cs4}
    >>
  }
  \relative c'{
    r2 r8 fs8 fs a b16 a
    r8 r4 r8 fs fs cs'8~ cs2
    r8 fs,4 e8 fs g fs16 e r8 r8
    
    fs8 fs a ~ a4
    
    r4. fs8 fs a b16 a r8 r4

    r8 
    fs8 fs cs'~ cs2	 
    r8 fs,4 e8
    fs8 g fs e
    r8 d4 d8
  }
  \relative c''{
    <<
      {a8 a g g fs fs e e g g fs fs e e d d cs cs cs cs cs d e4 cs8 cs cs cs cs d e4}
      {fs8 fs e e d d cs cs e e d d cs cs b b a a a a a b cs4 a8 a a a a b cs4}
    >>
  }
  \relative c''{
    <<
      {a8 a g g fs fs e e g g fs fs e e d d cs cs cs cs cs d e4 cs8 cs cs cs cs}  
      {fs8 fs e e d d cs cs e e d d cs cs b b a a a a a b cs4 a8 a a a a}       
    >>
    <<
      {fs'8 fs^"And I cried" a~}
      \\
      {d,8 e4}
      {b8_"the heart?" cs4}
    >>
  }
  \relative c''{
    a4 r r 
    fs8 a8~ a4 r r
    fs8 cs' 8~ cs2 r8 fs,8 r e~ e4
    r r8 fs fs a~ a4 r r8 
    fs fs a b16 a r8
    r4 r8 fs  fs cs'~cs2
    
    r8 fs,4 e8 fs g fs e
    
    r4 cs8 d
    
  }

  \relative c''{
    <<
      {a8 a g g fs fs e e g g fs fs e e d d cs cs cs cs cs d e4 cs8 cs cs cs cs d e4}
      {fs8 fs e e d d cs cs e e d d cs cs b b a a a a a b cs4 a8 a a a a b cs4}
    >>
  }
  \relative c''{
    <<
      {a8 a g g fs fs e e g g fs fs e e d d cs cs cs cs cs d e4 cs8 cs cs cs cs d e4}
      {fs8 fs e e d d cs cs e e d d cs cs b b a a a a a b cs4 a8 a a a a b cs4}
    >>
  }

  r1 r r r r r r r r \fermata \bar "|."
}

trumpetAintro={
  \relative c''{
    <<
      {e8 e r e r e c4 b}
      {g8 g r g r g e4 d}
    >>
  }
}


trumpetA={
  \relative c''{
    <<
      {e8 e r e r e c4 b}
      {g8 g r g r g f4 e}
    >>
  }
}

trumpetAe={
  \relative c''{
    <<
      {e8 e r e r e c2\glissando}
      {g8 g r g r g e4}
    >>
  }
}

trumpetAeintro={
  \relative c''{
    <<
      {e8 e r e r e c2\glissando}
      {g8 g r g r g e4}
    >>
    c''
  }
}

trumpetBridge={
  \relative c''{
    a2. g2 e2.~ e2
    g2. f2 e2.~ e2
    d4. e f4 g e2. r4 c

    a'2. g2 e2.~ e2
    g2. f2 e2.~ e2
    d4. e f4 g 
  }
}
bridgeOutA={
  \relative c'{
    <<
      {e'8 e r e r e c4 b}
      {g8 g r g r g e4 d}
      \\
      {e2. r2}
    >>
  }
}

bridgeOutB={
  \relative c'{
    <<
      {e'8 e r e r e c4 b}
      {g8 g r g r g f4 e}
      \\
      {e2. r2}
    >>
  }
}


sectionPartA={
  \key c \major
  r2. r2
  r2. r2
  r2. r2
  r2. r2
  \trumpetAintro
  \trumpetAintro
  \trumpetAintro
  \trumpetAintro
  
  \trumpetAintro
  \trumpetAintro
  \trumpetAintro
  \trumpetAeintro

   r2 r2		r2. r2
  r2. r2		r2. r2
  r2. r2		r2. r2
  r2. r2		r2. r2
  
  r2. r2		r2. r2
  r2. r2		r2. r2
  r2. r2		r2. r2
  r2. r2		r2. r2

  \trumpetA
  \trumpetA
  \trumpetA
  \trumpetA
  
  \trumpetA
  \trumpetA
  \trumpetA
  \trumpetAe

  \relative c'''{c4}
   r2 r2		r2. r2
  r2. r2		r2. r2
  r2. r2		r2. r2
  r2. r2		r2. r2


  \trumpetBridge
  \bridgeOutA
  \trumpetAintro
  \trumpetAintro
  \trumpetAintro

  \trumpetAintro
  \trumpetAintro
  \trumpetAintro
  \trumpetAe

  \relative c'''{c4}
   r2 r2		r2. r2
  r2. r2		r2. r2
  r2. r2		r2. r2
  r2. r2		r2. r2
  r2. r2		r2. r2
  r2. r2		r2. r2
  r2. r2		r2. r2
  r2. r2		r2. r2

  \trumpetA
  \trumpetA
  \trumpetA
  \trumpetA
  
  \trumpetA
  \trumpetA
  \trumpetA	
  \trumpetAe
  \trumpetBridge
  \bridgeOutB

  \trumpetA
  \trumpetA
  \trumpetA

  \relative c''{
    a2. g2
  }
  
}
sectionPartB={
  \time 4/4 
  \key d \major
%     \tempo \markup {
%     \concat {
%     (
%     \smaller \general-align #Y #DOWN \note #"4" #1
%     " = "
%     \smaller \general-align #Y #DOWN \note #"2" #1
%     )
%     }
%   }

  \relative c'{
    <fs a d>8. <fs a d>16 r8 <fs a d>8       r16 d'16 d8 d16 d d d
    <fs, a d>8. <fs a d>16 r8 <fs a d>8      r16 d'16 d8 d16 d d d
    <d, fs b>8. <d fs b>16 r8 <d fs b>8 	    r16 d'16 d8 d16 d d d
    <d, fs b>8. <d fs b>16 r8 <d fs b>8 	    r16 d'16 d8 d16 d d d
    <cs, fs a>8. <cs fs a>16 r8 <cs fs a>8    r16 a'16 a8 a16 a a a
    <cs, fs a>8. <cs fs a>16 r8 <cs fs a>8   r16 a'16 a8 a16 a a a
    <e a cs>8. <e a cs>16 r8 <e a cs>8       r16 a16 a8 a16 a a a
    <e a cs>8. <e a cs>16 r8 <e a cs>8       r16 a16 a8 a16 a a a
  }
  \relative c'''{
    <<
    {a 8. g16 r8  fs8 r2
    a8. g16 r8  fs8 r2
    a8. g16 r8  fs8 r2
    a8. g16 r8  fs8 r4 }
    {d,8. e16 r8  fs8 r2
    d8.   e16 r8  fs8 r2
    d8.   e16 r8  d8 r2
    a'8.  g16 r8  d8 r4 }
    >>
    <fs a fs'>8
    <a cs e>8~
    <a cs e>1~
    <a cs e>2
    r2
    
    <<
      {e4 fs g a b cs d e fs1}
      {cs,4 d e fs g a b cs d1}
    >>
  }
  r1 r r
  \relative c'''{
    \arpeggioArrowDown
    <g d b g>1\arpeggio^"guitar"
    <a, cs e a>1\arpeggio
    <g' d b g>1\arpeggio
  }
  \relative c '{
    g16 b d g
    a, cs e a
    b, d g b
    cs, e a cs
  }
  
  \relative c''{
    r8 
    <<{ d4 fs cs e8~ e b4 d a cs8~ cs g4 b fs a8~ a e4 g cs, d8~ d2}
      { fs4 a e g8~ g8 d4 fs cs e8~ e b4 d a cs8~ cs g4 b a fs8~ fs2}
    >>
  }
  
  r2 r1 r r
  \repeat volta 2{
    r1 r r r
  }
  r1 r
  \relative c'{
    <<
      {e4 fs g a b cs d e}
      {cs,4 d e fs g a b cs}
    >>
  }
  \relative c''{
    <<
    <d fs>1
    \\
    {a8. g16 r8  fs8 r2
    a8. g16 r8  fs8 r2
    a8. g16 r8  fs8 r2
    a8. g16 r8  fs8 r2 }
    {d8. e16 r8  d8 r2
    d8. e16 r8  d8 r2
    d8. e16 r8  d8 r2
    d8. e16 r8  d8 r2}
    >>
  }
  \relative c''{
    <<
    {a8. g16 r8  fs8 r2
    a8. g16 r8  fs8 r2
    a8. g16 r8  fs8 r2
    a8. g16 r8  fs8 r2 }
    {d8. e16 r8  d8 r2
    d8. e16 r8  d8 r2
    d8. e16 r8  d8 r2
    d8. e16 r8  d8 r2}
    >>
  }
  r1 r1 r1 r1
  r1 r1 r1 r1	
  \relative c''{
    <<
    {a8. g16 r8  fs8 r2
    a8. g16 r8  fs8 r2
    a8. g16 r8  fs8 r2
    a8. g16 r8  fs8 r2 }
    {e8. e16 r8  d8 r2
    e8. e16 r8  d8 r2
    e8. e16 r8  d8 r2
    e8. e16 r8  d8 r2}
    >>
  }

  \relative c''{
    <<
    {a8. g16 r8  fs8 r2
    a8. g16 r8  fs8 r2
    a8. g16 r8  fs8 r2
    a8. g16 r8  fs8 r2 }
    {d8. e16 r8  d8 r2
    d8. e16 r8  d8 r2
    d8. e16 r8  d8 r2
    d8. e16 r8  d8 r2}
    >>
  }
  r1 r1 r1 r1
  r1 r1 r1 r1
  \relative c''{
    <<
    {a8. g16 r8  fs8 r2
    a8. g16 r8  fs8 r2
    a8. g16 r8  fs8 r2
    a8. g16 r8  fs8 r2 }
    {d8. e16 r8  d8 r2
    d8. e16 r8  d8 r2
    d8. e16 r8  d8 r2
    d8. e16 r8  d8 r2}
    >>
  }
  \relative c''{
    <<
    {a8. g16 r8  fs8 r2
    a8. g16 r8  fs8 r2
    a8. g16 r8  fs8 r2
    a8. g16 r8  fs8 r2 }
    {d8. e16 r8  d8 r2
    d8. e16 r8  d8 r2
    d8. e16 r8  d8 r2
    d8. e16 r8  d8 r2}
    >>
  }
  \relative c''{
    <<
    {a8. g16 r8  fs8 r2
    a8. g16 r8  fs8 r2
    a8. g16 r8  fs8 r2
    a8. g16 r8  fs8 r2 }
    {d8. e16 r8  d8 r2
    d8. e16 r8  d8 r2
    d8. e16 r8  d8 r2
    d8. e16 r8  d8 r2}
    >>
  }
  r1 r r r
  r1 r r r
  r1 r r r


  \relative c''{
    r8 
    <<{ d4 fs cs e8~ e b4 d a cs8~ cs g4 b fs a8~ a e4 g cs, d8~ }
      { fs4 a e g8~ g8 d4 fs cs e8~ e b4 d a cs8~ cs g4 b a fs8~ }
    >>
  }

  \relative c'{
    <<{ d8 d'4 fs cs e8~ e b4 d a cs8~ cs g4 b fs a8~ a e4 g cs, d8~ }
      { fs,8 fs'4 a e g8~ g8 d4 fs cs e8~ e b4 d a cs8~ cs g4 b a fs8~ }
    >>
  }

  \relative c'{
    <<{ d8 d'4 fs cs e8~ e b4 d a cs8~ cs g4 b fs a8~ a e4 g cs, d8~ }
      { fs,8 fs'4 a e g8~ g8 d4 fs cs e8~ e b4 d a cs8~ cs g4 b a fs8~ }
    >>
  }

  \relative c'{
    <<{ d8 d'4 fs cs e8~ e b4 d a cs8~ cs g4 b fs a8~ a e4 g cs,4. }
      { fs,8 fs'4 a e g8~ g8 d4 fs cs e8~ e b4 d a cs8~ cs g4 b a4. }
    >>
  }
  
  \relative c'{
    < d fs,>1 \fermata
  }

}

tourne={
  \relative c{
    <<
      {c8 e g b~ b g b4 c c,8 e g b~ b g b4 c}
      \\
      {c,,4 g'8 c r g c4 g c,4 g'8 c r g c4}
    >>
  }
}

findecycle={
  \relative c{
    <<
      {c8 e g b~ b g b4 c c,8 e g b~ b g b4 r}
      \\
      {c,,4 g'8 c r g c4 g c,4 g'8 c r g c4 r}
    >>
  }
}

compPartA={
  \clef F
  \key c \major
  \relative c{
    c,4 g'8 c r g c4 g c,4 g'8 c r g c4 r
    c,4 g'8 c r g c4 g c,4 g'8 c r g c4 r
  }
  \tourne
  \tourne
  \tourne
  \findecycle

  \tourne
  \tourne
  \tourne
  \findecycle

  \tourne
  \tourne
  \tourne
  \findecycle

  \tourne
  \tourne
  \tourne
  \findecycle

  \tourne
  \tourne
  \tourne
  \findecycle

  \relative c{
    \sl
    d4 d d d d
    d4 d d d d
    d4 d d d d
    d4 d d d d
    d4 d d d d
    d4 d d d d
    d4 d d d d
    d4 d d d d
    d4 d d d d
    d4 d d d d
    d4 d d d d
    \nsl
  }
  \tourne
  \tourne
  \tourne
  \tourne
  
  \tourne
  \tourne
  \tourne
  \findecycle

  \tourne
  \tourne
  \tourne
  \findecycle

  \tourne
  \tourne
  \tourne
  \findecycle

  \relative c{
    \sl
    d4 d d d d
    d4 d d d d
    d4 d d d d
    d4 d d d d
    d4 d d d d
    d4 d d d d
    d4 d d d d
    d4 d d d d
    d4 d d d d
    d4 d d d d
    d4 d d d d
    \nsl
  }
  \tourne
  \tourne
  
  \relative c{
    f,4. <f' a c>4. <g b d>4 <a c e>4
  }
  \break
}


partBinstru={
  \relative c{
    <d, fs' a d>8. <d fs' a d>16 r8 <d fs' a d>8 r2
    <d fs' a d>8. <d fs' a d>16 r8 <d fs' a d>8 r2
    <b' d fs b>8. <b d fs b>16 r8 <b d fs b>8 r2
    <b d fs b>8. <b d fs b>16 r8 <b d fs b>8 r2
    <fs cs' fs a>8. <fs cs' fs a>16 r8 <fs cs' fs a>8 r2
    <fs cs' fs a>8. <fs cs' fs a>16 r8 <fs cs' fs a>8 r2
    <a e' a cs>8. <a e' a cs>16 r8 <a e' a cs>8 r2
    <a e' a cs>8. <a e' a cs>16 r8 <a e' a cs>8 r2
  }
}

pianoBassD={
  \relative c,{ d8. d16 r8 d8 r16 a' d8 e a}
  \relative c,{ d8. d16 r8 d8 r16 a' d8 e a}
  \relative c,{ d8. d16 r8 d8 r16 a' d8 e a}
  \relative c,{ d8. d16 r8 d8 r16 a' d8 e a}
}

compPartB={
  \clef F
  \time 4/4
  \key d \major
  
  
  \partBinstru
  \partBinstru
  \bar "||"  
  \pianoBassD
  \pianoBassD
  \pianoBassD
  \pianoBassD
  \break
  \repeat volta 2{
    \pianoBassD
  }
  \pianoBassD
  \sl
    d4 d d d  d4 d d d  d4 d d d  d4 d d d
    d4 d d d  d4 d d d  d4 d d d  d4 d d d
    d4 d d d  d4 d d d  d4 d d d  d4 d d d
    d4 d d d  d4 d d d  d4 d d d  d4 d d d
    d4 d d d  d4 d d d  d4 d d d  d4 d d d
    d4 d d d  d4 d d d  d4 d d d  d4 d d d
    d4 d d d  d4 d d d  d4 d d d  d4 d d d
    d4 d d d  d4 d d d  d4 d d d  d4 d d d
  \nsl
  \clef G

  \relative c'{
    d2 e fs g a b cs d 
  }
  \relative c'{
    d2^"Trumpet" e fs g a b cs d 
  }
  \relative c'{
    d2 e fs g a b cs d 
  }
  \relative c'{
    d2 e fs g a b cs d 
  }

  \clef F
  \sl
    d4 d d d  d4 d d d  d4 d d d  d4 d d d
    d4 d d d  d4 d d d  d4 d d d  d4 d d d
    d4 d d d  d4 d d d  d4 d d d  d4 d d d
    d4 d d d  d4 d d d  d4 d d d  d4 d d d
  \nsl
  \relative c{
    d1 b fs' a,
    d b fs' a,  
    d, \fermata
  }
  
  \bar "|."
}


chordNamesPartA={
  \chordmode{ 
    s2. s2 s2. s2 s2. s2 s2. s2
    s2. s2 s2. s2 s2. s2 s2. s2  s2. s2 s2. s2 s2. s2 s2. s2
    s2. s2 s2. s2 s2. s2 s2. s2  s2. s2 s2. s2 s2. s2 s2. s2
    s2. s2 s2. s2 s2. s2 s2. s2  s2. s2 s2. s2 s2. s2 s2. s2
    s2. s2 s2. s2 s2. s2 s2. s2  s2. s2 s2. s2 s2. s2 s2. s2
    s2. s2 s2. s2 s2. s2 s2. s2  s2. s2 s2. s2 s2. s2 s2. s2

    f2. s2 a2.:m s2  c2. s2 e2. s2 f2. s2 c2. s2 
    f2. s2 a2.:m  s2  c2. s2 e2. s2 f2. s2 c2. s2 

    s2. s2 s2. s2 s2. s2 s2. s2  s2. s2 s2. s2 s2. s2 s2. s2
    s2. s2 s2. s2 s2. s2 s2. s2  s2. s2 s2. s2 s2. s2 s2. s2
    s2. s2 s2. s2 s2. s2 s2. s2  s2. s2 s2. s2 s2. s2 s2. s2
    s2. s2 s2. s2 s2. s2 s2. s2  s2. s2 s2. s2 s2. s2

    f2. s2 a2.:m s2  c2. s2 e2. s2 f2. s2 c2. s2 
    f2. s2 a2.:m  s2  c2. s2 e2. s2 f2. s2 c2. s2 
    s2. s2 s2. s2 
    
    s2. s2 s2. s2   
  }
}
chordNamesPartB={
  \chordmode{
    d1 s b:m s fs:m s a s
    d1 s b:m s fs:m s a s
    d1 s s s     s s s s     s s s s 
    s s s s      s s s s     s s s s
    d1 b:m fs:m  a
    d1 b:m fs:m  a
    d1 b:m fs:m  a
    d1 b:m fs:m  a
    d1 b:m fs:m  a
    d1 b:m fs:m  a
    d1 b:m fs:m  a
    d1 b:m fs:m  a
  }
}

wordsPartA = \lyricmode {
  Oh great in -- ten -- tions
  
  I've got the best of in -- ter -- ven -- tions
  But when the ads come
  
  I think a -- bout it now
  
  In my in -- fli -- ction
  En -- tre -- pre -- neu -- ri -- al con -- di -- tions
  Take us to glo -- ry
  I think a -- bout it now

  Can -- not con -- ver -- sa -- tions cull u -- ni -- ted na -- tions?
  If you got the pa -- tience, ce -- le -- brate the an -- cients
  Can -- not all cre -- a -- tion call it ce -- le  -- bra -- tion?
  Or u -- ni -- ted na -- tion, put it to your head

  Oh, great white ci -- ty
  I've got the a -- de -- quate co -- mmi -- ttee
  Where have your walls gone?
  I think a -- bout it now
  
  Chi -- ca -- go, in fa -- shion, the soft drinks, ex -- pan -- sion
  Oh, Co -- lum -- bi -- a
  From Pa -- ris, in -- cen -- tive, like Cream of Wheat in -- ven -- ted
  The Fer --ris Wheel
  
  Oh, great in -- ten -- tions
  Co -- ve -- nant with the i -- mi -- ta -- tion
  Have you no con -- science?
  I think a -- bout it now
  
  Oh, God of Pro -- gress
  Have you de -- gra -- ded or for -- got us?
  Where have your laws gone?
  I think a -- bout it now

  An -- cient hie -- ro -- gly -- phic or the South Pa -- ci -- fic
  Ty -- pi -- cally ter -- ri -- fic, bu -- sy and pro -- li -- fic
  Cla -- ssi -- cal de -- vo -- tion, ar -- chi -- tect pro -- mo -- tion
  La -- cking in e -- mo -- tion, think a -- bout it now

  Chi -- ca -- go, the New Age
  But what would Frank Lloyd Wright say?
  Oh, Co -- lum -- bi -- a!
  A -- muse -- ment or trea -- sure
  These o -- pti -- mi -- stic plea -- sures
  Like the Fer -- ris 

  Can -- not con -- ver -- sa -- tions cull u -- ni -- ted na -- tions?
  If you got the pa -- tience, ce -- le -- brate the an -- cients

  Co -- lum -- bi 
}

wordsPartB={
  \lyricmode{
    Ahhhh
  }
  s s s s s s s
  s s s s s s s
  s s s s s s s
  s s s s s s s
  s s s s s s s 
  s s s s s s s 
  s s s s s s s s s s
  \lyricmode{
  I cried my -- self to sleep last night
  And the ghost of Carl, he a -- pproached my wi -- in -- do -- o -- ow
  I was hy -- pno -- tized, I was asked
  To im -- pro -- vi -- i --  i -- i -- se
  On the at -- ti -- tude, the re -- gret
  Of a thou -- sand cen -- tu -- ries of death

  E -- ven with the heart of ter -- ror and the su -- per -- sti -- tious wea -- rer
  I am ri -- ding all a -- lone
  I am wri -- ting all a -- lone
  E -- ven in my best con -- di -- tion, coun -- ting all the su -- per -- sti -- tion
  I am ri -- ding all a -- lone
  I am run -- ning all 

 %	And we laughed  at t
 
  the be -- a -- ti -- tudes of a thou -- sand li -- i -- i -- i -- nes
  We were asked at the at -- ti -- tudes
  They re -- min -- ded u -- u -- u -- u -- us of death

  E -- ven with the rest be -- la -- ted, e -- very -- thing is an -- ti -- qua -- ted
  Are you wri -- ting from the heart?
  Are you wri -- ting from the heart?
  E -- ven in his heart the De -- vil has to know the wa -- ter le -- vel
  Are you wri -- ting from the heart?
  Are you wri -- ting from 
  
  %And I cried 
  -
  my -- self to sleep last night
  For the Earth, and ma -- te -- ri -- als, they may sound just ri -- i -- i -- i -- ight to me


  E-- ven with the rest be -- la -- ted, e -- very -- thing is an -- ti -- qua -- ted
  Are you wri -- ting from the heart?
  Are you wri -- ting from the heart?
  E -- ven in his heart the De -- vil has to know the wa -- ter le -- vel
  Are you wri -- ting from the heart?
  Are you wri -- ting from the heart?

  }
}

words={
  \wordsPartA
  \wordsPartB
}

theme={
  \themePartA
  \themePartB
}

section={
  \sectionPartA
  \sectionPartB
}

comp={
  \compPartA
  \compPartB
}

chordNames={
  \chordNamesPartA
  \chordNamesPartB
}


\book {
  \bookOutputName "come_on_feel_the_illinoise_C"
  \score{
  <<
    \global 

    \new ChordNames { 
      \transpose c c { \chordNames }
    }

    \new Staff \with { instrumentName = #"Sufjan <3" } {
      \transpose c c { \theme }
      \addlyrics { \words }
    }

    \new Staff \with { instrumentName = #"Brass" } {
      \transpose c c { \section }
    }

    \new Staff \with { instrumentName = #"Piano" } {
      \transpose c c { \comp }
    }

  >>
  \layout { }
  }
}

\book {
  \bookOutputName "come_on_feel_the_illinoise_Bb"
  \score{
  <<
    \global 

    \new ChordNames { 
      \transpose c d { \chordNames }
    }

    \new Staff \with { instrumentName = #"Sufjan <3" } {
      \transpose c d { \theme }
      \addlyrics { \words }
    }

    \new Staff \with { instrumentName = #"Brass" } {
      \transpose c d { \section }
    }

    \new Staff \with { instrumentName = #"Piano" } {
      \transpose c d { \comp }
    }

  >>
  \layout { }
  }
}

\book {
  \bookOutputName "come_on_feel_the_illinoise_Eb"
  \score{
  <<
    \global 

    \new ChordNames { 
      \transpose c a { \chordNames }
    }

    \new Staff \with { instrumentName = #"Sufjan <3" } {
      \transpose c a { \theme }
      \addlyrics { \words }
    }

    \new Staff \with { instrumentName = #"Brass" } {
      \transpose c a { \section }
    }

    \new Staff \with { instrumentName = #"Piano" } {
      \transpose c a { \comp }
    }

  >>
  \layout { }
  }
}
