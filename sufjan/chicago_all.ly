
\version "2.18.2"
\language "english"

\paper {
  system-system-spacing #'basic-distance = #1
  page-count = 1
}

%\include "chords.ly"
%\include "parts.ly"

\header {
  title = "Chicago"
  composer = "Sufjan Stevens"
}


global = {
  \set Score.skipBars = ##t
  \time 4/4
  \key g \major
}


\paper {
  system-system-spacing =
    #'((basic-distance . 10)
       (minimum-distance . 6)
       (padding . 0.4)
       (stretchability . 60))
} 

words = \lyricmode {
  I fell in love a -- gain
  All things go
  All things go
  Drove to chi -- ca --go

  All things know
  All things know
  
  We sold our clothes to the state
  I don't mind I don't mind 
  
  I made a lot of mis -- takes
  in my mind in my mind
  
  You came to take us 
  All things go
  All things go
  
  to re -- cre -- ate us
  All things grow
  All things grow
  
  We had our mind -- set 
  All things know 
  All things know 
  
  You had to find it
  All things go
  All things go

  

}

refrainstart={
  \relative c'{
    r2 
    <d a' d a'>8. <d a' d a'> <d a' d a'>8 <d a' d a'>2 <d g c g'>2
    r2 
    <e b' e b'>4 
    <d g c g'>
    <d g b g'>
    r
    <e b' e b'>
    <d g c g'>
  }
}

refrainloop={
  \relative c'{
    <c fs a fs'> 
    r 
    <d a' d a'>8. <d a' d a'> <d a' d a'>8 <d a' d a'>2 <d g c g'>2
    %d8. d d8 d2 c2
    %r2 e4 c b r d b
    r2 
    <e b' e b'>4 
    <d g c g'>
    <d g b g'>
    r
    <e b' e b'>
    <d g c g'>
  }
}



voice = {
  r1 r r r r r r r r r r r
  \relative c''{
    r2 a8. a a8 a4. g8 g4 r
    r2 b8 g4 g8~ g4 r b8 g4 a8~ a4
    r4 a8. a a8 a2 g4 r
    r2 b8 g4 g8~ g4 r b8 g4 a8~ a4
    r a8. b16~ b8. c16~ c8 b4. g4 e8 e
    r2 b'8 g4 g8~ g4 r b8 g4 a8~
    a4 r a8. b c8 b4 r g4 e8 e
    r2 b'8 g4 g8~ g4 r b8 g4 a8
  }
  \bar "||"
  \refrainstart
  \refrainloop
  \refrainloop
  \refrainloop
  
}

riffA={
  g4 a b g' fs2 d e c b g
}

other = {
  \relative c'{
    <a a'>2 <fs' fs'> <c c'> <g' g'> <e e'> <b' b'> 
    <<
      {<d d'> <d d'> }
      \\
      {g,4 a b g'}
    >>
    fs2 d e c b g
  }
  \relative c''{
    \riffA
    g4 a b g' fs1 
  }
  
  r1 r r r r r
  \relative c''{
      g4 a b g' 
      <<
        {fs1}
        \\
        \relative c'{
          <a a'>2 <fs' fs'> 
        }
      >>
      <c, c'> <g' g'> <e e'> <b' b'> 
      <d, d'> <d' d'>
      
      <a, a'>2 <fs' fs'> 
      <c c'> <g' g'> <e e'> <b' b'> 
      
    <<
      {<d, d'> <d d'> }
      \\
      {g4 a b g'}
    >>
    fs2 d e c b g

  }
}

cycle=\chordmode{d1 a:m c g}
chordNames = {
    \cycle \cycle \cycle \cycle
    \cycle \cycle \cycle \cycle
}

\score {
  <<
    \new ChordNames { 
      %\transpose bes c' {
         \transpose c c { 
           \chordNames 
          }
      %}
    }
    

    \new Staff  \with {
      instrumentName = #"Voice"
    }{ 
      \global
       
      \new Voice = "voice"{
        
        \global
        
        \transpose c c { 
          \voice
        }
      }
    }
    \new Lyrics \lyricsto "voice" {
       \words
    }


    \new Staff  \with {
      instrumentName = #"Other"
    }{ 
      \global
      %\new Voice = "lyrics"{
        \global
        
        \transpose c c {
          \other
        }
      %}
    }
    
    
  >>
  \layout { 
    
  }
  \midi { }
}
