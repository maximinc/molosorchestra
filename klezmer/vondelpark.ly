\version "2.18.2"
\language "english"

\paper {
   page-count=1
}
\header {
  title = "Vondelpark"
  composer = "Amsterdam Klezmer Band"
}


theme={
  \key e \minor
  \time 4/4
  \tempo 4 = 167

  \mark \default

  \relative c'{
    b8 c ds e fs g fs e g4 \tuplet 3/2{g8 fs e ds e g} fs4
    r8 
    
    b,8 c ds e g fs e g4 \tuplet 3/2{g8 fs e ds e g} fs4
    r8 b8 \tuplet 3/2 {b as g g fs e} e ds e4 \tuplet 3/2 {e8 ds c c b a}
    a8 gs a gs a b c e c e
    \time 2/4 ds4 r
    \time 4/4
  }
  \break

  \relative c'{
    b8 c ds e fs g fs e g4 \tuplet 3/2{g8 fs e ds e g} fs4
    r8 
    
    b,8 c ds e g fs e g4 \tuplet 3/2{g8 fs e ds e g} fs4
    r8 b8 \tuplet 3/2 {b as g g fs e} e ds e4 \tuplet 3/2 {e8 ds c c b a}
    a8 gs a gs a b c e c e
    \bar ".|:"
    \time 4/4 ds8 b
    <<{cs ds e fs g a}
      {as, b cs ds e fs}
    >>
    \mark \default
  }
  \break
  \relative c''{
    <<
    {
    r8 \appoggiatura {as} b4 b8 b4 b \tuplet 3/2 {r8 b8 c d c b b a g g fs e } \break
    r8 \appoggiatura {gs} a4 a8 a4 a \tuplet 3/2 {r8 a8 b c b a a g fs fs e ds } \break
    r8 \appoggiatura {fs} g4 g8 g4 g \tuplet 3/2 {r8 g8 as b as g g fs e e ds c }
    }
    {
    r8 \appoggiatura {fs} g4 g8 g4 g \tuplet 3/2 {r8 g8 a b a g g fs e e d c } \break
    r8 \appoggiatura {es} fs4 fs8 fs4 fs \tuplet 3/2 {r8 fs8 g a g fs fs e ds ds c b } \break
    r8 \appoggiatura {ds} e4 e8 e4 e \tuplet 3/2 {r8 e8 fs g fs e e ds c c b a }
    }
    >>
    r8 gs a b c e c e 
  }
  \bar ":|.|:"
  \break
  \mark \default
  \relative c'{ 
    ds b ds ds r b e e r b fs' fs r b, g' g r
    b, ds e fs g fs e g fs e g fs4 r
    \break
    r8 b, ds ds r b e e r b fs' fs r b, g' g
    r8 gs, a b c e c e
  }
  \bar "|."
  
}

bass={
  \key d \minor
  \relative c{
    a4. e'4 g8 a4 bf g a r
    a,4. e'4 g8 a4 bf g a r
    d2 c bf a g gs \time 2/4 a
  }

  \relative c{
    a4. e'4 g8 a4 bf g a r
    a,4. e'4 g8 a4 bf g a r
    d2 c bf a g gs a4 b c cs 
  }
  \relative c{
    d4. a4. c4 
    d4. f4. g4
    a4. e4. cs4
    a4. cs e4
    bf'4. f d4
    bf4. f' bf,4 
    g2 gs
  }
 
  \relative c{
    a4. e'4 g8 a4 bf g a r
    a,4. e'4 g8 a4 bf g a r
    a,4. e'4 g8 a4 bf g a r
    g2 bf
  }

}

chordNames=\chordmode{ 
  a1 bf2 a
  a1 bf2 a
  d1:m bf g:m a2
  
  a1 bf2 a
  a1 bf2 a
  d1:m bf g:m a2 s
  
  d1:m s a s bf s g:m
  
  a1 bf2 a
  a1 bf2 a
  a1 bf2 a
  g:m

}

% structure=\markup{
%  % Structure: thème x N, terminer avec mesures 21 à 24 x 3
% }

\book {

  \bookOutputName "Vondelpark_Bb"
  \score{
  <<
    \new ChordNames { 
      \transpose c d { \chordNames }
    }

    \new Staff \with { instrumentName = #"Theme (Bb)" } {
      \transpose c c { \theme }
    }

    \new Staff \with { instrumentName = #"Bass (Bb)" } {
      \transpose c d  {   \clef F \bass }
    }

  >>
    \layout { #(layout-set-staff-size 16) 
    }
  }
}

\book {

  \bookOutputName "Vondelpark_C"
  \score{
  <<
    \new ChordNames { 
      \transpose c c { \chordNames }
    }

    \new Staff \with { instrumentName = #"Theme (C)" } {
      \transpose c bf, { \theme }
    }

    \new Staff \with { instrumentName = #"Bass (C)" } {
      \transpose c c  {   \clef F \bass }
    }

  >>
    \layout { #(layout-set-staff-size 16) 
    }
  }
}

\book {

  \bookOutputName "Vondelpark_Eb"
  \score{
  <<
    \new ChordNames { 
      \transpose c a { \chordNames }
    }

    \new Staff \with { instrumentName = #"Theme (Eb)" } {
      \transpose c g { \theme }
    }

    \new Staff \with { instrumentName = #"Bass (Eb)" } {
      \transpose c a'	  {   \clef G \bass }
    }

  >>
    \layout { #(layout-set-staff-size 16) 
    }
  }
}

