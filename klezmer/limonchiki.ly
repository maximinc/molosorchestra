\version "2.18.2"
\language "english"

\header {
  title = "Limonchiki"
  composer = ""
}


themeA={
  \tempo 4=160
  \key g \minor
  \mark \default
  \relative c'{
    d8 fs a d cs d a8. g16 fs g a8 g16 fs ef8 d fs a4
    d,8 fs a d cs d a8. g16 fs g a8 g16 fs ef8 d4 d
  }
  \mark \default
  \break
  \relative c''{
    r8 d4 cs8 d4 bf8 g bf8. a16 bf8 cs d4 bf8 g
    r8 c4 bf8 a g~ g4 fs8. g16 fs8 ef 
    <<
          \magnifyMusic 0.63 {d'16 ef fs d ef fs g a}\\
      {d,,4 d}
    >>
  }
  \break
  \relative c''{
    \magnifyMusic 0.63 {bf'8}
    d,4 cs8 d4 bf8 g bf8. a16 bf8 cs d4 bf8 g
    \mark \markup { \musicglyph #"scripts.coda" }
    r8 c4 bf8 a g~ g4 fs8. g16 fs8 ef d2
  }
  \bar "|."
  \break
  \mark \markup { \musicglyph #"scripts.coda" }
  \relative c''{
    r8 c4 bf8 a gs16 a as b c cs d4 a d, r
  }
  \bar "|."
  
}
themeB={
  \tempo 4=160
  \key g \minor
  \mark \default
  \relative c'{
    d8 fs a d cs d a8. g16 fs g a8 g16 fs ef8 d fs a4
    d,8 fs a d cs d a8. g16 fs g a8 g16 fs ef8 d4 d
  }
  \relative c'{
    r8 d4 cs8 d4 bf8 g bf8. a16 bf8 cs d4 bf8 g
    r8 ef'4 d8 c bf~ bf4 a8. bf16 a8 g fs4 fs
    r8 bf4 a8 bf4 g8 d g8. d16 g8 a bf4 g8 d
    \mark \markup { \musicglyph #"scripts.coda" }
    r8 ef'4 d8 c bf~ bf4 a8. bf16 a8 g fs2
  }
}

themeC={
  \tempo 4=160
  \key g \minor
  \mark \default
  \relative c'{
    <<{r8 fs16 fs fs8 d fs16 fs fs8 d fs
    r8 g16 g g8 ef fs16 fs fs8 d fs
    r8 fs16 fs fs8 fs fs16 fs fs8 d fs
    r8 g16 g g8 ef fs16 fs fs8 d fs}
      \\
    {r8 d16 d d8 a d16 d d8 a d
    r8 ef16 ef ef8 bf d16 d d8 a d
    r8 d16 d d8 a d16 d d8 a d
    r8 ef16 ef ef8 ef d c bf a}
    >>
  }
  \relative c''{
    r8 g16 g d8 g g g16 g d8 g
    r8 g16 g d8 g g g16 g d8 g
    a8 a g g fs fs ef4 d8. ef16 d8 c d4 r

    r8 g16 g d8 g g g16 g d8 g
    r8 g16 g d8 g g g16 g d8 g
    a8 a g g fs fs ef4 d8. ef16 d8 c d4 r
  }
}


grid={
  \chordmode{
    d1 c2:m d d1 c2:m d
    g1:m g:m c:m d
    g:m g:m c:m d:7
    
    c:m d:7
  }
}

gridC={
  \chordmode{
    d1 c:m g:m 
  }
}

dM={
  \relative c{
    r8 <fs a d>8 r8 <fs a d>8 r8 <fs a d>8 r8 <fs a d>8
  }
}
cm={
  \relative c'{
    r8 <g c ef> r8 <g c ef> r8 <g c ef> r8 <g c ef>
  }
}

gm={
  \relative c'{
    r8 <g bf d> r8 <g bf d> r8 <g bf d> r8 <g bf d>
  }
}



compTuto={
  \dM    \bar "||"
  \cm \bar "||"
  \gm	 \bar "|."
}

paroles=\lyricmode{
}


structure=\markup{
  \left-column{
    \line{A en boucle}
    \line{AB x 2 (sans voix 3)}
    \line{AB x 2 (solos)}
    \line{AB x 2 avec voix 3}
    \line{AB x 2 en accélérant & coda}
  }
}

\book {
  \bookOutputName "limonchiki_C"
  \score{
  <<
    \new ChordNames { 
      \transpose c c { \grid }
    }
    \new Staff \with { instrumentName = #"Theme 1 (C)" } {
      \set Staff.midiInstrument = "clarinet"
      \new Voice = "voice"{
        \transpose c c {
          \themeA
        }
      }
    }
    \new Lyrics \lyricsto "voice" { \paroles }
    \new Staff \with { instrumentName = #"Thème 2 (C)" } {
       \set Staff.midiInstrument = "tenor sax"
      \transpose c c {
        \themeB
      }
    }
    \new Staff \with { instrumentName = #"Comp 3 (C)" } {
       \set Staff.midiInstrument = "trumpet"
      \transpose c c {
        \themeC
      }
    }
  >>
  \layout {#(layout-set-staff-size 16)}
  %\midi{}
  }
  
 
  \score{
    <<
    \new ChordNames { 
      \transpose c c { \gridC }
    }
    \new Staff  \with { instrumentName = #"(C)" } {
      \mark Accords
      \transpose c c { \compTuto }
    }
    >>
  }
  \markup "Structure:"
  \structure
}

\book {
  \bookOutputName "limonchiki_Bb"
  \score{
  <<
    \new ChordNames { 
      \transpose c d { \grid }
    }
    \new Staff \with { instrumentName = #"Theme 1 (Bb)" } {
      \set Staff.midiInstrument = "clarinet"
      \new Voice = "voice"{
        \transpose c d {
          \themeA
        }
      }
    }
    \new Lyrics \lyricsto "voice" { \paroles }
    \new Staff \with { instrumentName = #"Thème 2 (Bb)" } {
       \set Staff.midiInstrument = "tenor sax"
      \transpose c d {
        \themeB
      }
    }
    \new Staff \with { instrumentName = #"Comp 3 (Bb)" } {
       \set Staff.midiInstrument = "trumpet"
      \transpose c d {
        \themeC
      }
    }
  >>
  \layout {#(layout-set-staff-size 16)}
  }
  
 
  \score{
    <<
    \new ChordNames { 
      \transpose c d { \gridC }
    }
    \new Staff  \with { instrumentName = #"(Bb)" } {
      \mark Accords
      \transpose c d { \compTuto }
    }
    >>
  }
  \markup "Structure:"
  \structure
}


\book {
  \bookOutputName "limonchiki_Eb"
  \score{
  <<
    \new ChordNames { 
      \transpose c a { \grid }
    }
    \new Staff \with { instrumentName = #"Theme 1 (Eb)" } {
      \set Staff.midiInstrument = "clarinet"
      \new Voice = "voice"{
        \transpose c a {
          \themeA
        }
      }
    }
    \new Lyrics \lyricsto "voice" { \paroles }
    \new Staff \with { instrumentName = #"Thème 2 (Eb)" } {
       \set Staff.midiInstrument = "tenor sax"
      \transpose c a {
        \themeB
      }
    }
    \new Staff \with { instrumentName = #"Comp 3 (Eb)" } {
       \set Staff.midiInstrument = "trumpet"
      \transpose c a {
        \themeC
      }
    }
  >>
  \layout {#(layout-set-staff-size 16)}
  }
  
 
  \score{
    <<
    \new ChordNames { 
      \transpose c a { \gridC }
    }
    \new Staff  \with { instrumentName = #"(Eb)" } {
      \mark Accords
      \transpose c a { \compTuto }
    }
    >>
  }
  \markup "Structure:"
  \structure
}
