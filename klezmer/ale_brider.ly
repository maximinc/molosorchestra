\version "2.18.2"
\language "english"

\header {
  title = "Ale Brider"
  composer = ""
}


themeA={
  \tempo 4=160
  \key d \minor
  \mark \default
  \relative c'{
    a8 d d e f f e d  f4 g f8 f e d a d d e f d g e f4 e d r
    \break
    c8 f f g a a g f a4 bf a8 a g f c f f g a f g e f4 e d8 a' a a
    \mark \default
    \break
    g8. f16 e4 r8 g g g f8. e16 d4 r8 f f f e4 a a8 g f g a2 r8 a a a
    \break
    g8. f16 e4 r8 g g g f8. e16 d4 r8 f f f e4 a a8 g f e d4 r r r
  }
  \bar "|."
}
themeB={
  \tempo 4=160
  \key d \minor
  \mark \default
  \relative c'{
    d8 f f g a a g f a4 bf a8 a g f d f f g a f bf g a4 g f r
    \break
    f8 a a bf c c bf a c4 d c8 c bf a f a a bf c a bf g a4 g f4 r
  }
  \mark \default
  \relative c'''{
    g16 a bf a g f e d cs bf a g f e f e d e f a d e f e d a f e d e f d 
    \break
    e f' e d cs d cs bf a bf a g f g f e d e f a d e f g a2
  }

  \relative c'''{
    g16 a bf a g f e d cs d cs bf a g f e d e f a d e f e d a f e d e f d
    \break
    e f' e d cs d cs bf a bf a g f g f e d e f a d e f e d4 r
    
  }
  \bar "|."
}


grid={
  \chordmode{
    d1:m d:m d:m a2:7  d:m
    f1   f   f   a2:7  d:m
    a1:7  d:m a:7 d:m a:7
    d:m a:7 d:m
  }
}

gridC={
  \chordmode{
    d1:m a:7  f
  }
}

dm={
  \relative c{
    r8 <f d'>8 r8 <f d'>8 r8 <f d'>8 r8 <f d'>8
  }
}
fM={
  \relative c'{
    r8 <a c>8 r8 <a c>8 r8 <a c>8 r8 <a c>8
  }
}
asept={
  \relative c'{
    r8 <g cs>8 r8 <g cs>8 r8 <g cs>8 r8 <g cs>8
  }
}



compTuto={
  \dm    \bar "||"
  \asept \bar "||"
  \fM	 \bar "|."
}

paroles=\lyricmode{
  Un mir zay -- nen a -- le bri -- der, 
  oy, oy, a -- le bri -- der, 	
  un mir zin -- gen shey -- ne li -- der, 
  oy, oy, oy. 
  
  Un mir halt -- n zikh in ey -- nem, 
  oy, oy, zikh in ey -- nem, 
  Azel -- khes iz ni -- to bay key -- nem, oy, oy, oy.
  
}


structure=\markup{
  \center-column{
    \line{A (Thème + oy-oy) B}
    \line{AB x 4 en accélérant}
    \line{B + fin 1 5 1}
  }
}


\book {
  \bookOutputName "ale_brider_C"
  \score{
  <<
    \new ChordNames { 
      \transpose c c { \grid }
    }
    \new Staff \with { instrumentName = #"Theme 1 (C)" } {
      \set Staff.midiInstrument = "clarinet"
      \new Voice = "voice"{
        \transpose c c {
          \themeA
        }
      }
    }
    \new Lyrics \lyricsto "voice" { \paroles }
    \new Staff \with { instrumentName = #"Thème 2 (C)" } {
       \set Staff.midiInstrument = "clarinet"
      \transpose c c {
        \themeB
      }
    }
  >>
  \layout {#(layout-set-staff-size 18) }
  }
 
  \score{
    <<
    \new ChordNames { 
      \transpose c c { \gridC }
    }
    \new Staff  \with { instrumentName = #"(C)" } {
      \mark Accords
      \transpose c c { \compTuto }
    }
    >>
  }
  \markup "Structure:"
  \structure
}

\book {
  \bookOutputName "ale_brider_C_clef_ut"
  \score{
  <<
    \new ChordNames { 
      \transpose c c { \grid }
    }
    \new Staff \with { instrumentName = #"Theme 1 (C)" } {
      \set Staff.midiInstrument = "clarinet"
      \new Voice = "voice"{
        \transpose c c {
          \clef C
          \themeA
        }
      }
    }
    \new Lyrics \lyricsto "voice" { \paroles }
    \new Staff \with { instrumentName = #"Thème 2 (C)" } {
       \set Staff.midiInstrument = "clarinet"
      \transpose c c, {
        \clef C
        \themeB
      }
    }
  >>
  \layout {#(layout-set-staff-size 18) }
  }
 
  \score{
    <<
    \new ChordNames { 
      \transpose c c { \gridC }
    }
    \new Staff  \with { instrumentName = #"(C)" } {
      \mark Accords
      \transpose c c { \compTuto }
    }
    >>
  }
  \markup "Structure:"
  \structure
}

\book {
  \bookOutputName "ale_brider_Bb"
  \score{
  <<
    \new ChordNames { 
      \transpose c d { \grid }
    }
    \new Staff \with { instrumentName = #"Theme 1 (Bb)" } {
      \set Staff.midiInstrument = "clarinet"
      \new Voice = "voice"{
        \transpose c d {
          \themeA
        }
      }
    }
    \new Lyrics \lyricsto "voice" { \paroles }
    
    \new Staff \with { instrumentName = #"Thème 2 (Bb)" } {
      \set Staff.midiInstrument = "clarinet"
      \transpose c d {
        \themeB
      }
    }
  >>
  \layout { #(layout-set-staff-size 17)}
  }
 
  \score{
    <<
    \new ChordNames { 
      \transpose c d { \gridC }
    }
    \new Staff  \with { instrumentName = #"(Bb)" } {
      \mark Accords
      \transpose c d { \compTuto }
    }
    >>
  }
  \markup "Structure:"
  \structure
}


\book {
  \bookOutputName "ale_brider_Eb"
  \score{
  <<
    \new ChordNames { 
      \transpose c a { \grid }
    }
    \new Staff \with { instrumentName = #"Theme 1 (Eb)" } {
      \set Staff.midiInstrument = "clarinet"
        \new Voice = "voice"{
          \transpose c a {
          \themeA
        }
      }
    }
    \new Lyrics \lyricsto "voice" { \paroles }
    \new Staff \with { instrumentName = #"Thème 2 (Eb)" } {
       \set Staff.midiInstrument = "clarinet"
      \transpose c a {
        \themeB
      }
    }
  >>
  \layout {#(layout-set-staff-size 17) }
  }
 
  \score{
    <<
    \new ChordNames { 
      \transpose c a { \gridC }
    }
    \new Staff  \with { instrumentName = #"(Eb)" } {
      \mark Accords
      \transpose c a { \compTuto }
    }
    >>
  }
  \markup "Structure:"
  \structure
}
