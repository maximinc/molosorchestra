\version "2.18.2"
\language "english"

\header {
  title = "Galitsianer tentsl"
  composer = ""
}


theme={
  \key f \minor
  \time 4/4
  \tempo 4 = 100

  \mark \default

  \bar ".|:"
  \relative c'{
    c16 e g e f e f af g e f e f e f af g f g af bf g af f g e f df e c df bf
    c16 e g e f e f af g e f e f e f af g f g af bf g af f g e f df c4
  }
  \bar ":|.|:"
  \break
  \mark \default


  \relative c''{
    c8 c4. bf16 a bf c df c bf a bf8 ef df c bf8. af16 g f ef d ef8 ef8~ef16 
    bf16 c d ef d ef f ef8. g16 g f f ef ef d d c d ef c8 ~ c8 \parenthesize <c g'>
  }
  
  \bar ":|.|:"
  \break
  \mark \default
  \relative c'{
    \repeat volta 2{
      f4 ef \tuplet 3/2 {d8 bf df } c4
      bf16 ef d ef f g gs a bf g ef g f8. f16 
      g f d' f, f ef c' ef, ef d b' d, d c d c 
    }
    \alternative{
      {c af' g f ef f d ef c4 c16 d ef e }
    
      {c af' g f ef f d ef c4 r }
    }
  }  
  \bar "|."
}


cdm=\relative c{ r8 <f a d> r8 <f a d> }
ca=\relative c{r8 <e a cs> r8 <e a cs> }
cgm=\relative c'{r8 <g bf d> r8 <g bf d>}

comp={
  \key f \minor
  
  \relative c{
    <e g c>8 r r <f af c> r r <f af c> r
    <e g c>8 r r <f bf df> r r <f bf df> r
    <e g c>8 r r <f af c> r r <f af c> r
    <e g c>8 r r <f bf df> r r <e g c> r
  }
  
  <<
  \relative c'{
    ef8 ef4. g16 f g af bf af g f g8 c bf af g8. f16 ef d c8  bf8 bf8~ bf16 
    d16 ef f g fs g af g8. b16 b af af g g f f ef f g ef8 ~ ef8 s %\parenthesize <ef bf'>
  }
  \\
  \relative c{
   r8 <ef g c> r8 <ef g c> r8 <f bf df>  r8 <f bf df>
   r8 <ef g bf> r8 <ef g bf> r8 <d f bf> r8 <d f bf>
   r8 <ef g bf> r8 <ef g bf> r8 <ef g bf>r8 <ef g bf>
   r8 <d g b> r8 <d g b> r8 <ef g c> r8 <ef g c>
  }
  >>
  
  \relative c''{
    af4 g \tuplet 3/2 {bf8 f af} g4
  }
  
  \relative c{
    r8 <ef g bf> r8 <ef g bf> r8 <ef g bf> <d f bf> r8
    r8 <d f b> r8 <ef g c> r8 <d g b> r8 <ef g c>
    
    r8 <f af c> r8 <d g b> <ef g c>4 r8 r8
    r8 <f af c> r8 <d g b> <ef g c>4 r
  }

}

chordNames=\chordmode{ 

  c4.  f:m f4:m c4. bf:m bf4:m 
  c4.  f:m f4:m c4. bf:m c4
  
  c2:m  bf:m ef2 bf2 ef1 g2:7 c:m
  
  r1 ef2. bf4 f4:dim c:m g:7 c:m  f:m g:7 c2:m f4:m g:7 c:m
}

structure=\markup{
 % Structure: thème x N, terminer avec mesures 21 à 24 x 3
}

\book {
  \bookOutputName "galitsianer_tentsl_C"
  \score{
  <<
    \new ChordNames { 
      \transpose c c { \chordNames }
    }

    \new Staff \with { instrumentName = #"Theme (C)" } {
      \transpose c c { \theme }
    }

    \new Staff \with { instrumentName = #"Comp (C)" } {
      \transpose c c { \comp }
    }

  >>
  %\layout { #(layout-set-staff-size 16) }
  \layout {}
  \midi { }
  }
  \structure
}

\book {
  \bookOutputName "galitsianer_tentsl_C_clef_ut"
  \score{
  <<
    \new ChordNames { 
      \transpose c c { \chordNames }
    }

    \new Staff \with { instrumentName = #"Theme (C)" } {
      \clef C
      \transpose c c { \theme }
    }

    \new Staff \with { instrumentName = #"Comp (C)" } {
      \clef C
      \transpose c c { \comp }
    }

  >>
  %\layout { #(layout-set-staff-size 16) }
  \layout {}
  }
  \structure
}

\book {
  \bookOutputName "galitsianer_tentsl_Bb"
  \score{
  <<
    \new ChordNames { 
      \transpose c d { \chordNames }
    }

    \new Staff \with { instrumentName = #"Theme (Bb)" } {
      \transpose c d { \theme }
    }

    \new Staff \with { instrumentName = #"Comp (Bb)" } {
      \transpose c d { \comp }
    }

  >>
  %\layout { #(layout-set-staff-size 16) }
  \layout {}
  }
  \structure
}

\book {
  \bookOutputName "galitsianer_tentsl_Eb"
  \score{
  <<
    \new ChordNames { 
      \transpose c a { \chordNames }
    }

    \new Staff \with { instrumentName = #"Theme (Eb)" } {
      \transpose c a { \theme }
    }

    \new Staff \with { instrumentName = #"Comp (Eb)" } {
      \transpose c a { \comp }
    }

  >>
  %\layout { #(layout-set-staff-size 16) }
  \layout {}
  }
  \structure
}


\book {
  \bookOutputName "galitsianer_tentsl_A_Bb"
  \score{
  <<
    \new ChordNames { 
      \transpose c b { \chordNames }
    }

    \new Staff \with { instrumentName = #"Theme (Bb)" } {
      \transpose c b { \theme }
    }

    \new Staff \with { instrumentName = #"Comp (Bb)" } {
      \transpose c b { \comp }
    }

  >>
  %\layout { #(layout-set-staff-size 16) }
  \layout {}
  }
  \structure
}

