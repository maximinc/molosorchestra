\version "2.18.2"
\language "english"

\header {
  title = "Nakht in Gan Eden"
  composer = ""
}


themeA={
  \tempo 4=160
  \key d \minor
  
  \relative c'{
    s2 s8
    a8 d e 
    \mark \default
    \repeat volta 2{
      f4 e8 d f4 e8 d e f d4 
      r8 c f g 
      a4 g8 f a4 g8 f g a f4 
      r8 c f a
      \break
      c4 bf a g8 f f g f e d4
      
      r8 c 
      \mark \markup { \musicglyph #"scripts.coda" }
      f g~ g f \tuplet 3/2 {a g f f e d}  
    }
    \alternative{
      {d4 r r8 a d e}
      { d4 r r a'}
    }
  }
  \break
  \mark \default
  \repeat volta 2{
    \relative c''{ 
      a8 d d d d a f'16 e d8 cs d bf a g4 r8
      g fs g a bf cs d cs bf a4 r r
    }
    \relative c'{
      c4 c8 f f a c c bf a d g, g g g a16 g f8 e 
    }
  }
  \alternative{
    \relative c' {d4 a'8 f g g f g a2 d4.\trill a8}
    \relative c' {d4 a'8 f g g f e d4 r r8 a' bf a }
  }
  \break
  \mark \default
  \repeat volta 2{
    \relative c''{
      d2 ~ d8 a8 f e d2  r8 a' bf a f'2~  \tuplet 3/2 {f8 e d cs d e} d2 r8 c b c
      \break
      d ef d c b c bf a b c bf a8 \mordent ~ a g\mordent g f8 f g4 f8
      \tuplet 3/2 {a g f f e d}  
    }
  }
  \alternative{
    \relative c'{d2 r8 a' bf a}
    \relative c'{d2 r8 a d e}
  }
  \bar "||"
 
  \break  
  \mark \markup { \musicglyph #"scripts.coda" }
  \relative c'{
    f8 g~ g f a8 gs16 a as b c cs d4 a d, r \bar "|."
  }
}

themeB={
  \tempo 4=160
  \key d \minor  
  s2 s8 r8 r r
  \repeat volta 2{
    \relative c'{
      r8 <d f>8 r8 <d f>8 r8 <d f>8 r8 <d f>8
      r8 <d f>8 r8 <d f>8 r8 <d f>8 r4
      <c f>2 <a c> <c f>4 <a c>4
      r8 <a c>8 r8 <a c>8
      
      %r1 r1 
      r8 <a c>8 r8 <a c>8 r8 <a c>8 r8 <a c>8
      <a cs>2 <a d>4 r
      
      
      <a cs>8 <a cs>4 <a cs>8 <a cs>8 <a cs>4 r8
      
    }
  }
  \alternative {
    \relative c'{ r8 bf a f d4 r}
    \relative c'{ r8 bf a f d4 r}
  }
  \repeat volta 2{
    \relative c{
      r8 <f a> r8 <f a> r8 <f a> r8 <f a> 
      <fs a>2 g8 d' bf g fs g a bf cs d cs bf
      <<{a2}\\
        {a8 cs e cs}
      >>
      <<
        {\stemUp
          c'8 b16 d c4}\\
        {\stemDown a,4 r}
      >> 
      c1 d       
    }
  }
  \alternative {
    \relative c' {r1 r8 f e d bf a f e}
    \relative c' {r1 r8 bf a f 
          <<
            {r8 a8 bf a}\\
            {d,4 r}
          >>
    }
  }
  \repeat volta 2{
    \relative c{
      r8 d16 d f8 a d2
      r8 d a f d2 f'2~ 
      \tuplet 3/2 {f8 e d cs d e} d2 r
      f2 f4 f f f f f 
      <a, cs>8 <a cs>4 <a cs>8 <a cs>8 <a cs>4 r8
    }
  }
  \alternative{
    \relative c' {r8 bf a f d4 r}
    \relative c' {r8 bf a f d4 r}
  }
  \bar "||"
  \break
  \mark \markup { \musicglyph #"scripts.coda" }
  \relative c'{
    <a cs>8 <a cs>4 <a cs>8 <a cs>8 <a cs>4 <a cs>8
    <a d>4 <f a> <d f> r \bar "|."
  }
}


grid={
  \chordmode{
    s1 
    d1:m d:m f f
    f a2:7 d:m a1:7 d:m d:m
    
    d:m d2:7 g:m g1:m a2.:7 c4 f1 
    g1:m 
      d2:m/a a:7 d1:m 
      d2:m/a a:7 d1:m 
    
    d:m d:m d:m d2:m d:7
    f1 f2 g:m a1:7 d:m 
            d:m
    a:7 d:m
  }
}

paroles=\lyricmode{
}


structure=\markup {"Structure: ABC ABC A(avec reprise) to coda"}

\book {
  \bookOutputName "nakht_in_gan_eden_C"
  \score{
  <<
    \new ChordNames { 
      \transpose c c { \grid }
    }
    \new Staff \with { instrumentName = #"Theme 1 (C)" } {
      \set Staff.midiInstrument = "clarinet"
      \new Voice = "voice"{
        \transpose c c {
          \themeA
        }
      }
    }
    \new Lyrics \lyricsto "voice" { \paroles }
    \new Staff \with { instrumentName = #"Thème 2 (C)" } {
       \set Staff.midiInstrument = "trombone"
      \transpose c c {
        \themeB
      }
    }
  >>
  \layout { #(layout-set-staff-size 16)}
  %\midi{}
  }  
  \structure
}

\book {
  \bookOutputName "nakht_in_gan_eden_C_clef_ut"
  \score{
  <<
    \new ChordNames { 
      \transpose c c { \grid }
    }
    \new Staff \with { instrumentName = #"Theme 1 (C)" } {
      \set Staff.midiInstrument = "clarinet"
      \new Voice = "voice"{
        \transpose c c {
          \clef C
          \themeA
        }
      }
    }
    \new Lyrics \lyricsto "voice" { \paroles }
    \new Staff \with { instrumentName = #"Thème 2 (C)" } {
       \set Staff.midiInstrument = "trombone"
      \transpose c c {
        \clef C
        \themeB
      }
    }
  >>
  \layout { #(layout-set-staff-size 16)}
  %\midi{}
  }  
  \structure
}
\book {
  \bookOutputName "nakht_in_gan_eden_Bb"
  \score{
  <<
    \new ChordNames { 
      \transpose c d { \grid }
    }
    \new Staff \with { instrumentName = #"Theme 1 (Bb)" } {
      \set Staff.midiInstrument = "clarinet"
      \new Voice = "voice"{
        \transpose c d {
          \themeA
        }
      }
    }
    \new Lyrics \lyricsto "voice" { \paroles }
    \new Staff \with { instrumentName = #"Thème 2 (Bb)" } {
       \set Staff.midiInstrument = "trombone"
      \transpose c d {
        \themeB
      }
    }
  >>
  \layout { #(layout-set-staff-size 16)}
  }
  \structure
}

\book {
  \bookOutputName "nakht_in_gan_eden_Eb"
  \score{
  <<
    \new ChordNames { 
      \transpose c a { \grid }
    }
    \new Staff \with { instrumentName = #"Theme 1 (Eb)" } {
      \set Staff.midiInstrument = "clarinet"
      \new Voice = "voice"{
        \transpose c a {
          \themeA
        }
      }
    }
    \new Lyrics \lyricsto "voice" { \paroles }
    \new Staff \with { instrumentName = #"Thème 2 (Eb)" } {
       \set Staff.midiInstrument = "trombone"
      \transpose c a {
        \themeB
      }
    }
  >>
  \layout { #(layout-set-staff-size 16)}
  }
  
  \structure
}
