\version "2.18.2"
\language "english"

\header {
  title = "Varshaver Freylekhs"
  composer = ""
}

themeA={
  \repeat volta 2{
  \relative c'{
    d8 g r bf d g, ef' g, d' g, cs g bf16 a g8 d'4
    d,8 g r bf cs d e f e16 f g8 f16 e d8 d16 cs bf8 d4
    \break
    d,8 g r bf d g, ef' g, d' g, cs g bf16 a g8 d'4
    d,8 e fs g a bf cs d d16 cs bf8 bf16 a g8 g4^"Fine" r
  }
  }
}
themeB={
  \repeat volta 2{
  \relative c''{
    e16 g f4 e8 g4 f16 e d8 cs16 e d4 cs8 d16 cs bf8 bf16 a g8
    \break
    e'16 g f4 e8 g4 f16 e d8 d2 g2
    \break
    e16 g f4 e8 g4 f16 e d8 cs16 e d4 cs8 d16 cs bf8 bf16 a g8
  }
  \break
  \relative c'{
    d8 e fs g a bf cs d d16 cs bf8 bf16 a g8 g4 r
  }
  }
}

themeC={
  \repeat volta 2{
  \relative c''{
    <<
      {g8 b r d c b d4 d8. e16 f e d8 c8 b d4
       g,8 b r d c b d4 a8. b16 c b a8 g4 r }
      {d8 g r b a g b4 a8. b16 c b a8 a g b4
       d,8 g r b a g b4 fs8. g16 a g fs8 g4}
    >>
  }
  \relative c''{
    <<
      {g8 b r d c b d4 d8. e16 f e d8 c8 b d4
       g,8 b r d c b d4 a8. b16 c b a8 g4 r }
      {d8 g r b a g b4 a8. b16 c b a8 a g b4
       d,8 g r b a g b4 fs8. g16 a g fs8 g4}
    >>
  }
  }
}


theme={
  \tempo 4=160
  \key g \minor
  \mark \default
  \themeA
%   \break
%   \relative c'{
%     d8 g r bf d g, ef' g, d' g, cs g bf16 a g8 d'4
%     d,8 g r bf cs d e f e16 f g8 f16 e d8 d16 cs bf8 d4
%     \break
%     d,8 g r bf d g, ef' g, d' g, cs g bf16 a g8 d'4
%     d,8 e fs g a bf cs d d16 cs bf8 bf16 a g8 g4 r
%   }
  
  \mark \default
  \break
  \themeB
  \mark \default
  \key g \major
  \break
  \themeC
}

themesax={
  \tempo 4=160
  \key g \minor
  \mark \default
  \transpose c c, {\themeA}
  
  \mark \default
  \break
  \transpose c c,{\themeB}
  \mark \default
  \key g \major
  \break
  \themeC
}


gm={
  \relative c'{
    r8 <g bf d> r <g bf d> r <g bf d> r <g bf d>
  }
}
dsept={
  \relative c{
    r8 <fs a c>8 r <fs a c>8 r <fs a c>8  r <fs a c>8 
  }
}

gM={
  \relative c'{
    r8 <g b d> r <g b d> r <g b d> r <g b d>
  }
}

compA={
  \key g \minor
  \relative c'{
    \repeat percent 6 { \gm }
    \dsept
  }
  \relative c{
    <fs c'>8 <fs c'>8 r <fs c'>8
    <g bf>8 <g bf> r <g bf>
  }
  \relative c'{
    \repeat percent 6 { \gm }
    \dsept
  }
  \relative c{
    <fs c'>8 <fs c'>8 r <fs c'>8
    <g bf>8 <g bf> r <g bf>
  }
  \key g \major
  \relative c'{
    \gM \gM \gM 
  }
  \relative c{
    <fs a>8 <fs a>8 r <fs a>8
    <g b>8 <g b> r <g b>
  }
  \relative c'{
    \gM \gM \gM 
  }
  \relative c{
    <fs a>8 <fs a>8 r <fs a>8
    <g b>8 <g b> r <g b>
  }
}

gmB={
  \relative c'{
    <g bf d>4.-. <g bf d>4.-. <g bf d>4
  }
}
dseptB={
 \relative c{
   <fs a c>4.-. <fs a c>4.-. <fs a c>4
 }
}
gMB={
  \relative c'{
    <g b d>4.-. <g b d>4.-. <g b d>4
  }
}

compB={
  \key g \minor
  \repeat percent 6 {\gmB }
  \dseptB 
  \relative c{
    <fs c'>4 <fs c'> <g bf>4 <g bf>
  }
  \repeat percent 6 {\gmB }
  \dseptB 
  \relative c{
    <fs c'>4 <fs c'> <g bf>4 <g bf>
  }
  \repeat percent 6 {\gMB }
  \dseptB 
  \relative c{
    <fs c'>4 <fs c'> <g b>4 <g b>
  }
}

grid={
  \chordmode{
    g1:m s g1:m s
    g1:m s d:7 d2:7 g:m

    g1:m s g1:m s
    g1:m s d:7 d2:7 g:m

    g1 s g1 d2:7 g
    g1 s g1 d2:7 g
  }
}

bassA={
  \clef F
  \key g \minor
  \relative c{
    \repeat percent 6 { g4 d g d }
    d a' d, a' d, a' g d
  }
  \relative c{
    \repeat percent 6 { g4 d g d }
    d a' d, a' d, a' g d
  }
  \key g \major
  \relative c{
    \repeat percent 3 { g4 d g d }
    d a' g d
  }
  \relative c{
    \repeat percent 3 { g4 d g d }
    d a' g d
  }
}

bassB={
  \clef F
  \key g \minor
  \relative c{
    \repeat percent 6 { g4. bf4. d4 }
    d4. fs4. a4 d,4 fs g4 d
  }
  \relative c{
    \repeat percent 6 { g4. bf4. d4 }
    d4. fs4. a4 d,4 fs g4 d
  }
  \key g \major
  \relative c{
    \repeat percent 3 { g4 d g d }
    d a' g d
  }
  \relative c{
    \repeat percent 3 { g4 d g d }
    d a' g d
  }
}

compTutoA={
  \gmB \bar "||"
  \dseptB \bar "||"
  \gMB \bar "|."
  \break
}
compTutoB={
  \gm \bar "||"
  \dsept \bar "||"
  \gM \bar "|."
}


gridC={
  \chordmode{g1:m d:7 g}
}

structure=\markup{
  \left-column{
    \line{Doina sur AB}
    \line{tourne freylekhs}
    \line{AB freylekhs, C en umpah sans voix 2}
    \line{AB freylekhs, C en umpah + voix 2}
    \line{ABC umpah en accélérant}
    \line{A (avec reprise to fine)}
  }
}

\book {
  \bookOutputName "varshaver_freylekhs_C"
  \score{
  <<
    \new ChordNames { 
      \transpose c c { \grid }
    }
    \new Staff \with { instrumentName = #"Theme (C)" } {
      \transpose c c {
        \theme
      }
    }
    
%     \new Staff \with { instrumentName = #"Comp (C)" } {
%       \transpose c c { \compB }
%     }
%     
%     \new Staff \with { instrumentName = #"Bass (C)" } {
%       \transpose c c { \bassB }
%     }

  >>
  \layout { }
  }
 
  \score{
    <<
    \new ChordNames { 
      \transpose c c { \gridC \gridC }
    }
    \new Staff  \with { instrumentName = #"(C)" } {
      \mark "Freylekhs"
      \transpose c c { \compTutoA }
      \mark "Um-pah"
      \transpose c c { \compTutoB }
    }
    >>
  }
  \markup "Structure:"
  \structure
}

\book {
  \bookOutputName "varshaver_freylekhs_C_clef_ut"
  \score{
  <<
    \new ChordNames { 
      \transpose c c { \grid }
    }
    \new Staff \with { instrumentName = #"Theme (C)" } {
      \transpose c c, {
        \clef C
        \theme
      }
    }
  >>
  \layout { }
  }
 
  \score{
    <<
    \new ChordNames { 
      \transpose c c { \gridC \gridC }
    }
    \new Staff  \with { instrumentName = #"(C)" } {
      \mark "Freylekhs"
      \clef C
      \transpose c c { \compTutoA }
      \mark "Um-pah"
      \clef C
      \transpose c c { \compTutoB }
    }
    >>
  }
  \markup "Structure:"
  \structure
}

\book {
  \bookOutputName "varshaver_freylekhs_Bb"
  \score{
  <<
    \new ChordNames { 
      \transpose c d { \grid }
    }
    \new Staff \with { instrumentName = #"Theme (Bb)" } {
      \transpose c d {
        \theme
      }
    }
  >>
  \layout { }
  }
 
  \score{
    <<
    \new ChordNames { 
      \transpose c d { \gridC \gridC }
    }
    \new Staff  \with { instrumentName = #"(Bb)" } {
      \mark "Freylekhs"
      \transpose c d { \compTutoA }
      \mark "Um-pah"
      \transpose c d { \compTutoB }
    }
    >>
  }
  \markup "Structure:"
  \structure
}

\book {
  \bookOutputName "varshaver_freylekhs_Eb"
  \score{
  <<
    \new ChordNames { 
      \transpose c a { \grid }
    }
    \new Staff \with { instrumentName = #"Theme (Eb)" } {
      \transpose c a {
        \themesax
      }
    }
  >>
  \layout { }
  }
 
  \score{
    <<
    \new ChordNames { 
      \transpose c a { \gridC \gridC }
    }
    \new Staff  \with { instrumentName = #"(Eb)" } {
      \mark "Freylekhs"
      \transpose c a { \compTutoA }
      \mark "Um-pah"
      \transpose c a { \compTutoB }
    }
    >>
  }
  \markup "Structure:"
  \structure
}


% Note: broken (added repeats for each section)
% \book {
%   \bookOutputName "varshaver_freylekhs_all"
% 
%   \score{
%     \unfoldRepeats{
%     <<
%       \new ChordNames { 
%         \transpose c c { \grid }
%       }
%       \new Staff \with { instrumentName = #"Theme (C)" } {
%         \set Staff.midiInstrument = "clarinet"
%         \transpose c c { \theme \theme }
%       }
%       
%       \new Staff \with { instrumentName = #"Comp (C)" } {
%         \set Staff.midiInstrument = "clarinet"
%         \transpose c c { \compB \compA }
%       }  
%       \new Staff \with { instrumentName = #"Bass (C)" } {
%         \set Staff.midiInstrument = "tuba"
%         \transpose c c { \bassB	 \bassA }
%       }  
%     >>
%     }
%     %\midi{}
%   }
% }