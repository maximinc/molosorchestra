\version "2.24.4"
\language "english"

\paper {
  page-count=1
  indent = 0
  %ragged-right = ##f
}

\header {
  title = "Horas (3/8)"
}

chords_bosnian_nigun= \new ChordGrid \chordmode {
  \time 3/8
  d4.:m s s s 
  d4.:m g4.:m d4.:m s  \break
  d4.:m s s s 
  d4.:m a4.:  d4.:m s \break
  \bar "||"
  
  d4.:m g4.:m g4.:m g4.:m 
  f4. s s s\break
  d4.:m g4.:m g4.:m g4.:m 
  a4. a4. d4.:m d4.:m
  \bar "||"
}

chords_nor_gelebt=\new ChordGrid \chordmode {
  \time 3/8
  
  \mark \default
  \bar ":.|.:"
  a4.:m s s s a:m  s s s\break
  d:m a:m e a:m d:m a:m e a:m  \break
  d:m a:m e a:m d:m a:m e a:m
  \break
  \bar ":.|.:"
  
  \mark \default
  a:m e/gs a:m d:m a:m s s s \break
  a:m e/gs a:m d:m e e a:m  a:m   \break
  \bar ":.|.:"
}      


gen_bosnian=
#(define-scheme-function
     (key)
     (ly:pitch?)
#{
  \score{
    \header {
      piece = "Bosnian Nigun"
    }
    \transpose c #key {
      \chords_bosnian_nigun
    }
  }
#})

gen_nor=
#(define-scheme-function
     (key)
     (ly:pitch?)
#{
  \score{
    \header {
      piece = "Nor gelebt nor gelakht"
    }
    \transpose c #key {
      \chords_nor_gelebt
    }
  }
#})

\book {
  \bookOutputName "horas_C"
  \gen_bosnian c
  \gen_nor c
}

\book {
  \bookOutputName "horas_Bb"
  \gen_bosnian d
  \gen_nor d
}

\book {
  \bookOutputName "horas_Eb"
  \gen_bosnian a
  \gen_nor a
}

