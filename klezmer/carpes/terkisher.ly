\version "2.24.4"
\language "english"

\paper {
  page-count=1
  indent = 0
  %ragged-right = ##f
}

\header {
  title = "Terkisher (4/4)"
}

chords_yiddish_hora= \new ChordGrid \chordmode {
  \time 4/4
  \mark \default
  c1 s c s
  \bar "||"
  c d:m e a:m
  \break
  c d:m g c
  \bar "||"
  c d:m c2 g:m a1:m
  \break
  \mark \default
  \bar ".|:-|"
  a:m s a:m s
  \bar "||"
  a:m s a:m s
  \break
  a d:m a s
  \bar "||"
  g:m a a2 g:m a1:m
  \bar ":|."
  
}

gen_yiddish=
#(define-scheme-function
     (key)
     (ly:pitch?)
#{
  \score{
    \header {
      piece = "Yiddish Hora"
    }
    \transpose c #key {
      \chords_yiddish_hora
    }
  }
#})

\book {
  \bookOutputName "terkisher_C"
  \gen_yiddish c
}

\book {
  \bookOutputName "terkisher_Bb"
  \gen_yiddish d
}

\book {
  \bookOutputName "terkisher_Eb"
  \gen_yiddish a
}

