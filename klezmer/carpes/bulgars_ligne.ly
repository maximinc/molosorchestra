\version "2.24.4"
\language "english"

\paper {
  page-count=1
  indent = 0
  %ragged-right = ##f
}

\header {
  title = "Bulgars en ligne"
}

chords_rumeynisher=\new ChordGrid \chordmode {
  \time 4/4
  \mark \default
  \bar ".|:"
  d1 s d s
  \bar "||"
  d  d2 c:m c1:m d
  \break

  \mark \default
  \bar ":.|.:"
  c1:m c:m c:m d
  \bar "||"
  c:m  c:m c:m d
  \break
  
  \mark \default
  \bar ":.|.:"
  g:m s   d s
  \bar "||"
  g:m s   c:m d
  \bar ":|."  
}

chords_freitog=\new ChordGrid \chordmode {
  \time 2/4
  \mark \default
  \bar ".|:"
  d2:m s d:m s \bar "||"
  d:m s d:m s
  \break
  \bar ":.|.:"
  \mark \default \textMark "alternative: rester en Im"
  a:m s a:m d:m
  \bar "||"
  a:m s a:m d:m
  \break
  \bar ":.|.:"
  \mark \default \textMark "alternative: rester en I"
  d2 s g4 d a d
  \bar "||"
  d2 s g4 d a d
  \bar ":|."  
}

chords_old_bulgar=\new ChordGrid \chordmode {
  \time 4/4
  \mark \default
  \bar ".|:"
  d1:m s d:m s
  \bar "||"
  d:m s g2:m a d1:m
  \bar ":.|.:"
  \mark \default
  \break
  f1 c2 f2 c1 f \bar "||"
  f  s  g2:m a d1:m
  \bar ":|."  
}

gen_rum =
#(define-scheme-function
     (key)
     (ly:pitch?)
#{
  \score{
    \header {
      piece = "Rumeynisher Bulgar (4/4)"
    }
    \transpose c #key {
      \chords_rumeynisher
    }
  }
#}
)

gen_freitog =
#(define-scheme-function
     (key)
     (ly:pitch?)
#{
  \score{
    \header {
      piece = "Freitog Nokh'n Tsimes (2/4)"
    }
    \transpose c #key {
      \chords_freitog
    }
  }
#}
)

gen_old =
#(define-scheme-function
     (key)
     (ly:pitch?)
#{
  \score{
    \header {
      piece = "Old Bulgar (4/4)"
    }
    \transpose c #key {
      \chords_old_bulgar
    }
  } 
#})
  
  
\book {
  \bookOutputName "bulgars_ligne_C"
  \gen_rum c
  \gen_freitog c
  \gen_old c
}

\book {
  \bookOutputName "bulgars_ligne_Bb"
  \gen_rum d
  \gen_freitog d
  \gen_old d
}

\book {
  \bookOutputName "bulgars_ligne_Eb"
  \gen_rum a
  \gen_freitog a
  \gen_old a
}

