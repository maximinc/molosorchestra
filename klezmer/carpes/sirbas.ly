\version "2.24.4"
\language "english"

#(set-global-staff-size 19)
\paper {
  indent = 0
  %ragged-right = ##f
}

\header {
  title = "Sirbas"
}

chords_rom=\new ChordGrid \chordmode {
  \time 4/4
  \mark \default
  \bar ".|:"
  d1:m s d:m s \bar "||"
  d:m s a d:m 
  \break
  \mark \default
  \bar ":|.|:"
  f s f s \bar "||"
  f s c f
  \bar ":|.|:"
  \mark \default	
  \break
  d1:m d:m d g:m
  \bar "||"
  a s a d:m
  \bar ":|."
}

chords_naie=\new ChordGrid \chordmode {
  \mark \default
  \bar ".|:"
  d1:m d:m d:m d:m
  \bar "||"
  d2:m g:m g1:m a d:m
  \bar ":|.|:"
  c1 f c f
  \bar "||"
  c f:m a d:m
  \bar ":|.|:"
  d1 s d1 s
  \bar "||"
  d s d d4 c:m d2
  \bar ":|."
}


gen_rom =
#(define-scheme-function
     (key)
     (ly:pitch?)
#{
  \score{
    \header {
      piece = "Romeynishe Sirba (4/4)"
      
    }
    \transpose c #key {
      \chords_rom
    }
  }
#}
)

gen_naie =
#(define-scheme-function
     (key)
     (ly:pitch?)
#{
  \score{
    \header {
      piece = "Die Naie Sirba (4/4)"
      
    }
    \transpose c #key {
      \chords_naie
    }
  }
#}
)
  
  
  
\book {
  \bookOutputName "sirbas_C"
  \gen_rom c
  \gen_naie c
}

 
\book {
  \bookOutputName "sirbas_Bb"
  \gen_rom d
  \gen_naie d
}


\book {
  \bookOutputName "sirbas_Eb"
  \gen_rom a
  \gen_naie a
}
