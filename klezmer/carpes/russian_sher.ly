\version "2.24.4"
\language "english"


\paper {
  indent = 0
  %ragged-right = ##f
}

\header {
  title = "Russian Sher (4/4)"
  composer="Abe Schwartz, Russkij Orkestr Novinka, https://www.youtube.com/watch?v=cwRvkWmkz9w"
}

chords_sher=\chordmode {
  \time 2/4
  \mark \default
  
  \bar ".|:"
  d2:m s d:m s
  \bar "||"
  d2:m s d4:m a d2:m
  \bar ":|."
  
  \mark \default
  \break
  f2 c4 f c2 f
  \bar "||"
  f s c f
  \break
  \bar "||"
  \mark \default
  d2:m s d:m s
  \bar "||"
  d2:m s d4:m a d2:m

  \break
  \mark \default
  \bar ".|:-||"
  c2 s f s
  \bar "||"
  f:m s d4:m a d2:m
  
  \bar ":.|.:"
  \break
  \mark \default
  f2 s f s f s f s
  \break
  \mark \default
  \bar ":.|.:"
  
  \repeat volta 2 {
    d2:m s a s
    \bar "||"
    a s d:m s
    \break
    d4:m d g2:m  d:m s
    d:m d4:m a4 d2:m 
  }
  \alternative{
    {d2:m}
    {c2}
  }
  \mark \default
  \break
  \bar ".|:-||"
  f2 s c f 
  \bar "||"
  f s c f
  \bar ":|."
  \break
  \mark \default
  d2:m s d:m s \bar "||"
  a s d:m s
  \break
  g:m s d:m s \bar "||"
  d:m d4:m a d2:m s
  \bar ".|:-||"
  
  
  \break
  \mark \default
  d2 s d s \bar "||" c:m s c:m d 
  \break
  \mark \default
  \bar ":.|.:"
  g:m d g:m s
  \bar "||"
  g:m d g:m s
  \bar ":.|.:"
  \mark \default
  \break
  d s d s 
  \bar "||"
  c:m s c:m d
  \bar ":|."
}

sher_theme={
  \key d \minor
  \relative c'{
    d8 f16 f f f f f g f g a f4 f16 e d e f g f e d8 f a d
    d,8 f16 f f f f f g f g a f4 f16 c c f f ef ef d 
    <<{d2}\\
      {s4. \parenthesize  c8}
    >>
  }
  \break
  \relative c''{
    a8 a g a bf bf a4 g16 a g8 d8 e f4 r8 a8 
    a16 bf c8~ c  c d8. a16 c bf a bf g8. g16 c bf a bf	 a4 r8 a16 a
  }
  \break
  \relative c''{
    a16 g g f f e e f d cs d e f e f g a g g f f e e f d4 r8 a'16 a
    a g g f f e e f d cs d e f e f g a g g f f e e f d4 r8 f
  }
  \relative c'{
    e16 f g8~ g af g af g8. f16 f e d e f g f e f4. f8 e16 f g af g af g af d c b af g8.
    f16 f e e f f ef ef d d4 r8 <c f>
  }
  \relative c''{
    a4. g16 f g8 a8~ a8 f16 a c bf bf a a8 g16 f g8 a8~ a	f16 a 
    c bf bf a a8 g16 f c' bf bf a a8 g16 f c' bf bf a a8 g16 f g a f8~f c
  }
  \relative c'{
    \repeat volta 2{
    
      d8 d d4 f8 f f4 e8 d cs d e4. a,8 e'8 e e4 g8 g g4 f8 e d f
      <<
        {a'16 gs a bf a g f e d8}
        \\
        {a2 r8}
      >>
      
      a g a bf a g4 a8 d, e f a16 gs f8 f16 e d8 e	 f~ f e \tuplet 3/2 {f8 e d cs d e} d4. <a' cs,>8
    }
    \alternative{
      {a16 gs f8 f16 e d16 a}
      {c2}
    }
  }
  \relative c''{
    a16 c b c d c b c f8 c a f g8. a16 bf a g8 a2	 
    a16 c b c d c b c f8 c a f g8. a16 bf a g8 
    <<{f2}
      \\
      {f8 a, d e}
    >>
  }
  \relative c'{
    f8. e16 d4~ d8 e8 f16 e f g f8. e16 d4 r8 d8
    bf'8. a16 g8. f16 e4 r8 e8 f g f16 e d8
    a'4 a16 gs f8 f16 e d8	 
    bf'8. a16 g4 r8 e8 f g a8. g16 f4
    r8 a,8 d e f f f f  \tuplet 3/2 {f8 e d8 cs8 d e8 } d2~ d
  }
  \key g \minor
  
  \relative c'{
    d4 fs fs16 ef d ef fs4
    d  a' a16 g fs g a8 d, c c d ef d c~ c a'16 g fs g a g fs8. ef16 d4 r8 <d a>
  }
  \relative c'{
    bf'4. a16 g a4. ef16 d g fs g a bf a a g g c c bf bf a a g 
    bf4.  a16 g a4. ef16 d g fs g a bf a a g g4. <d g>8
  }
  \relative c''{
    c8. bf16 a bf c8 r16 bf16 a bf c bf a g a8. g16 fs g a8
    r16 g fs g a g fs ef d8 c d ef d c r
    g'16\mordent g fs g a g fs8. ef16 d4 \parenthesize d'
  }
}

gen_sher=
#(define-scheme-function
     (key)
     (ly:pitch?)
#{
  \score{
    \header {
      piece = "Structure: AA B C D E D F F H H JJ KK LL"
    }
    \transpose c #key {
       \new ChordGrid { \chords_sher }
    }
  }
#})


gen_sher_theme=
#(define-scheme-function
     (key)
     (ly:pitch?)
#{
  \score{
    \header {
      piece = "Structure: AA B C D E D F F H H JJ KK LL"
    }
    \transpose c #key {
      <<
       \new ChordNames { 
         \chords_sher
       }
       \sher_theme
      >>
    }
  }
#})

#(set-global-staff-size 13)

\book {
  \bookOutputName "sher_C"
  \gen_sher c
}

\book {
  \bookOutputName "sher_Bb"
  \gen_sher d
}

% \book {
%   \bookOutputName "sher_Eb"
%   \gen_sher a
% }

#(set-global-staff-size 18)
\book {
  \bookOutputName "russian_sher_C"
  \gen_sher_theme c
}

\book {
  \bookOutputName "russian_sher_Bb"
  \gen_sher_theme d
}

\book {
  \bookOutputName "russian_sher_Eb"
  \gen_sher_theme a
}