\version "2.24.4"
\language "english"

\paper {
  page-count=1
  indent = 0
  %ragged-right = ##f
}

\header {
  title = "Bulgars en couple (4/4)"
}

chords_tsurik_fun_milkhome= \new ChordGrid \chordmode {
  \time 4/4
  \mark \default
  \repeat volta 2{
  c1:m s  c:m s
  \bar "||"
  c1:m g:m c2:m g
  }
  \alternative{
    {c1:m}
    {c4:m c:m/b bf2}
  }
  \mark \default
  \break
  \repeat volta 2{
  ef1 s ef s 
  \bar "||"
  ef s ef c:m
  }
  \alternative{
    {c4:m c:m/b bf2}   
    {c1:m}
  }
  %\bar ".|"
}

chords_kamenetzer=\new ChordGrid \chordmode {
  \time 4/4
  \mark \default
  c1:m s c2:m c f1:m
  \bar "||"
  g c:m g c:m
  \bar ":.|.:"
  \break
  \mark \default
  \repeat volta 2{
    c:m f:m g 
  }
  \alternative{
    {c1:m}
    {c2.:m bf4}
  }
  
  \mark \default
  \break
  ef1 s ef s
  \bar "||"
  ef ef
  g c:m
}

gen_tsurik =
#(define-scheme-function
     (key)
     (ly:pitch?)
#{
  \score{
    \header {
      piece = "Tsurik fun der milkhome"
    }
    \transpose c #key {
      \chords_tsurik_fun_milkhome
    }
  }
#}
)

gen_kamenetzer =
#(define-scheme-function
     (key)
     (ly:pitch?)
#{
  \score{
    \header {
      piece = "Kamenetzer bulgar"
    }
    \transpose c #key {
      \chords_kamenetzer
    }
  }
#}
)

\book {
  \bookOutputName "bulgars_en_couple_C"
  \gen_tsurik c
  \gen_kamenetzer c
}
\book {
  \bookOutputName "bulgars_en_couple_Bb"
  \gen_tsurik d
  \gen_kamenetzer d
}
\book {
  \bookOutputName "bulgars_en_couple_Eb"
  \gen_tsurik a
  \gen_kamenetzer a
}