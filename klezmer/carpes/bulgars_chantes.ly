\version "2.24.4"
\language "english"

\paper {
  indent = 0
  %ragged-right = ##f
}

\header {
  title = "Dos Keshenever Shtikele"
  composer = "Moyshe Oysher"
}

chords_shtikele=\new ChordGrid \chordmode {
  \time 4/4
  \mark "Intro"
  \bar ".|:"
  c1 c df c \bar "||"
  c1 c2 bf:m bf1:m c
  \bar ":|.|:"
  \break
  \mark "Chant"
  c1 bf2:m c f1:m c
  \bar "||"
  c  c bf:m c
  \break
  f f f:m df \bar "||"
  f f df c
  c1 bf2:m c f1:m c
  \bar "||"
  c  c bf:m c
  \break
  f1 f df c
  \bar "||"
  c c2 bf:m bf1:m c
  \bar ":|."
  \mark "Coda"
  bf1:m bf:m bf:m \bar "||" c1 c bf:m c \bar "|."
  
}


gen_sht =
#(define-scheme-function
     (key)
     (ly:pitch?)
#{
  \score{
    \header {
      piece = "Dos Keshenever Shtikele (4/4)"
      
    }
    \transpose c #key {
      \chords_shtikele
    }
  }
#}
)
  
  
\book {
  \bookOutputName "bulgars_chantes_C"
  \gen_sht bf
}

\book {
  \bookOutputName "bulgars_chantes_Bb"
  \gen_sht c
}




