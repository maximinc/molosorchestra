\version "2.24.4"
\language "english"


\paper {
  indent = 0
  %ragged-right = ##f
}

\header {
  title = "Neue Sher (4/4)"
  composer="Dave Tarras https://www.youtube.com/watch?v=BdkJAGHIv5c"
}

%%% Structure:
%%%  A x 3
%%%  B, 8 mesures x 2
%%%  C, 8 mesures x 2
%%%  A, 4 mesures x 4
%%%  B, 8 mesures x 1
%%%  C 8 meesures x 2



chords_sher_standalone=\chordmode {
  
  \mark \default
  d1 d d2 a d1 \bar "||"
  d1 d d2 a d1 \bar ":|.|:"
  \mark \default
  \break
  g:m d a d \bar "||"
  g:m d a d  \bar ":|.|:"
  \break
  \mark \default
  d d d a \bar "||" 
  a a a d
  \bar ":|."
}

chords_sher=\chordmode {
  
  \mark \default
  d1 d d2 a d1
  d1 d d2 a d1 d
  
  \mark \default
  g:m d a d
  g:m d a d
      d a d
  
  d d d a
  a a a d
  
  d d d a
  a a a d
  
}

sher_theme={
  \time 4/4
  \tempo 4=180
  \key d \major
  \mark \default
  \relative c'{
    \repeat volta 2{

      d8. d16 a8 d fs fs d fs a4 b8 c~c b16 d c8\mordent b16 a gs8 a4  fs8  g8. fs16 g8 a fs2 r
      \break
      d8. d16 a8 d fs fs d fs a4 b8 c~c b16 d c b16 a8 gs8 a4  fs8  g8 fs16 e a8 a,
    }
    \alternative{
      {d2 r}
      {d2 r8 cs16 d ds e f fs }
    }
  }
  \break
  \relative c''{
    \repeat volta 2{
      g2 r8 bf a g gs a fs4 r8 fs8 e8. d16 e8. fs16 g8 a fs d \tuplet 3/2 {e d cs} gs' a4. r16
      d16 df c b bf a af 
      \break
      g2 r8 bf a g 
    }
    \alternative {
      {\tuplet 3/2 {e' d cs } \tuplet 3/2 {bf a g} fs4 r
      a16 gs a fs g fs g e fs es fs d e ds e cs  d2 r8 cs16 d ds e f fs \break 	}
      {gs8 a fs4 r8 a8 g8.\trill fs16 e8 fs g8 a fs d e8. \trill cs16 d2 \tuplet 3/2 {r8 d8 e d cs b}}
    }
    \break
  }
  \mark \default
  \relative c'{
    a8. d16 e8 fs e d e fs gs a~a4~a4 r16 fs g gs
    a8 fs g e fs d cs16 d cs d e2~ e4 r8 gs, 
    a8. cs16 e8 g fs8. \trill e16 fs8 fs~fs g4. r4 r16 fs g gs a cs b a g b a g fs a g fs e g fs e d2 
    \tuplet 3/2 {r8 d8 e d cs b}
    \break
    \mark 3
    a'8. d16 e8 fs e fs e d gs a~a4 r r16 fs g gs
    a d, fs a g8\trill fs e8.\trill d16 cs8 d e2 r16 a g fs e d cs b
    a8. cs16 e8 g fs g fs8.\trill e16 fs8 g4.~g4 r16 fs g gs
    a8.\trill fs16 g8 e fs d e8.\trill cs16 d4 d'2.\trill
  }
  \bar "|."
}

gen_sher=
#(define-scheme-function
     (key)
     (ly:pitch?)
#{
  \score{
    \header{
      piece = "Neue sher"
    }
    \transpose c #key {
       \new ChordGrid { \chords_sher_standalone }
    }
  }
#})


gen_sher_theme=
#(define-scheme-function
     (key)
     (ly:pitch?)
#{
\score{
    \transpose c #key {
      <<
       \new ChordNames { 
         \chords_sher
       }
       \sher_theme
      >>
    }
    %\midi{}
}
#})

% #(set-global-staff-size 13)
% 
% \book {
%   \bookOutputName "neue_sher_C"
%   \gen_sher c
% }

#(set-global-staff-size 18)
\book {
  \bookOutputName "naie_sher_tona_originale_C"
  \gen_sher_theme c
}

\book {
  \bookOutputName "naie_sher_tona_originale_Bb"
  \gen_sher_theme d

}

\book {
  \bookOutputName "naie_sher_tona_C_C"
  \gen_sher_theme bf,
}

#(set-global-staff-size 18)
\book {
  \bookOutputName "neue_sher_C_chords"
  \gen_sher c
 
  \gen_sher bf
}

