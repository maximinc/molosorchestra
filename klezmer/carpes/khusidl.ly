\version "2.24.4"
\language "english"

#(set-global-staff-size 14)

\paper {
  %spage-count=1
  indent = 0
  %ragged-right = ##f
}

\header {
  title = "Khusidl (4/4)"
}

chords_lustiger = \new ChordGrid \chordmode {
  \time 4/4
  \mark \default
  d1:m s d:m a:m \bar "||"
  d1:m g:m d2:m a d1:m
  \break
  \mark \default
  \bar ":.|.:"
   
  g1:m s d:m f
  \bar "||"
  d1:m g:m d2:m a d1:m
  \break
  \mark \default
  \bar ":.|.:"
  
  g1:m d:m f s
  \bar "||"
  g1:m d:m a d:m

  \break
  \mark \default
  \bar ":.|.:"
  
  d g:m d s \bar "||"
  d d c:m d
  \bar ":|."
}

chords_lustiger = \new ChordGrid \chordmode {
  \time 4/4
  \mark \default
  d1:m s d:m a:m \bar "||"
  d1:m g:m d2:m a d1:m
  \break
  \mark \default
  \bar ":.|.:"
   
  g1:m s d:m f
  \bar "||"
  d1:m g:m d2:m a d1:m
  \break
  \mark \default
  \bar ":.|.:"
  
  g1:m d:m f s
  \bar "||"
  g1:m d:m a d:m

  \break
  \mark \default
  \bar ":.|.:"
  
  d g:m d s \bar "||"
  d d c:m d
  \bar ":|."
}

chords_khupe = \new ChordGrid \chordmode {
  \time 4/4
  \mark \default
  c1:m s c:m s \bar "||"
  \break
  c1:m s c2:m bf ef1 
  \bar "||"
  \break
  c1:m c2:m f:m g1 c:m \bar ":.|.:"
  \mark \default
  \break
  f1:m s f:m f2:m ef \bar "||" 	\break
  f1:m s c2:m/ef bf2:m c1:m
  
  \bar ":.|.:"
  \break
  \mark \default
  c1:m s c1:m s 
  \break
  ef s f2:m \textMark "1st time: rester sur I majeur" g c:m bf
  \break
  ef1 ef f2:m g c1:m
  
  \bar ":|."
}



gen_lustiger=
#(define-scheme-function
     (key)
     (ly:pitch?)
#{
  \score{
    \header {
      piece = "4/4 Lustige Hasidim (version Margot Leverett)"
    }
    \transpose c #key {
      \chords_lustiger
    }
  }
#})

gen_khupe=
#(define-scheme-function
     (key)
     (ly:pitch?)
#{
  \score{
    \header {
      piece = "Freylekhs (https://www.youtube.com/watch?v=3RfOWtW0WTs) Christian Dawid, Sanne Möricke"
    }
    \transpose c #key {
      \chords_khupe
    }
  }
#})


\book {
  \bookOutputName "khusidl_C"
  \gen_lustiger c
  \gen_khupe c
}

\book {
  \bookOutputName "khusidl_Bb"
  \gen_lustiger d
  \gen_khupe d
}

% \book {
%   \bookOutputName "khusidl_Eb"
%   \gen_lustiger a
%   \gen_khupe a
% }
