\version "2.24.4"
\language "english"

#(set-global-staff-size 19)
\paper {
  indent = 0
  %ragged-right = ##f
}

\header {
  title = "Sirbas"
}

chords_gef=\new ChordGrid \chordmode {
  \time 4/4
  \mark \default
  g1:m s c:m g:m \bar "||"
  g:m  s c2:m d g1:m
  
  \break
  f  bf  d g:m \bar "||"
  g:m g:m c2:m cs:dim7 d1

  \break
  g4:m \mark "Gefil -te Fish"
  d g2:m 
  g4:m \mark "Gefil -te Fish"
  d g2:m \break
  
  \mark \default
  g1:m s g:m \mark "Alt: rester Im"  d 
  \bar "||" 
  g1:m s g:m s 
  \break
  g1:m s g:m d \bar "||"
  g1:m s 
  c2:m \mark "Alt: Im" d g4:m d g2:m 
  \break
  g4:m \mark "Gefil -te Fish"
  d g2:m 
  g4:m \mark "Gefil -te Fish"
  d g2:m \break
}

chords_sirb=\new ChordGrid \chordmode {
  \mark \default
  \bar ".|:"
  g1:m s c2:m d g1:m \bar "||"
  g1:m s c2:m d g1:m 
  \bar ":|.|:"

  \break
  \mark \default
  g1:m c2:m cs:dim7 d1:7 g1:m \bar "||"
  g1:m c2:m cs:dim7 d1:7 g1:m 
  \bar ":|.|:"
  \break
  \mark \default
  ef1 bf c:m g:m \bar "||"
  c:m cs:dim7 d:7 g:m
  \bar ":|."
}


gen_gef =
#(define-scheme-function
     (key)
     (ly:pitch?)
#{
  \score{
    \header {
      piece = "Gefilte Fish (4/4)"
      
    }
    \transpose c #key {
      \chords_gef
    }
  }
#}
)

gen_sirb =
#(define-scheme-function
     (key)
     (ly:pitch?)
#{
  \score{
    \header {
      piece = "Gefilte Sirba (4/4)"
      
    }
    \transpose c #key {
      \chords_sirb
    }
  }
#}
)
  
  
  
\book {
  \bookOutputName "gefilte_C"
  \gen_gef c
  \gen_sirb c
}


\book {
  \bookOutputName "gefilte_Bb"
  \gen_gef d
  \gen_sirb d
}


% \book {
%   \bookOutputName "sirbas_Eb"
%   \gen_gef a
%   \gen_sirb a
% }
% 
