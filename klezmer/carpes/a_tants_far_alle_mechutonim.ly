\version "2.24.4"
\language "english"

\include "freylekhs.ly"

\paper {
  %page-count=2
  indent = 0
  %ragged-right = ##f
}

mechutonium={
  \key g \minor
  \relative c'{
    s8 d g bf
    \mark \default
    \repeat volta 2{
    ef4 d8 c d4 c8 bf c4. bf8 ef16 d c8 d4 
    ef8 ef d c f4 ef16 d c8 d2 r8
    d, g bf
    \break
    ef4 d8 c d4 c8 bf c4. bf8 d16 c bf a g4
    \tuplet 3/2 {f8 g a bf c d f ef d c bf a bf4 \parenthesize a8 c bf a} 
    }
    \alternative{
      {g8 d g bf}
      {g2}
    }
  }
  \break
  \mark \default
  \relative c'{
    \repeat volta 2{
      \tuplet 3/2 {f8 g a bf c d d c bf} d4
      \tuplet 3/2 {d8 c bf d c bf d c bf} f'4		
      \tuplet 3/2 {f,8 g a bf c d f ef d c bf a bf4 \parenthesize a8 c bf a}  
    }
    \alternative{
      {g4 r}
      {g8 d'8 bf d}
    } 
  }
  \break
  \mark \default
  \bar ".|:"
  \relative c'''{
    g4 f8 e f4 e8 d e4 d8 cs d cs bf4
    cs8 d d d f e d cs d2 g4 g
    \break
    g8 g f8 e f f e8 d e e d8 cs d cs bf4
    d8 g, a bf d16 cs bf8 bf16 a g8 g2 
    
    r8 g bf d
  }
  \bar ":|.|:"
}


chords_mechutonim=\chordmode{
  g2:m s g:m s c:m f bf4 d g2:m
  g:m s c:m  g:m
  bf c:m d g:m g:m
  
  bf s bf s bf c:m d g:m g:m
  
  g:m s g:m s g:m s g:m s g:m s g:m s d s g:m
}



\header{
  title="A tanz far alle mechutonim"
}

\book {
  \bookOutputName "a_tanz_far_alle_mechutonim_C"
  \score{
    <<
      \time 2/4
      \new ChordNames {
        \transpose c d{
          s2
          \chords_mechutonim
        }
      }
      \transpose c d {
        \mechutonium
      }
    >>
  }
  \score{
    \header {
      piece="A tanz far all mechutonim"
    }
    \transpose c d{
      \chords_mechutonim_grid
    }
  }
}
