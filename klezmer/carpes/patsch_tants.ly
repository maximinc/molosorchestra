\version "2.24.4"
\language "english"

\paper {
  indent = 0
  %ragged-right = ##f
}

\header {
  title = "Patsch tants (2/4)"
}

chords_patsch_pleskun= \new ChordGrid \chordmode {
  \time 2/4
  \mark \default
  d2:m s d:m s 
  \bar "||"
  d:m s d:m s
  \bar ":|."
  \break
  \mark \default
  g2:m s f s
  \bar "||"
  d:m s d:m s
  \break
  d2:m g:m a d2:m
  \bar "||"
  d2:m g:m a d2:m
  \bar ".|:-|."
}


gen_patsch=
#(define-scheme-function
     (key)
     (ly:pitch?)
#{
  \score{
    \header {
      piece = "Pleskun / glik"
    }
    \transpose c #key {
      \chords_patsch_pleskun
    }
  }
#})

\book {
  \bookOutputName "patsch_tants_C"
  \gen_patsch c
}

\book {
  \bookOutputName "patsch_tants_Bb"
  \gen_patsch d
}

% \book {
%   \bookOutputName "patsch_tants_Eb"
%   \gen_patsch a
% }