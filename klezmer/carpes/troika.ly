\version "2.24.4"
\language "english"

\paper {
  page-count=1
  indent = 0
  %ragged-right = ##f
}

\header {
  title = "Troika (4/4)"
}

chords_troika= \new ChordGrid \chordmode {
  \time 4/4
  \mark \default
  d1:m s d:m s
  \bar "||"
  c2 f a d:m g:m d:m a d:m
  \break
  \bar ":.|.:"
  d1:m a bf a
  \bar "||"
  d:m g:m d2:m a d1:m
  \bar ":|."
}


gen_troika=
#(define-scheme-function
     (key)
     (ly:pitch?)
#{
  \score{
    \header {
      piece = "Troika"
    }
    \transpose c #key {
      \chords_troika
    }
  }
#})


\book {
  \bookOutputName "troika_C"
  \gen_troika c
}

\book {
  \bookOutputName "troika_Bb"
  \gen_troika d
}

\book {
  \bookOutputName "troika_Eb"
  \gen_troika a
}

