\version "2.24.4"
\language "english"

#(set-global-staff-size 14)
\paper {
  %page-count=2
  indent = 0
  %ragged-right = ##f
}

\header {
  title = "Freylekhs (2/4)"
}

chords_beregovsky= \new ChordGrid \with {
      majorSevenSymbol = \markup "x4"
    }\chordmode {

  \time 2/4
  \mark \default
  \bar ":.|.:"
  g2:m s s s 
  \bar "||"
  g2:m s s s
  \bar ":.|.:"
  \break
  
  \mark \default
  c2:m  s
  g2:m  s
  \bar "||"
  g2:m c:m d:7 g:m
  \bar ":.|.:"
  \break
   
  \mark \default
  g2 s s s
  \bar "||"
  g2 s s s
  \bar ":.|.:"
  \break

  \mark \default
  g2   s c:m s
  \bar "||"
  c:m  g g g:maj7
  
  \bar ":.|.:"
}

chords_poshet=\new ChordGrid \chordmode {
  \time 2/4
  
  \mark \default
  \bar ":.|.:"
  d2 s g:m s
  \bar "||"
  c:m s d s \break
  
  \mark \default
  \bar ":.|.:"
  d2 s g:m s 
  \bar "||"
  c:m s d s \break
  \bar ":.|.:"
}      



chords_mechutonim_grid=\new ChordGrid \chordmode {
  \time 2/4
  \mark \default
  \bar ":.|.:"
  g2:m s g:m s \bar "||"
  c:m f bf4 d g2:m  
  \break
  g:m s c:m  g:m
  \bar "||"
  bf c:m d g:m
  \bar ":.|.:"
  \break
  \mark \default
  bf s bf s 
  \bar "||"
  bf c:m d g:m
  \bar ":.|.:"
  \break
  \mark \default
  
  g:m s g:m s \bar "||" 
  g:m s g:m s 
  \break
  g:m s g:m s \bar "||"
  d s g:m s \bar ":.|.:"
}


gen_mechu =
#(define-scheme-function
     (key)
     (ly:pitch?)
#{
  \score{
    \header {
      piece = "A tanz far alle mechutonim"
    }
    \transpose c #key {
      \chords_mechutonim_grid
    }
  } 
#})
  
gen_beregovsky =
#(define-scheme-function
     (key)
     (ly:pitch?)
#{
  \score{
    \header {
      piece = "Freylekhs 137"
    }
    \transpose c #key {
      \chords_beregovsky
    }
  } 
#})
  
gen_poshet =
#(define-scheme-function
     (key)
     (ly:pitch?)
#{
  \score{
    \header {
      piece = "Poshet Freylekhs"
    }
    \transpose c #key {
      \chords_poshet
    }
  } 
#})
  

\book {
  \bookOutputName "freylekhs_C"
  \gen_mechu c
  \gen_beregovsky c
  \gen_poshet c
}

\book {
  \bookOutputName "freylekhs_Bb"
  \gen_mechu d
  \gen_beregovsky d
  \gen_poshet d
}
\book {
  \bookOutputName "freylekhs_Eb"
  \gen_mechu a
  \gen_beregovsky a
  \gen_poshet a
}