\version "2.24.4"
\language "english"

\paper {
  indent = 0
  %ragged-right = ##f
}

\header {
  title = "Valses (3/4)"
}

chords_vals= \new ChordGrid \chordmode {
  \time 3/4
  \mark \default
  a2.:m s e a
  \bar "||"
  d:m s a d:m
  \break
  e s a:m s 
  \bar "||"
  d:m a:m e a:m
  \bar ":.|.:"
  \break
  
  \mark \default
  e s a:m s 
  \bar "||"
  g s c s
  \break
  d:m s a:m s
  \bar "||"
  d:m a:m e a:m
  \bar ":|."
  \break
  
  \mark \default
  c s c s 
  \break
  \bar ".|:-|"
  d:m a:m d:m g
  \bar "||"
  c g f f
  \break
  d:m a:m d:m g
  \bar "||"
  g g c s
  \bar ":|."
  

}

chords_alexandrovsky=\chordmode {
  \time 3/4
  e2.:m s b s 
  \bar "||"
  b s e:m s \break
  a:m s a:m s
  \bar "||"
  e:m b e:m s
}      

chords_vi=\new ChordGrid \chordmode {
  \time 3/4
  b2.:m s b:m s \bar "||" e:m s b:m s
  \break
  e:m e:m/cs b:m s \bar "||" fs s b:m s
  \break
  e:m e:m/cs b:m s \bar "||" fs s b:m s
}      


chords_shloymele=\new ChordGrid \chordmode {
  \time 3/4
  \bar ".|:"
  b2.:m s b:m s \bar "||" b:m s fs s \break
  e:m s b:m s \bar "||" fs s b:m s
  \bar ":.|.:" \break
  b:m s e:m s \bar "||" e:m s b:m s \break
  d s a s \bar "||" a s d fs
  \bar ":|."
  
}

gen_vals=
#(define-scheme-function
     (key)
     (ly:pitch?)
#{
  \score{
    \header {
      piece = "Vals"
    }
    \transpose c #key {
      \chords_vals
    }
  }
#})

gen_alexandrovsky=
#(define-scheme-function
     (key)
     (ly:pitch?)
#{
  \score{
    \header {
      piece = "Alexandrovsky"
    }
    \new ChordGrid {
      \transpose c #key {
        \repeat volta 2 {\chords_alexandrovsky}
        \break
        \transpose c f{
          \repeat volta 2 { \chords_alexandrovsky}
        }
%         \break
%         \transpose c bf{
%           \repeat volta 2 { \chords_alexandrovsky}
%         }
%         \break
%         \transpose c df{
%           \repeat volta 2 {\chords_alexandrovsky}
%         }
      }      
    }

    %\transpose c f {
     % \chords_alexandrovsky
    %}
  }
#})

gen_vi_iz=
#(define-scheme-function
     (key)
     (ly:pitch?)
#{
  \score{
    \header {
      piece = "Vi iz dus gesele"
    }
    \transpose c #key {
      \chords_vi
    }
  }
#})

gen_shloymele=
#(define-scheme-function
     (key)
     (ly:pitch?)
#{
  \score{
    \header {
      piece = "Shloymele, Malkele"
    }
    \transpose c #key {
      \chords_shloymele
    }
  }
#})

\book {
  \bookOutputName "valses_C"
  \gen_vals c
}

\book {
  \bookOutputName "valses_Bb"
  \gen_vals d
}

\book {
  \bookOutputName "valses_Eb"
  \gen_vals a
}

#(set-global-staff-size 12)
\book {
  \bookOutputName "alexandrovsky_vi_iz_shloymele_C"
  \gen_alexandrovsky d
  %\gen_alexandrovsky g
  \gen_vi_iz c
  \gen_alexandrovsky bf
  %\gen_alexandrovsky f
  \gen_shloymele c
}

\book {
  \bookOutputName "alexandrovsky_vi_iz_shloymele_Bb"
  \gen_alexandrovsky e
  %\gen_alexandrovsky g
  \gen_vi_iz d
  \gen_alexandrovsky c
  %\gen_alexandrovsky f
  \gen_shloymele d
}

% \book {
%   \bookOutputName "alexandrovsky_vi_iz_shloymele_Eb"
%   \gen_alexandrovsky a
%   \gen_vi_iz a
%   \gen_shloymele a
% }

#(set-global-staff-size 20)
\book {
  \bookOutputName "vu_iz_dus_alexandrovsky"
  \score{
    \header {
      piece = "vi iz dus sur Alex"
    }
    \transpose b a{
    <<
      \new ChordNames{
        \chordmode{
          s2. b2.:m s fs s
              fs s b:m s
              e:m s e:m s
              b:m fs b:m
        }
      }
      \new Staff{
        \time 3/4
        \tempo 4=180
        \relative c'{
           \key b \minor
           s2 s8 fs8 b4 b4. d8 fs4 d4. cs8 b4 as4. b8 cs2.
           cs4 cs4. d8 e4 d4. cs8 b4 b4. d8 fs2.
           \break
           b4 b4. b8 b4 g4. fs8 e4 e4. g8 b2.
           fs4 d4.. fs16 \tuplet 3/2 {e8 fs e} d4.. cs16 b2. r2. 
        }
          
      }
      \new Staff{
        \relative c''{
          %\time 3/4
          \key b \minor
          s2.
          b4 b4. cs8 d4 d4. e8 fs4 fs4. es8 fs2.
          fs,4 as cs <g' e> <fs d>  <e cs> d4 d4. b8 d2.
          
          e4 e4. fs8 g4 fs e g4 g4. a8 <b g>2.
          fs,4 as cs <g' e> <fs d>  <e cs> <d b> <fs d>4. <e cs>8 <d b>2.
        }
        \bar "|."
      }

    >>
    }
    %\midi {}
  }
}
