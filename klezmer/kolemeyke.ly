\version "2.18.2"
\language "english"

\header {
  title = "Kolemeyke"
  composer = ""
}


theme={
  \key d \minor
  \time 4/4
  \tempo 4 = 100
  \mark \default

  \bar ".|:"
  \relative c'{
    
     a16^"8ve for clarinet" d f a gs a f a gs a f a gs f e d 
     a16 d f a gs a f a a4 \glissando d8 r

     a,16 d f a gs a f a gs a f a gs f e d 
     f e g f e d e f d4 d8 \parenthesize d^"2nd time"
  }
  \bar ":|.|:"
  \break
  \mark \default
  \relative  c'{
    d8 g4 \tuplet 3/2 {a16 g fs} g8 g4 \tuplet 3/2 {a16 g fs} g8 g4 
    \tuplet 3/2 {a16 g fs} g8 f a4 gs16 a gs f e d e f d8 d d d
    gs16 a gs f e d e f d4 d8 <d a'>
  }
  \break
  \bar ":|.|:"
  \mark \default
  <<
  \relative c''{
    d4. cs16 e d4. cs16 e d8 cs16 e d8 cs16 e d8 cs16 e  d4
    d16 c c b b a a d d c c b b a a d d c c b b a a gs gs8. f16 a4
    d,8 g16 f e d e f d8 d d d
    d8 g16 f e d e f d4 r8 <d a'>
  }\\
  \relative c'{
    f4. e16 g f4. e16 g f8 e16 g f8 e16 g f8 e16 g f4\trill
    d8 c b a d c b a d c b a gs8. f16 a4
  }
  >>
  \bar ":|."
}


cdm=\relative c{ r8 <f a d> r8 <f a d> }
ca=\relative c{r8 <e a cs> r8 <e a cs> }
cgm=\relative c'{r8 <g bf d> r8 <g bf d>}
comp={
  \key d \minor
  \cdm \cdm \cdm \relative c{ r8 <f a d> <f a d> r}
  \cdm \cdm \ca  \relative c{ r8 <f a d> <f a d> r}
  \cgm \cgm \cgm \relative c'{ r8 <g bf d> <f a d>8  r}
  \ca \cdm \ca \relative c{ r8 <f a d> <f a d> r}
  
  
  \relative c{ 
    r8 <f a d> <f a d>4
    r8 <f a d> <f a d>4
    \cdm \cdm \cdm \cdm 
    \cdm \cdm 
    \ca \cdm \ca 
    <f a d>8 r  <f a d>8 r
    
  }
}

chordNames=\chordmode{
  d1:m d:m d:m  a2:7 d2:m
  g1:m g2.:m d4:m/f a2:7  d:m a:7 d:m
  
  d1:m d:m
  d:m d:m a2:7 d:m a:7 d:m
}

structure=\markup{
 % Structure: thème x N, terminer avec mesures 21 à 24 x 3
}

\book {
  \bookOutputName "kolemeyke_C"
  \score{
  <<
    \new ChordNames { 
      \transpose c c { \chordNames }
    }

    \new Staff \with { instrumentName = #"Theme (C)" } {
      \transpose c c { \theme }
    }

    \new Staff \with { instrumentName = #"Comp (C)" } {
      \transpose c c { \comp }
    }

  >>
  %\layout { #(layout-set-staff-size 16) }
  \layout {}
  %\midi { }
  }
  \structure
}

\book {
  \bookOutputName "kolemeyke_C_clef_ut"
  \score{
  <<
    \new ChordNames { 
      \transpose c c { \chordNames }
    }

    \new Staff \with { instrumentName = #"Theme (C)" } {
      \clef C
      \transpose c c { \theme }
    }

    \new Staff \with { instrumentName = #"Comp (C)" } {
      \clef C
      \transpose c c { \comp }
    }

  >>
  %\layout { #(layout-set-staff-size 16) }
  \layout {}
  %\midi { }
  }
  \structure
}

\book {
  \bookOutputName "kolemeyke_Bb"
  \score{
  <<
    \new ChordNames { 
      \transpose c d { \chordNames }
    }

    \new Staff \with { instrumentName = #"Theme (Bb)" } {
      \transpose c d { \theme }
    }

    \new Staff \with { instrumentName = #"Comp (Bb)" } {
      \transpose c d { \comp }
    }

  >>
  %\layout { #(layout-set-staff-size 16) }
  \layout{}
  }
  \structure
}


\book {
  \bookOutputName "kolemeyke_Eb"
  \score{
  <<
    \new ChordNames { 
      \transpose c a { \chordNames }
    }

    \new Staff \with { instrumentName = #"Theme (Eb)" } {
      \transpose c a { \theme }
    }

    \new Staff \with { instrumentName = #"Comp (Eb)" } {
      \transpose c a { \comp }
    }

  >>
  %\layout { #(layout-set-staff-size 16) }
  \layout{}
  }
  \structure
}

