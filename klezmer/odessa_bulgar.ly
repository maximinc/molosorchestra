
\version "2.18.2"
\language "english"

\header {
  title = "Odessa Bulgar"
}


chordNames = \chordmode{
  s1
  d1:m d:m d:m d:m
  d:m d:m a:7 d:m
  a:7 d:m d:m
  
  d:m e2:7 a:m e:7 d:m a1:7 d:m
  a:7 d:m
  d:m a:7
  d:m a:7
  a2:7 d:m

%   d1:m d:m d:m a:7
%   d:m d:m a:7 d:m
%   a:7 d:m d:m

}

themeA={
  \repeat volta 2{
  a4 gs a b c b a8 gs f4 gs16 b a8 a a a gs f gs a b a gs f e f gs \break
  a4 gs a b c b a8 gs f4 e8 f 
  a16 gs f8
  gs4  a d, r8 a'8 {a16 gs f8} f4
  \break 
  e8 f  {a16 gs f8 } gs4  a 
  
  }
   \alternative{
    {d,  r r8 a d f }
    {d2  r2}
   }
}
clarinet = {
  \tempo 4=160
  \key a \minor
  \relative c'{
    r4 r r8 a d f 
    \mark \default
    \themeA
  }
  \break
  \mark \default
  \relative c''{
    \repeat volta 2 {
    r8 d d d d d d d
    
    d c b d c b a c b a gs b
    a gs f4 
    e8 f  {a16 gs f8 } gs4  a \break
    d, r8 a'8 {a16 gs f8 } f4
    e8 f {a16 gs f8  } gs4  a <d, d'> r r2
    }
  }
  \break	 
  \relative c'{
    \mark \default
    \repeat volta 2{
    d8 d r a'8 {a16 gs f8} f4
    e8 d e f gs a f16 e d8
    d8 d r a'8  {a16 gs f8 } f4
    }
    \alternative{
      {e8 d e f d4 r4}
      {e8 d e f d a d f}
    }
  }
%   \break
%   \relative c''{
%     \themeA
%   }
  \bar "|."
}

voicetwo={
  \key a \minor
  r4 r r r
  \relative c'{  
    \mark \default
    \repeat volta 2{
    r8 f16 f f8 d f16 f f8 d f
    r8 f16 f f8 d f16 f f8 d f
    r8 f16 f f8 d f16 f f8 d f
    r8 f16 f f8 d f16 f f8 d f
    %r8 e16 e e8 cs e16 e e8 cs e
    r8 f16 f f8 d f16 f f8 d f
    r8 f16 f f8 d f16 f f8 d f
    r8 e16 e e8 cs e16 e e8 cs e
    r8 f16 f f8 d f16 f f8 d f
    r8 e16 e e8 cs e16 e e8 cs e
    }
    \alternative{
      {    r8 f16 f f8 d f16 f f8 d f  }
      {    r8 f16 f f8 d f16 f f8 d f  }
    }
  }
  \break
  \mark \default
  \relative c''{
    \repeat volta 2 {
    r8 f f f f f f f
    
    f e d f e d c e d c b d
    c b a4 
    gs8 a  {c16 b a8 } gs4  a \break
    f r8 c'8 {c16 b a8 } a4
    gs8 a {c16 b a8  } gs4  a f4 r r2
    }
  }
  \break	 
  \relative c'{
    \mark \default
    \repeat volta 2{
    f8 f r c'8 {c16 b a8} a4
    gs8 f gs a b c a16 gs f8
    f8 f r c'8  {c16 b a8 } a4
    }
    \alternative{
      {gs8 f gs a f4 r4}
      {gs8 a gs f d a d f}
    }
  }
%   \break
%   \relative c''{
%     \themeA
%   }
  \bar "|."
}

\book{
  \bookOutputName "odessa_bulgar_C"
  \score {
    <<
      \new ChordNames { 
        \transpose c ef { \chordNames }
      }
      \new Staff  \with { instrumentName = #"(C)" }{ 
        \transpose c ef { \clarinet  }
      }
      \new Staff  \with { instrumentName = #"(C)" }{ 
        \transpose c ef { \voicetwo  }
      }
    >>
    \layout { }
  }
}

\book{
  \bookOutputName "odessa_bulgar_C_clef_ut"
  \score {
    <<
      \new ChordNames { 
        \transpose c ef { \chordNames }
      }
      \new Staff  \with { instrumentName = #"(C)" }{ 
        \clef C
        \transpose c ef, { \clarinet  }
      }
      \new Staff  \with { instrumentName = #"(C)" }{ 
        \clef C
        \transpose c ef, { \voicetwo  }
      }
    >>
    \layout { }
  }
}

\book{
  \bookOutputName "odessa_bulgar_Bb"
  \score {
    <<
      \new ChordNames { 
        \transpose c f { \chordNames }
      }
      \new Staff  \with { instrumentName = #"(Bb)" }{ 
        \transpose c f { \clarinet  }
      }
      \new Staff  \with { instrumentName = #"(Bb)" }{ 
        \transpose c f { \voicetwo  }
      }
    >>
    \layout { }
  }
}


\book{
  \bookOutputName "odessa_bulgar_Eb"
  \score {
    <<
      \new ChordNames { 
        \transpose c c { \chordNames }
      }
      \new Staff  \with { instrumentName = #"(Eb)" }{ 
        \transpose c c { \clarinet  }
      }
      \new Staff  \with { instrumentName = #"(Eb)" }{ 
        \transpose c c { \voicetwo  }
      }
    >>
    \layout { }
  }
}
