\version "2.18.2"
\language "english"

\header {
  title = "Ershter vals"
  composer = ""
}


theme={
  \tempo 4=160
  \time 3/4
  \key d \minor
  
  \relative c'''{
    r4 bf4 a g4. f8 e4 r a g f4. e8 d4 r g f e f e e d cs d
    r r r r
%         << 
%           \magnifyMusic 0.63 {f a d}\\
%           \magnifyMusic 0.63 {a, f d}
%         >>
  }

  \relative c'{ a }

  \break
  \mark \default

  \repeat volta 2{
    \relative c'{
      bf2 a4 d2 e4 f2 r4 r d e f d e f g f e2 r4 r r a,
      %\break
      bf2 a4 e'2 f4 g2 r4 r e f g e f g a bf a2 r4 r r a,
      %\break
      bf2 a4 d2 e4 f2 r4 r d e f e f g f g a2 r4 r bf4 a
      %\break
      g4. f8 e4 r a g f4. e8 d4 r g f e f e e d cs 
    }
  }
  \alternative {
    \relative c'{ d^"Fine" 
%       << 
%         \magnifyMusic 0.63 {f a d}\\
%         \magnifyMusic 0.63 {a, f d}
%       >>
      r r r
      r4 a
    }
    \relative c'{ d  r r \mark \markup { \musicglyph #"scripts.coda" } r
%       << 
%         \magnifyMusic 0.63 {f a d}\\
%         \magnifyMusic 0.63 {a, f \mark \markup { \musicglyph #"scripts.coda" }  d }
%       >>
    }
  }
  \relative c''{ a f }
  
  \mark \default
  %\break
  \repeat volta 2{
    \relative c''{
       a2. r4 a f
       a2. r4 bf g
       bf2. r4 bf g 
       bf2. r4 r8 e, f4 g2 g4 g f g a2. 
       r4 g f e2 bf'4 a2 cs,4 d2.
    }
  }
  
  \alternative{
    \relative c''{ r4 a f }
    \relative c'{ r4 r^"To A" a }
  }
  \bar "|."
  \break
  
  \mark \markup { \musicglyph #"scripts.coda" }  
  \relative c''{ r4 r8 d4 c8 }
  \mark \default
  
  \repeat volta 2{
    \relative c''{
      c2. r4 bf4. g8 a2.
      r4 ef'4. d8 d2. c4 a c bf2.
      r4 g4. a8 
      %\break
      bf2. 
      r4 g4. d'8 c2.
      r4 bf4 g c,4. e8 g bf d2 c4 f4. f8 f4
    }
  }   
  \alternative{
    \relative c''{r4 r8 d4 d8}
    \relative c''{r4 a bf^"To B"}
  }
  \bar "|."
}

achord=\relative c'{r <cs e> <cs e>}
dmchord=\relative c'{r <d f> <d f>}
cseven=\relative c'{ r <bf e> <bf e>}
fchord=\relative c'{r <a f'> <a f'>}
dseven=\relative c'{ r <c fs> <c fs>}
gmchord=\relative c'{ r <bf g'> <bf g'>}

comp={
  %\tempo 4=160
  \time 3/4
  \key d \minor
  %\set Score.markFormatter = #format-mark-circle-letters
  
  \relative c'{
    r4 r r
    \achord
    \achord
    \dmchord
    \dmchord
    \achord
    \achord
    <d f> a f d r r
  }
  \repeat volta 2{
    \dmchord \dmchord \dmchord \dmchord 
    \dmchord \dmchord \achord \achord 
    \achord \achord \achord \achord 
    \achord \achord \dmchord \dmchord
    \dmchord \dmchord \dmchord \dmchord
    \dmchord \cseven \fchord \fchord
    \achord \achord
    \dmchord \dmchord
    \achord \achord
  }
  \alternative{
    \relative c'{<d f> a f d r r}
    \relative c'{<d f> a f d r r}
  }
  \repeat volta 2{
    \relative c'{
      <<{r4 d8 e f g a2 r4
         r4 d,8 e f g a2 r4
         r4 e8 f g a bf2 r4
         r4 e,8 f g a bf2 r4}
        {r4 a,8 cs d e f2 r4
         r4 a,8 cs d e f2 r4
         r4 cs8 d e f g2 r4
         r4 cs,8 d e f g2 r4}
      >>
      \achord \achord  \dmchord \dmchord
      \achord \achord
      \relative c'{<d f>4 a f}
    }
  }
  \alternative{
    \relative c{ d4 r r}
    \relative c{ d4 r r }
  }
  
  \bar "|."
  \mark \markup { \musicglyph #"scripts.coda" }  
  \relative c{ d4 r r }
  \mark \default
  
  \repeat volta 2{
    \cseven \cseven \fchord \fchord
    \dseven \dseven \gmchord \gmchord
    \gmchord \gmchord \fchord \fchord
    \cseven \cseven \fchord
  }
  \alternative{
    \relative c'{<a f'>4 r r}
    \relative c'{<a f'>4 r r}
  }
}

grid={
  \chordmode{
    s2. a:7 s d:m s a:7 s d:m s
    
    d:m s d:m s d:m s a:7 s
    a:7 s a:7 s a:7 s d:m s
    d:m s d:m s d:m c:7 f s
    a:7 s d:m s a:7 s
      d:m s
      d:m s
    
    d:m s d:m s g:m s g:m s a:7 s d:m s a:7 s d:m s s s
    
    c s f s d:7 s g:m s
    g:m s f s c s f s
    
  }
}

structure=\markup{
  \left-column{
    \line{Intro, AA BB AA CC, BB A fine}
  }
}


\paper{
   page-count=1
}

\book {
  \bookOutputName "ershter_vals_C"
  \score{
  <<
    \new ChordNames { 
      \transpose c c { \grid }
    }
    \new Staff \with { instrumentName = #"Theme (C)" } {
      \transpose c c {
        \theme
      }
    }

    \new Staff \with { instrumentName = #"Comp (C)" } {
      \transpose c c {
        \comp
      }
    }

  >>
  %\layout { }
  \layout { #(layout-set-staff-size 16) 
    indent = 1\cm
    \context {
      \Staff
      \override VerticalAxisGroup.default-staff-staff-spacing.basic-distance = #0
    }
  }
  %\midi{}
  }
  \structure
}

\book {
  \bookOutputName "ershter_vals_C_clef_ut"
  \score{
  <<
    \new ChordNames { 
      \transpose c c { \grid }
    }
    \new Staff \with { instrumentName = #"Theme (C)" } {
      \transpose c c {
        \clef C
        \theme
      }
    }

    \new Staff \with { instrumentName = #"Comp (C)" } {
      \transpose c c {
        \clef C
        \comp
      }
    }

  >>
  %\layout { }
  \layout { #(layout-set-staff-size 16) 
    indent = 1\cm
    \context {
      \Staff
      \override VerticalAxisGroup.default-staff-staff-spacing.basic-distance = #0
    }
  }
  %\midi{}
  }
  \structure
}

\book {
  \bookOutputName "ershter_vals_Bb"
  \score{
  <<
    \new ChordNames { 
      \transpose c d { \grid }
    }
    \new Staff \with { instrumentName = #"Theme (Bb)" } {
      \transpose c d {
        \theme
      }
    }

    \new Staff \with { instrumentName = #"Comp (Bb)" } {
      \transpose c d {
        \comp
      }
    }

  >>
  %\layout { }
  \layout { #(layout-set-staff-size 16) 
    indent = 1\cm
    \context {
      \Staff
      \override VerticalAxisGroup.default-staff-staff-spacing.basic-distance = #0
    }
  }
  }
  \structure
}

\book {
  \bookOutputName "ershter_vals_Eb"
  \score{
  <<
    \new ChordNames { 
      \transpose c a { \grid }
    }
    \new Staff \with { instrumentName = #"Theme (Eb)" } {
      \transpose c a {
        \theme
      }
    }

    \new Staff \with { instrumentName = #"Comp (Eb)" } {
      \transpose c a {
        \comp
      }
    }

  >>
  %\layout { }
  \layout { #(layout-set-staff-size 16) 
    indent = 1\cm
    \context {
      \Staff
      \override VerticalAxisGroup.default-staff-staff-spacing.basic-distance = #0
    }
  }
  }
  \structure
}
