
\version "2.18.2"
\language "english"

\header {
  title = "Shalom Aleichem"
  composer = "Barcelona Gipsy Balkan Orchestra"
}


chordNames = \chordmode{
  s1 
  d2:m a:7 d1:m d1:7 g1:m
  a1:7 d:m f2 g:m a4:7 bf a2
 
  d2:m a:7 d1:m d1:7 g1:m
  a1:7 d:m f2 g:m a:7  d:m
  
  d1:m f2 g:m g:m a:7 d:m a:7
  d1:m f2 g:m bf e:7/gs a:7 d:m
}

clarinet = {
  \tempo 4=160
  \key d \minor
  
  \relative c'{
    r2 r8 a d f 
    
    
  }
  %\mark \default
  \repeat volta 2{
    \relative c''{
      a4. d8 f,4. a8 e d d4
      r8 d16 f a d f a d2 bf4 a8 bf a g g4    
      r8 g4 a16 bf 
      \break
      a4. e8 a4. g8 g f f4
      r8 e8 f g a4 a a a a16 bf a g f e f g a8 a, d f
      a4. d8f,8 g16 f e8 f e d d4 r8 d f a d2 bf4 a8 bf a g g4
      r8 g4 a16 bf 
      \break
      a4. e8 a4. g8 g f f4
      r8 e f g a4 a a a a16 bf a g f e d cs d4 r8
      
      a 
      \break
      f'8. f16 f8 e d4 r8 d d'8. a16 a8 af g4  r8
      g g8. a16 bf8 d cs e d cs d8 a a f a4 
      r8
      a,  
      \break
      f'8. f16 f8 e d4 r8 d d'8. a16 a8 f g4  r8 bf
      \bar "||"
      a16 bf a g a g f e e f g f e d cs e d cs d e f e d cs d8 
      
      \parenthesize  a \parenthesize  d \parenthesize  f
    }
  }
}


\book{
  \bookOutputName "shalom_aleichem_C"
  \score {
    <<
      \new ChordNames { 
        \transpose c c { \chordNames }
      }
      \new Staff  \with { instrumentName = #"Theme (C)" }{ 
        \transpose c c { \clarinet  }
      }
    >>
    \layout { }
  }
}

\book{
  \bookOutputName "shalom_aleichem_C_clef_ut"
  \score {
    <<
      \new ChordNames { 
        \transpose c c { \chordNames }
      }
      \new Staff  \with { instrumentName = #"Theme (C)" }{ 
        \clef C
        \transpose c c, { \clarinet  }
      }
    >>
    \layout { }
  }
}


\book{
  \bookOutputName "shalom_aleichem_Bb"
  \score {
    <<
      \new ChordNames { 
        \transpose c d { \chordNames }
      }
      \new Staff  \with { instrumentName = #"Theme (Bb)" }{ 
        \transpose c d { \clarinet  }
      }
    >>
    \layout { }
  }
}

\book{
  \bookOutputName "shalom_aleichem_Eb"
  \score {
    <<
      \new ChordNames { 
        \transpose c a { \chordNames }
      }
      \new Staff  \with { instrumentName = #"Theme (Eb)" }{ 
        \transpose c a, { \clarinet  }
      }
    >>
    \layout { }
  }
}
