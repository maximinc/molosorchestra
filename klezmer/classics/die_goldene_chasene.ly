\version "2.18.2"
\language "english"

\header {
  title = " Die Goldene Chasene "
  composer = "Dave Tarras https://www.youtube.com/watch?v=lO3Ih5W1KkI"
}


theme={
  \key a \minor
  \time 4/4
  \relative c''{
    s2 a2\fermata 
    \glissando
    
    \mark \default
    \bar "||"
    a'4 e8 a~a g fs8.\trill  e16 a8 e4. r8 a g16 fs e8 ds e g16 fs e8 e16 ds c8 c16 b a8
    ds8 e4.  r8  ds16 e f fs g gs \break
    a4 e8 a~a g f\trill e a d,4. f2
    ds16 e fs8 g16 fs e8 e16 ds c8 c16 b a8 a2 
    r8  a8 d fs
    \break
    \mark \default
    a2~a8 gs g8.\trill fs16 gs8 a a a a2
    b8.\trill a16 b8.\trill a16 b8.\trill a16 b8.\trill a16 b8 c~c4~c2
    \break
    b8\mordent a a\mordent gs gs\mordent f f\mordent e 
    g gs8~gs4~gs4. f8 
    e f\mordent gs a gs\mordent e f\mordent d 
    e2 r8 ds16 e f fs g gs \break
    
    \mark 1
    a4 e8 a~a g fs8.\trill  e16 a8 e4. 
    r4 a16 g fs e ds8 e g16 fs e8 e16 ds c8 c16 b a8 ds e~e4
    r8 ds16 e f fs g gs \break
    a4 e8 a~a g f\trill e a d,4. f2
    ds16 e fs8 g16 fs e8 e16 ds c8 c16 b a8 a2
    
    r
    \mark 3
    \break
    a,8 c c c c a c e ds8.\trill c16 ds8\trill c 
    b c b\trill a
    
    gs b e, gs b d c\trill b a b c ds e4 \tuplet 5/4 {e,16 f fs g gs}
    \break
    
    a8 c c c c a c e ds8.\trill c16 ds8\trill c 
    b c b8.\trill a16
    gs8 b e, gs b d c\trill b a2 \glissando e'
    \break
    
    \mark 3
    r1 r r r2 r4 a \glissando \break a'1 \trill ~ a
    
    r1 r2 r4 a 
    \break
    \mark 4
    \repeat volta 2{
      f2. a4 e2 r8 e d8.\trill c16 b4. c8 d b c ds e2 r8 a8 r16 af g gf
      \break
      f2~f8 a16 gs b a gs f e2 r8 e8 ds8.\mordent c16 
      
      b8\coda c ds e ds\mordent c b c
    }
    \alternative {
      {a2 r4 a'4}
      {a,2 r16 e' ds e f fs g gs^"DC"}
    }
  }  
  \break
  \sectionLabel "Coda"
  \relative c''{
    b8 \coda c ds e 
    %a,2 \glissando 
    \tuplet 3/2 {a,16^"or glissando" as b c cs d ds e f fs g gs}
    a4 
    e a,4 r \bar "|."
  }
  
}

tenor={
  \key a \minor
  \relative c'{
    s2 r 
    r1 r8 e a c ds e~e4 r1 
    r8 a,16 a a8 a a2
    r1 r8 f4. a2 b8 c~c a gs gs e4
    r8 a16 a a8 e a2
    
    a1 r8	 fs8 e d fs2	
    r1 f' f4-> r4 r2
    r8 b,16 b e8 b e4 r
    r1
    r8 e16 e e8 e e d c b


    a4 e8 a~a2
    r8 e a c ds e~e4 r1 
    r8 e,16 e a8 e a16 a a8 e a
    c2 cs d b
    b8 c~c a gs gs e8 e a16 a a8 e a a16 a a8 e a
    
    
    r1 r r r r r r
    r2 r8  e'16 e e8 e
    
    a, c c c c a c e ds\mordent c ds\mordent c b c b\mordent  a
    gs b e, gs b d c\mordent b a b c ds e2
    a,8 c c c~c a c e ds\mordent c ds\mordent c b c b\mordent a
    gs b e, gs b d c b a2.
    
    c4
    \repeat volta 2{
      d2. d4 c2 r8 c8 b\mordent a
      gs4. a8 b gs a b c2 r4 e
      d2. d4 c2 r8 c b\mordent a gs2. \coda e4
    }
    \alternative{
      {r8 a16 a a8 a a a b cs }
      {r8 e16 e e8 e e4 r}
    }
  }
  \section
  \sectionLabel "Coda"
  \relative c''{
    gs2 \coda r a4 e a r
  }
}
chord_grid=\chordmode{ 
  s1
  a1:m s a:m s
  a2:m a d1:m e a:m
  d1 s d1 s
  e1 s e  s
  
  a1:m s a:m s
  a2:m a d1:m e a:m
  
  a:m s e a:m
  a:m s e a:m
  a:m s e a:m
  a:m s e a:m
  
  d:m a:m e a:m
  d:m a:m e a:m
  a:m
  e a:m

}

#(set-global-staff-size 18)


\book {
  \bookOutputName "die_goldene_chasene_C"
  \score{
  <<
    \new ChordNames { 
      \transpose c c { \chord_grid }
    }

    \new Staff \with { instrumentName = #"Cla (C)" } {
      \transpose c c{ \theme }
    }
    \new Staff \with { instrumentName = #"Tpt (C)" } {
      \transpose c c{ \tenor }
    }
  >>
  }
}

\book {
  \bookOutputName "die_goldene_chasene_Bb"
  \score{
  <<
    \new ChordNames { 
      \transpose c d { \chord_grid }
    }

    \new Staff \with { instrumentName = #"Cla (Bb)" } {
      \transpose c d{ \theme }
    }
    \new Staff \with { instrumentName = #"Tpt (Bb)" } {
      \transpose c d{ \tenor }
    }
  >>
  }
}


\book {
  \bookOutputName "die_goldene_chasene_Eb"
  \score{
  <<
    \new ChordNames { 
      \transpose c a { \chord_grid }
    }

    \new Staff \with { instrumentName = #"Cla (Eb)" } {
      \transpose c a{ \theme }
    }
    \new Staff \with { instrumentName = #"Tpt (Eb)" } {
      \transpose c a{ \tenor }
    }
  >>
  }
}
