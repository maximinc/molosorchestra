\version "2.18.2"
\language "english"

\header {
  title = "Dos tsigayner"
  composer = "Dave Tarras https://www.youtube.com/watch?v=3Z4RKPgfm8w"
}


trumpet={
  \key bf \major
  \time 4/4
  
  \mark \default
  \repeat volta 2{
    \relative c' {
      fs8 g g g a bf a8.\trill g16 \tuplet 3/2 {g16 a g} f8~f e f16 g e4 c8
      fs8 g g g a bf a8.\trill g16 \tuplet 3/2 {g16 a g} f8~f e f16 g e4 c8
      \break
      d16 ef ef8 ef ef fs g fs8.\trill ef16 ef8 d8 r16 b16 c d ef d c b c4
      d16 ef ef8 ef ef fs g fs8.\trill ef16 \tuplet 3/2 {ef16 f ef} d8 r16 b16 c d ef d c b c4 
    }
  }
  \break	 
  \mark \default
  \relative c''{
    \repeat volta 2{
      <<
      {d8 ef ef d d\mordent c4 r8
      d8 ef ef d d\mordent c4 r8}
      \\
      {
        f,8 g af g f ef4 r8
        f8 g af g f ef4 r8
      }
      >>
      r8 c' r8 c r4
      f,	16 d ef f 
    }
    \alternative {
      {gf af f gf ef f d ef c4 r }
      {gf'16 af f gf ef f d ef c4 bf}
    }
  }
  \break
  \mark \default 

  \relative c''{
    \repeat volta 2{
      <<{g4 af g af g }
        \\
        {bf,4 cf bf cf bf}
      >>
      
      f'16 ef g f ef8 bf ef f
      g4 f16 g f ef d ef f8 r16 d ef f
    }
    \alternative {
      {fs16 r ef8 r d c g c4}
      {fs16 r ef8 r d c g bf4}
    }
  }
  \break
  \mark \default
  \relative c''{
    \repeat volta 2{
      \tuplet 3/2 {a8 bf c} bf8. af16 \tuplet 3/2 {af bf af} g8~g8 \parenthesize ef8
      \tuplet 3/2 {d8 ef f} ef8. d16 c16 \parenthesize d c8 r4
      d8 ef f gf gf f r16 d ef f
    }	  
    \alternative {
      {      r8 ef r d c g c4}
      {      r8 ef r d c4 bf}
    }
  }
  \mark 3
  \break
  \repeat volta 2{  s1  }
  
  \mark 5
  \relative c''{
    \repeat volta 2{
      c4 bf16 a bf c a4 r
      \tuplet 3/2 {bf8 bf bf } af16 g af bf g4 r
      g8 bf af g16 f f8 f af4
    }
    \alternative{
      {g\trill f16 e f g e4 r}
      {g\trill f16 e f g e4 r}
    }
  }
  \break
  \mark \default
  \relative c''{
    \repeat volta 2{
      r8 g r g r g g g g1
      r8 g r g af2
      r16 g f e df c bf df c2
    }
  }
  \mark 1
  s1  
  \bar "|."
  
}

cla={
  \key bf \major
  \repeat volta 2{
    \relative c'' {
      fs8 g g g a bf a8.\trill g16 g8 f4 e8 f16 g e8 r16 g, c e
      fs16 g g g g g g g a a bf bf a8.\trill g16 g8 f4 e8 f16 g e8 r16 g, c d
      \break
      d ef ef ef ef ef ef ef fs8 g fs8. \trill  ef16 ef8 d8 r16 b c d ef d c b c4
      d16 ef ef ef ef ef ef ef fs8 g fs8. \trill  ef16 ef8 d8 r16 b c d ef d c b c4
    }
  }
  \relative c''{
    \repeat volta 2{
      r2 r16 g16 c16. d32 \tuplet 3/2 {ef16 d c} g'8
      r2 r16 g,16 c16. d32 \tuplet 3/2 {ef16 d c} g'8
      
      d8 ef f gf gf f8 r16 d ef f 
    }
    \alternative{
      {gf af f gf ef f d ef c4 r}
      {gf'16 af f gf ef f d ef c4 bf}
    }
  }
  \relative c'''{
    \repeat volta 2{
      g4 f16 \mordent ef f af g4 f16 \mordent ef f af g4 f16 \mordent ef g f
      ef8 	bf ef f
      g4 f16 g f\mordent ef d ef f8 r16 d ef f 
    }
    \alternative{
      {gf f8\trill ef16 ef d \tuplet 3/2 {ef d c} c8 g c4}
      {gf'16 f8\trill ef16 ef d \tuplet 3/2 {ef d c} c8 g c4}
    }
  }
  \relative c'''{
    \repeat volta 2{
      \tuplet 3/2 {a8 bf c} bf8. \trill af16 af g16  bf g bf g f \mordent ef
      \tuplet 3/2 {d8 ef f} ef8.\trill d16 c ef g ef g  ef32 g \tuplet 3/2 {ef16 d c}
      d8 ef f gf gf f r16 d16 ef f 
     
    }
    \alternative{
      {gf af f gf ef f d ef c8 g c4}
      {gf'16 af f gf ef f d ef c4 bf4}
    }
  }
  \mark 3
  \break
  \repeat volta 2{  s1  } 
  
  \relative c'''{
    \repeat volta 2 {
      c4 bf16 a bf c a4 r16 f g a \tuplet 3/2 {bf8 bf8 bf } af16 g af bf g4 r
      g16 af bf af af af\mordent g f f8 f af4 
    }
    \alternative{
      {g8 g f16 \mordent e f g e4 c \glissando}
      {g'8 g f16 e f g e4 r16 g, c e}
    }
  }
  
  \relative c'''{
    \repeat volta 2{
      g8\trill f f f f4. b8 g8 g 	f\trill e16 d e f g8 r16 g,16 c e
      g8\trill f f f f8 af,16 c df f af c b8 f16 e df c bf df c2
    }
  }
  \mark 1
  s1  
  \bar "|."
}

chord_grid=\chordmode{ 
  c1 g2 c c1 g2 c
  c1:m g2 c:m c1:m g2 c:m
  
  f4:m/c f:m/af c2:m f4:m/c f:m/af c2:m
  c4:m fs:dim f2 :m 
  g2 c:m
  g2 c:m
  
  ef1 ef ef2 f:m 
  g2 c:m
  g2 c:m
  
  ef1 g2 c:m	
  
  c2:m f:m 
  g c:m 
  g c:m 
  
  s1
  
  c2 f bf ef
  c2 f:m 
  g c 
  g c
  
  g1 g2 c g2 f:m df4 g c2
}

#(set-global-staff-size 18)


\book {
  \bookOutputName "dos_tsigayner_C"
  \score{
  <<
    \new ChordNames { 
      \transpose c c { \chord_grid }
    }

    \new Staff \with { instrumentName = #"Cla (C)" } {
      \transpose c c{ \cla }
    }
    \new Staff \with { instrumentName = #"Tpt (C)" } {
      \transpose c c{ \trumpet }
    }
  >>
  }
}


\book {
  \bookOutputName "dos_tsigayner_Bb"
  \score{
  <<
    \new ChordNames { 
      \transpose c d { \chord_grid }
    }

    \new Staff \with { instrumentName = #"Cla (Bb)" } {
      \transpose c d{ \cla }
    }
    \new Staff \with { instrumentName = #"Tpt (Bb)" } {
      \transpose c d{ \trumpet }
    }
  >>
  }
}

\book {
  \bookOutputName "dos_tsigayner_Eb"
  \score{
  <<
    \new ChordNames { 
      \transpose c a { \chord_grid }
    }

    \new Staff \with { instrumentName = #"Cla (Eb)" } {
      \transpose c a{ \cla }
    }
    \new Staff \with { instrumentName = #"Tpt (Eb)" } {
      \transpose c a{ \trumpet }
    }
  >>
  }
}
