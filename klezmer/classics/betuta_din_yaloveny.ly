\version "2.18.2"
\language "english"

\header {
  title = "Betuta din Yaloveny"
  composer = "German Goldenshteyn https://www.youtube.com/watch?v=mUj-up1frIE"
}


theme={
  \key f \major
  \time 4/4
  
  \break
  \mark \default
  \relative c'{
    \repeat volta 2{
      c4 f8 a gs a bf8. a16 gs a bf8~ bf16 a16 gs a f g a bf c b c8
      c,4 f8 a gs a bf8. a16
    }
    \alternative{
      {gs16 a bf8~ bf16 a16 gs a f4 r}
      {gs16 a bf8~ bf16 a16 gs a f4 r8 d'16 c}
    }
  }
  \break
  \mark \default
  \relative c''{
    \repeat volta 2{
      b16 c d ef d8.\trill c16 f c a c bf8.\trill a16 gs a bf8~ bf16 a16 gs a f g a bf c b c8
      b16 c d ef d8.\trill c16 f c a c bf8.\trill a16
    }
    \alternative{
      {gs16 a bf8~ bf16 a16 gs a f4 r8 d'16 c}
      {gs16 a bf8~ bf16 a16 gs a f4 f8. e16}
    }
  }
  \break
  \mark \default
  \relative c'{
    \repeat volta 2{
      d8 f a d c16 bf a bf g8. f16 e f g8 g16 f e f d e f g a gs a8 cs16 e d8
      d,16 f a d c bf a bf g8. f16 e f g8 g16 f e f d4 r
    }
  }
  
}

chord_grid=\chordmode{ 
  f1 f f c2 f c2 f
  f1 f f c2 f c2 f
  
  d2:m g:m a d:m d:m g:m a d:m

}

\book {
  \bookOutputName "betuta_din_yaloveny_C_et_Bb"
  \score{
  <<
    \new ChordNames { 
      \transpose c c { \chord_grid }
    }

    \new Staff \with { instrumentName = #"Theme (C)" } {
      \transpose c c{ \theme }
    }
  >>
  }
  \score{
  <<
    \new ChordNames { 
      \transpose c d { \chord_grid }
    }

    \new Staff \with { instrumentName = #"Theme (Bb)" } {
      \transpose c d{ \theme }
    }
  >>
  }
}


\book {
  \bookOutputName "betuta_din_yaloveny_Bb_et_Eb"
  \score{
  <<
    \new ChordNames { 
      \transpose c d { \chord_grid }
    }

    \new Staff \with { instrumentName = #"Theme (Bb)" } {
      \transpose c d{ \theme }
    }
  >>
  }
  \score{
  <<
    \new ChordNames { 
      \transpose c a { \chord_grid }
    }

    \new Staff \with { instrumentName = #"Theme (Eb)" } {
      \transpose c a{ \theme }
    }
  >>
  }
}