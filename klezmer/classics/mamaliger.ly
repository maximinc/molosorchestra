\version "2.18.2"
\language "english"

\paper {
   page-count=1
}
\header {
  title = "Mamaliger"
  composer = " Danny Rubinstein And His Orchestra – The Happy People https://www.youtube.com/watch?v=aV6S1LJV4lM"
}


theme={
  \key d \minor
  \time 3/4
  \tempo 4=120
  
  \break
  %\mark \default
  \relative c''{
    s4. g8 f g f4. e8 d e f4. e8 d e f e g2~ g4. c,8 d e f4. e8 g e f4. d8 e c d2.~d4
    \tempo 4=134
    r8 g8 f g
  }
  \break
  \time 2/4
  \mark \default
  \relative c'{
    f8. e16 d8 e f8  e d8 e f e g4 
    r8 c, d e f e g e f d e c d2~d2
  }
  \break
  \mark \default
  \relative c''{
    \repeat volta 2{
      d4 a8. g16 fs8 g a bf a8. g16 fs8 g a4 \glissando d
      \break
      r8 c, d e f8. e16 g8 e f d e\trill c 
    }
    
    \alternative {
       <<
         {s2 r8 d fs a}\\
         {d,2~ d2}
        >>
      {d2~d4 cs16 d fs a }
    }
  }
  \break
  \bar "||"
  \mark \default
  \relative c''{
    c2~c2~c2~c8 b16 d c8 b16 a as8 b4.~b2~b2~b8 d,8 g a
    \break
    bf2~bf2~bf2 d8 cs16 e d8 cs16 bf
    a2~a2~a2~a2
  }
  \break
  \mark \default
  \bar "||"
  \relative c''{
    d4 a8 a d4 a8 a d8. a16 a8 g g8.\trill fs16 e8 d
    c'4 g8 g c4 g8 g \tuplet 3/2 {g fs ef ef d c} c4 r8 g
    \break
    \tuplet 3/2 {c d ef g fs ef g fs ef ef d c c	 d ef g fs ef}
    fs g c4
    \tuplet 3/2 {c,8 d ef g fs ef g fs ef ef d c} d2 r8 g f g  
    \bar "|."
  }
}

chord_grid=\chordmode{ 
  s2. d:m s s c d:m s s s
  
  d1:m d4:m c4 r1. d1:m 
  
  d1 d 
  r1. d1:m 
      d1:m
  
  d1 s g s g:m s d s
  
  d2 s d s
  c:m s c:m s
  c:m s c:m s
  c:m s d s

}

\book {
  \bookOutputName "mamaliger_C"
  \score{
  <<
    \new ChordNames { 
      \transpose c c { \chord_grid }
    }

    \new Staff \with { instrumentName = #"Theme (C)" } {
      \transpose c c{ \theme }
    }
  >>
  }
}

\book {
  \bookOutputName "mamaliger_Bb"
  \score{
  <<
    \new ChordNames { 
      \transpose c d { \chord_grid }
    }

    \new Staff \with { instrumentName = #"Theme (Bb)" } {
      \transpose c d{ \theme }
    }
  >>
  }
}

\book {
  \bookOutputName "mamaliger_Eb"
  \score{
  <<
    \new ChordNames { 
      \transpose c a { \chord_grid }
    }

    \new Staff \with { instrumentName = #"Theme (Eb)" } {
      \transpose c a{ \theme }
    }
  >>
  }
}
