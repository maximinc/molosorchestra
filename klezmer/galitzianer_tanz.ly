
\version "2.18.2"
\language "english"

\header {
  title = "Galitzianer Tanz"
}


chordNames = \chordmode{
  s1  d:m d:m d:m a:7
  a:7 d:m a:7 d:m d2:m s4 c:7
  f1 f f c:7
  c:7  c:7 c:7 f f
  
  d:m d:m d:m g2:m c:7 f1 d:m
  a:7 d:m
  d:m d:m d:m g2:m c:7 f1 d:m
  a:7 d:m 
}

clarinet = {
  \tempo 4=160
  \key d \minor
  
  r2 r8
  \relative c''{
    a8 bf a 
  }
  \mark \default
  \repeat volta 2{
    \relative c''{
      d4 a f' e d2 r8 d8 e f g f e d e d cs bf a4 r r
     a
     a8 cs cs cs cs a bf a a d d d d2
     e8 g f e f e d cs       
    }
  }
  \alternative{
    \relative c'' {d4 r r8 a bf a}
    \relative c'' {d4 r r c}
  }
  
  \mark \default
  \repeat volta 2{
    \relative c''{
      f8 a a a a4\trill g8 f f a a a a4\trill
      g8 f a4 bf a g8 gs a g g fs g4 r 
      
      e8 g g g g4 f8 e e g g g g4 f8 e  g4 a g8 f a g 
      
    }
  }
  \alternative{
    \relative c''{f e f g a g f e}
    \relative c''{f4 e8 f4 e8 \tuplet 3/2 {f e d}}
  }
  \break
  \mark \default
  \relative c''{
    d2 r8 g8 f e f4 e8 f8~f e \tuplet 3/2 {f e d}
    d2 r8 bf' a g g2\trill r4 c, f4. a8 c bf a g f g f e d e f g a4 g f e 
    d2 r4 f4 d a8 d~ d g f e f4 e8 f~ f e \tuplet 3/2 {f e d } 
    d4 a8 d~ d bf' a g g2\trill r4
    c,4 f4. a8 c bf a g f g f e d e f g a4 g f e d2 r8 a bf a
  }
  \bar "|."
}

voicetwoAB={
  r2 r2
  \repeat volta 2{
    \relative c'{
      f1 d4 f8 a d,4. a8
      f'2 f4 f8 e8~ e8 cs'4 e8 a4 r
      e,2 ~ e8 a, e' a, f'4 f8 e d f e d e2 e4 a
    }
  }
  \alternative{
    \relative c'{f f8 e f4 r}
    \relative c'{f f8 e f4 r}
  }
  \repeat volta 2{
    \relative c''{
      a8 c c c c4 bf8 a a c c c c4
      bf8 a c4 d c bf8 a c bf bf a bf4 r
      g8 bf bf bf bf4 a8 g g bf bf bf bf4 a8 g bf4 c bf a8 g 
    }
  }
  
  \alternative{
    \relative c''{a2 r}
    \relative c''{a2 r}
  }
}

voicetwoC={
  \relative c'{
    f2 d a'2 d, f  r g4 a bf r2 r
    f4 f2 r4 d4 a' bf a cs d a d
    a f2  d'4 d a4. d d4 f,4. d' d4 g,4 a bf c 
    f4. a8 c bf a g f g f e d e f g a4 bf a cs d2 r
  }
  %\bar "|."
}
voicetwo={
  \key d \minor
  \voicetwoAB
  \voicetwoC
}

voicetwosax={
  \key d \minor
  \voicetwoAB
  \transpose c c, {\voicetwoC}
}


\book{
  \bookOutputName "galitzianer_tanz_C"
  \score {
    <<
      \new ChordNames { 
        \transpose c c { \chordNames }
      }
      \new Staff  \with { instrumentName = #"(C)" }{ 
        \transpose c c { \clarinet  }
      }
      \new Staff  \with { instrumentName = #"(C)" }{ 
        \transpose c c { \voicetwo  }
      }
    >>
    \layout { }
  }
}

\book{
  \bookOutputName "galitzianer_tanz_C_clef_ut"
  \score {
    <<
      \new ChordNames { 
        \transpose c c { \chordNames }
      }
      \new Staff  \with { instrumentName = #"(C)" }{ 
        \clef C
        \transpose c c, { \clarinet  }
      }
      \new Staff  \with { instrumentName = #"(C)" }{ 
        \clef C
        \transpose c c { \voicetwo  }
      }
    >>
    \layout { }
  }
}

\book{
  \bookOutputName "galitzianer_tanz_Bb"
  \score {
    <<
      \new ChordNames { 
        \transpose c d { \chordNames }
      }
      \new Staff  \with { instrumentName = #"(Bb)" }{ 
        \transpose c d { \clarinet  }
      }
      \new Staff  \with { instrumentName = #"(Bb)" }{ 
        \transpose c d { \voicetwo  }
      }
    >>
    \layout { }
  }
}

\book{
  \bookOutputName "galitzianer_tanz_Eb"
  \score {
    <<
      \new ChordNames { 
        \transpose c a { \chordNames }
      }
      \new Staff  \with { instrumentName = #"(Eb)" }{ 
        \transpose c a, { \clarinet  }
      }
      \new Staff  \with { instrumentName = #"(Eb)" }{ 
        \transpose c a { \voicetwosax  }
      }
    >>
    \layout { }
  }
}
