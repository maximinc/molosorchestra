\version "2.18.2"
\language "english"

\header {
  title = "Dhrupad"
  composer = ""
}


theme={
  \key e \major
  \time 3/4
  %\tempo 4 = 100

  \relative c'{
    s4 s e 
    \repeat volta 2 {a2^"Browning Raga Bhimpalasi" gs4 fs2 fs4 a2 gs4 fs2 fs4 a2 a4 b2 a4 gs2 fs4 e2 fs4 }
  }
  
  \break
  \bar ":|."
  
  \time 5/4
  \key cs \minor
  \relative c''{
    gs4^"Bandish Gyan mad mate" fs8 gs b a gs8 fs e4
    gs4 fs8 gs b a gs8 fs e4
    gs4 fs8 gs b a gs8 fs e4
    cs8 e fs gs fs e fs d4 cs8
    cs8 e fs gs fs e fs d4 cs8
    b cs e fs gs b cs4 cs
    e d8 cs cs b cs a gs e
    gs4 fs8 gs b a gs8 fs e4
    gs4 fs8 gs b a gs8 fs e4
    gs4 gs r r r
    gs8 a b cs a cs4 cs r8
    gs8 a b cs a cs4 cs r8
    
    e8 ds e fs d cs4 cs cs8
    gs8 a b cs a cs4 cs r8
    e8 ds e fs d cs4 cs cs8
    
    d4 d8 d b cs a4 gs
    d'4 d8 d b cs a4 gs
    
    e8 fs gs fs e fs4 d8 b cs
    gs'4 fs8 gs b a gs8 fs e4
    gs4 fs8 gs b a gs8 fs e4
    gs4 r r r r

  }

  \break
  \bar ":|."
  
 \time 5/4
  \key df \major
  \relative c''{
    c8.^"Bandish Madho Man Bhayo" bf16 af4 f8 gf f8. ef16 df4
    c'8. bf16 af4 f8 gf f8. ef16 df4
    c8 c af c4 df8 gf f8~ f16 ef16 df8
    c8 c af c4 df8 gf f8~ f16 ef16 df8
    df4 gf8 f gf af8~ af8 af8~ af4
    df,4 gf8 f gf af8~ af8 af8~ af4
  }

}


chordNames=\chordmode{ 
s
}

\book {
  \bookOutputName "Dhrupad_C"
  \score{
  <<
    \new ChordNames { 
      \transpose c c { \chordNames }
    }

    \new Staff \with { instrumentName = #"Theme (C)" } {
      \transpose c c { \theme }
    }
  >>
  %\layout { #(layout-set-staff-size 16) }
  \layout {}
  \midi { }
  }
}